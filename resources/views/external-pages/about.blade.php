@extends('layouts.web-skin')

{{-- title --}}
@section('title')
	MerrisCoop | About Us
@endsection

{{-- contents --}}
@section('contents')
    <section class="background-11" id="about">
        <div class="container">
            <div class="row no-gutters">
                <div class="col-lg-4 py-3 py-lg-0" style="min-height:400px; background-position: top;">
                    <div class="background-holder radius-tl-secondary radius-bl-lg-secondary radius-tr-secondary radius-tr-lg-0" style="background-image:url('assets/images/ceo.jpg');"></div>
                    <!--/.background-holder-->
                </div>
                <div class="col-lg-8 px-5 py-6 my-lg-0 background-white radius-tr-lg-secondary radius-br-secondary radius-bl-secondary radius-bl-lg-0">
                    <div class="d-flex align-items-center h-100">
                        <div data-zanim-timeline="{}" data-zanim-trigger="scroll">
                            <h5 data-zanim='{"delay":0}'>Message From CEO</h5>
                            <p class="my-4" data-zanim='{"delay":0.1}'>About 1.7 billion people are excluded from basic banking services, out of this firgure, 60 million are Nigerians. At MerrisCoop, we’re optimistic about the future of the underserved and unbanked segment of the economy. Our services are geared towards including this  segment into financial services. <br /><br />
It is common knowledge that getting loans from mainstream banks for business needs either for personal requirement or other needs could be a herculian task due to their process and requirement. We believe that accessing cash loans for immidate business needs or personal needs is the right of everyone and shouldn’t be difficult.
<br /><br />
With alternative forms of financing, like our agricultural loans, cooperative loans, association or group loans, perosonal loans or micro business loan. micro business owners are ensured the fundings they need to meet emergencies and grow their business, and expand their bottom line.
<br /><br />
MerrisCoop is here to serve you. We’re the face of growth, success and greatness. We are the Financial Power House of the future!
</p><img data-zanim='{"delay":0.2}' src="assets/images/signature.png" alt="" />
                            <h5 class="text-uppercase mt-3 fw-500 mb-1" data-zanim='{"delay":0.3}'>Merris Coop</h5>
                            <h6 class="color-7 fw-600" data-zanim='{"delay":0.4}'>Apapa office</h6></div>
                    </div>
                </div>
            </div>
            <div class="row mt-6">
                <div class="col">
                    <h3 class="text-center fs-2 fs-md-3">Company Overview</h3>
                    <hr class="short" data-zanim='{"from":{"opacity":0,"width":0},"to":{"opacity":1,"width":"4.20873rem"},"duration":0.8}' data-zanim-trigger="scroll" />
                    <p class="text-justify">
                        We are certified professionals, highly knowledgeable workforce with high microfinance skills, micro enterprise development expertise, Modern power house solution for start-ups and budding enterprises, and state of the earth service, High-level standards and stability.
                    </p>
                </div>
                <div class="col-12">
                    <div class="background-white px-3 mt-6 px-0 py-5 px-lg-5 radius-secondary">
                        <h5>We Advise. We Lend. We Amass Funds. We Partner.</h5>
                        <p class="mt-3">MerrisCoop is legally registered, authorized to perform agency banking and licensed to practice lending services. As a credit-led financial services institution, we are positioned to be the leading non- collaterised innovative loan provider in the private lending industry.</p>
                        <blockquote class="blockquote my-5 ml-lg-6" style="max-width: 700px;">
                            <h5 class="fw-500 ml-3 mb-0">Quote from Merriscoop</h5></blockquote>
                        <p class="column-lg-2 dropcap">We are focused to serve those at the bottom of the economic pyramid. These segments are the “unbanked” and “underserved”, which has remained largely excluded from financial services despite several initiatives.
                        We are passionate about improving access for loans, helping the active poor, providing the needed support and enhancing financial inclusion for nano and micro businesses and individuals. At MerrisCoop, we help businesses to grow, successful and attain greatness as well as equip individuals with the needed funds without collateral with our range of credit and lending facilities.
                        </p>
                    </div>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>
    <section id="mission">
        <div class="container">
            <div class="row mb-2">
                <div class="col-md-6 mt-6 mt-sm-0 text-center">
                    <img src="{{ asset('images/icons/mountain.png') }}">
                    <h3 class="text-center fs-2 fs-lg-3">Our Mission</h3>
                    <p>
                        To provide easy, quick, flexible, accessible, affordable products, services and programs that uplift, respond to the unique opportunities and the needs of our customers.
                    </p>
                </div>
                <div class="col-md-6 mt-6 mt-sm-0 text-center">
                    <img src="{{ asset('images/icons/telescope.png') }}">
                    <h3 class="text-center fs-2 fs-lg-3">Our Vision</h3>
                    <p>
                        We envisioned being the leading financial lender in the market we serve and the preferred in every branded product we offer.
                    </p>
                </div>
            </div>
        </div>
        <!--/.container-->
    </section>
    <section id="objective">
        <div class="container">
            <div class="row mb-2">
                <div class="col-md-5 mt-6 mt-sm-0 text-center">
                    <img src="{{ asset('images/icons/objective.png') }}">
                    <h3 class="text-center fs-2 fs-lg-3">Our Objective</h3>
                </div>

                <div class="col-md-7 mt-6 mt-sm-0 text-left">
                    <ul>
                        <li>Provide responsible lending to the poor and promote sustainable development.</li>
                        <li>Creation of funds and a source of credit for our customers at affordable rate of interest for productive and providential purposes.</li>
                        <li>Facilitating accumulation of savings for our customers and promote supportive enterprise.</li>
                        <li>Provision of full nano, micro financial and non-financial services for our customers.</li>
                        <li>Enhance financial inclusion for the underserved and unbanked.</li>
                    </ul>
                </div>
            </div>
        </div>
        <!--/.container-->
    </section>

    <section id="core-value">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mb-4 mt-sm-0">
                    <h3 class="text-center fs-2 fs-lg-3">Our Core Value</h3>
                </div>
                <div class="col-md-6 mt-4 mt-sm-0">
                    <ul>
                        <li class="mb-4">
                            <h5>Customers First</h5>
                            Driven by our customer philosophy, the customer always comes first. We are customer- centric organization. The customer is the driving force of our entire product.
                        </li>
                        <li class="mb-4">
                            <h5>Integrity</h5>
                            Integrity is central in all our operations. Compliance with regulations and the host communities is primary while decisions are based purely on business model and ethics. 
                        </li>
                        <li class="mb-4"><h5>Excellence</h5>
                            We pay thorough attention to details and strive at all times to attain and exceed the highest possible standards and expectations.</li>
                        <li class="mb-4">
                            <h5>Innovation</h5>
                            Innovation is core to our business.  We leverage technology to automated processes, develop unique platforms, create new financials products, systems, design procedures that are consistent and enduring. 
                        </li>
                        
                    </ul>
                </div>
                <div class="col-md-6 mt-6 mt-sm-0">
                    <ul>
                        <li class="mb-4">
                            <h5>Team Work</h5>
                            At MerrisCoop, we hold the interest of team above those of individuals, while showing mutual respect for all employees and sharing information throughout our organization.

                        </li>

                        <li class="mb-4"><h5>Accountability</h5>
                            We understand conflict of interest is detrimental to our business. As an organization, we are answerable to our stakeholders on all actions and results. </li>
                        <li class="mb-4"><h5>Support & Service to the Community</h5>
                            We believe that our organization and societies in which we do business are in a symbolic relationship. This is why we have taken Corporate Social Responsibility as core to our existence and would initiate several programmes to our host communities.</li>
                        
                        <li class="mb-4">
                            <h5>Tolerance</h5> 
                            No tolerance for behavior that is inconsistent with these values.
                        </li>
                    </ul>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>

    {{-- <section id="leadership">
        <div class="container">
            <div class="row mb-4">
                <div class="col">
                    <h3 class="text-center fs-2 fs-lg-3">Global leadership</h3>
                    <hr class="short" data-zanim='{"from":{"opacity":0,"width":0},"to":{"opacity":1,"width":"4.20873rem"},"duration":0.8}' data-zanim-trigger="scroll">
                </div>
            </div>
            <div class="row text-center">
                <div class="col-sm-6 col-lg-4">
                    <div class="background-white pb-4 h-100 radius-secondary"><img class="mb-4 radius-tr-secondary radius-tl-secondary" src="assets/images/portrait-3.jpg" alt="Profile Picture" />
                        <div class="px-4" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                            <div class="overflow-hidden">
                                <h5 data-zanim='{"delay":0}'>Merris Coop</h5></div>
                            <div class="overflow-hidden">
                                <h6 class="fw-400 color-7" data-zanim='{"delay":0.1}'>Advertising Consultant</h6></div>
                            <div class="overflow-hidden">
                                <p class="py-3 mb-0" data-zanim='{"delay":0.2}'>Merris Coop is the Founder and CEO of MerrisCoop, which he started from his dorm room in 2014 with 3 people only.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4 mt-4 mt-sm-0">
                    <div class="background-white pb-4 h-100 radius-secondary"><img class="mb-4 radius-tr-secondary radius-tl-secondary" src="assets/images/portrait-4.jpg" alt="Profile Picture" />
                        <div class="px-4" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                            <div class="overflow-hidden">
                                <h5 data-zanim='{"delay":0}'>Lily Anderson</h5></div>
                            <div class="overflow-hidden">
                                <h6 class="fw-400 color-7" data-zanim='{"delay":0.1}'>Activation Consultant</h6></div>
                            <div class="overflow-hidden">
                                <p class="py-3 mb-0" data-zanim='{"delay":0.2}'>Lily leads MerrisCoop Nig, and oversees the company’s Customer Operations teams supporting millions ofr users.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4 mt-4 mt-lg-0">
                    <div class="background-white pb-4 h-100 radius-secondary"><img class="mb-4 radius-tr-secondary radius-tl-secondary" src="assets/images/portrait-5.jpg" alt="Profile Picture" />
                        <div class="px-4" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                            <div class="overflow-hidden">
                                <h5 data-zanim='{"delay":0}'>Thomas Anderson</h5></div>
                            <div class="overflow-hidden">
                                <h6 class="fw-400 color-7" data-zanim='{"delay":0.1}'>Change Management Consultant</h6></div>
                            <div class="overflow-hidden">
                                <p class="py-3 mb-0" data-zanim='{"delay":0.2}'>As the VP of People, Thomas’s focus lies in the development and optimization of talent retention.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4 mt-4">
                    <div class="background-white pb-4 h-100 radius-secondary"><img class="mb-4 radius-tr-secondary radius-tl-secondary" src="assets/images/portrait-6.jpg" alt="Profile Picture" />
                        <div class="px-4" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                            <div class="overflow-hidden">
                                <h5 data-zanim='{"delay":0}'>Legartha Mantana</h5></div>
                            <div class="overflow-hidden">
                                <h6 class="fw-400 color-7" data-zanim='{"delay":0.1}'>Brand Management Consultant</h6></div>
                            <div class="overflow-hidden">
                                <p class="py-3 mb-0" data-zanim='{"delay":0.2}'>As General Counsel of MerrisCoop, Tony oversees global legal activities and policies across all aspects.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4 mt-4">
                    <div class="background-white pb-4 h-100 radius-secondary"><img class="mb-4 radius-tr-secondary radius-tl-secondary" src="assets/images/portrait-7.jpg" alt="Profile Picture" />
                        <div class="px-4" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                            <div class="overflow-hidden">
                                <h5 data-zanim='{"delay":0}'>John Snow</h5></div>
                            <div class="overflow-hidden">
                                <h6 class="fw-400 color-7" data-zanim='{"delay":0.1}'>Business Analyst</h6></div>
                            <div class="overflow-hidden">
                                <p class="py-3 mb-0" data-zanim='{"delay":0.2}'>John has overseen the meteoric growth while protecting scaling its uniquely creative and culture.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4 mt-4">
                    <div class="background-white pb-4 h-100 radius-secondary"><img class="mb-4 radius-tr-secondary radius-tl-secondary" src="assets/images/portrait-1.jpg" alt="Profile Picture" />
                        <div class="px-4" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                            <div class="overflow-hidden">
                                <h5 data-zanim='{"delay":0}'>Ragner Lothbrok</h5></div>
                            <div class="overflow-hidden">
                                <h6 class="fw-400 color-7" data-zanim='{"delay":0.1}'>Business Consultant</h6></div>
                            <div class="overflow-hidden">
                                <p class="py-3 mb-0" data-zanim='{"delay":0.2}'>Ragner, SVP of Engineering, oversees MerrisCoop’s vast engineering organization which drives the core programming.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section> --}}
@endsection

{{-- scripts --}}
@section('scripts')
	
@endsection