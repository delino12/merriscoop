@extends('layouts.web-skin')

{{-- title --}}
@section('title')
	MerrisCoop | Terms and Conditions
@endsection

{{-- contents --}}
@section('contents')

    <section id="vision">
        <div class="container">
            <div class="row mb-4">
                <div class="col-md-12 mt-6 mt-sm-0">
                    <h3 class="text-center fs-2 fs-lg-3">FAQ </h3>
                </div>

                <div class="col-md-12 mt-6 mt-sm-0">
                    <div class="panel-group" id="accordion">
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                            Who is MerrisCoop?</a>
                          </h4>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse in">
                          <div class="panel-body">Answers here...</div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                            How can i apply for loan?</a>
                          </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse">
                          <div class="panel-body">Answers here...</div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                            What is MerrisCoop Savings?</a>
                          </h4>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse">
                          <div class="panel-body">Answers here...</div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>
@endsection

{{-- scripts --}}
@section('scripts')
	
@endsection