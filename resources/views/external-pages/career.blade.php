@extends('layouts.web-skin')

{{-- title --}}
@section('title')
	MerrisCoop | Careers
@endsection

{{-- contents --}}
@section('contents')
    <section class="background-11 ">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="overflow-hidden" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                        <h4 data-zanim='{"delay":0.1}'>
                            Join Our Innovative and Dynamic Team…
                        </h4>
                    </div>
                </div>
                <div class="col-lg-8">
                    <p><span style="font-weight: 400;">We are recruiting to fill the following position:</span></p>
                    <p>&nbsp;</p>
                    <p><strong>Job Title</strong><span style="font-weight: 400;">: Mobile Teller / Micro Savings Officer</span></p>
                    <p>&nbsp;</p>
                    <p><strong>Job Reference</strong><span style="font-weight: 400;">: MTMSO/MC/18</span></p>
                    <p><strong>Locations</strong><span style="font-weight: 400;">: Lagos (Lagos Island, Mushin, Ojo Alaba, International Trade fair Complex, Sanya, Oshodi) </span></p>
                    <p>&nbsp;</p>
                    <p><strong>Job Description:</strong></p>
                    <ul>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Prospect potential clients and follow up through regular contact with prospect.</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Generate new customers, maintain existing customers, sell our micro daily save financial products.</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Initiate ideas based on your interaction with customers on the field that would aid to developing products that will meet the needs of the target market.</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Manage a portfolio of Clients</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Grow savers savings/deposits portfolio in line with the targets set by the management by offering quality service.</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Identify clients&rsquo; needs in order to cross sell effectively.</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Manage the relationship with clients in the portfolio to ensure maximum clients satisfaction.</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">You will be responsible for mobilizing daily micro savings for the institution from our target market and customers.</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">You will also be responsible for developing the micro savings and deposits portfolio for the target market.</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">You will be responsible for posting into manifest sheet, customer&rsquo;s daily savings card, account, processing and monitoring daily savings/ deposits portfolio. </span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Organize an efficient schedule that will allow regular meetings with clients and maximized performance.</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">You will spend more than 70% of the time on the field performing visits to clients in business areas.</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Monitor the deposits of the clients under your supervision.</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Participate in the promotion of MerrisCoop products and services by informing prospects as well as clients.</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Accordingly answer clients&rsquo; questions and complaints related to MerrisCoop products and services.</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">You will be responsible for collecting loan repayment from our customers who opted for mobile teller&rsquo;s collection &nbsp;or daily repayment model as well as any other duties you would be assigned.</span></li>
                    </ul>
                    <p>&nbsp;</p>
                    <p><strong>Field Cash Collection Activities:</strong></p>
                    <ul>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Ensure all cash transactions are done according to MerrisCoop policies and procedure.</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Report any cash shortage or excess to the immediate superior or business manager.</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Create new customers and accounts on the field.</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Encourage clients to save daily/ make daily deposit on their micro save account.</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Set collection units or centres at then field to easy collections.</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Ensure customers deposits/ daily saves or money collected is accurately entered into customer&rsquo;s accounts. </span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Ensure new and existing customer accounts are probably created by adhering to MerrisCoop policies on KYC. </span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Report any suspicious transaction immediately to your superior.</span></li>
                    </ul>
                    <p>&nbsp;</p>
                    <p><strong>Organization and Control:</strong></p>
                    <ul>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Cash counts and controls- ensure and control the conformity of transaction with collected cash and monitoring tools.</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Ensure compliance of all operations performed with MerrisCoop policies and procedures.</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Share information and best practices with other Mobile Teller/ Mobile savings Officer.</span></li>
                    <li style="font-weight: 400;"><span style="font-weight: 400;">Report all the issues related to the job to the Team Leader.</span></li>
                    </ul>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p><strong>JOB REQUIREMENT</strong></p>
                    <p>&nbsp;</p>
                    <p><strong>Minimum Qualification </strong></p>
                    <p><span style="font-weight: 400;">NECO, WACE OND. </span></p>
                    <p><span style="font-weight: 400;">Applicants must not be older than 28 years of Age</span></p>
                </div>
                <div class="col-lg-4 text-center ml-auto mt-5 mt-lg-0">
                    <div class="row mt-5 px-2">
                        <div class="col">
                            <h5 class="mb-3">Related Articles</h5>
                            <div class="background-white pb-7 radius-secondary">
                                <div class="owl-carousel owl-theme owl-nav-outer owl-dot-round mt-4" data-options='{"items":1}'>
                                    <div class="item">
                                        <div class="background-white pb-4 h-100 radius-secondary"><img class="w-100 radius-tr-secondary radius-tl-secondary" src="http://markup.themewagon.com/tryelixir/assets/images/9.jpg" alt="Featured Image">
                                            <div class="px-4 pt-4"><a href="news.html"><h5>Tax impacts of lease mean accounting change</h5></a>
                                                <p class="color-7">By Paul O'Sullivan</p>
                                                <p class="mt-3">HMRC released a consultation document to flag some potential tax impacts that a forthcoming change...</p><a href="news.html#">Learn More &xrarr;</a></div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="background-white pb-4 h-100 radius-secondary"><img class="w-100 radius-tr-secondary radius-tl-secondary" src="http://markup.themewagon.com/tryelixir/assets/images/10.jpg" alt="Featured Image">
                                            <div class="px-4 pt-4"><a href="news.html"><h5>What brexit means for data protection law</h5></a>
                                                <p class="color-7">By Enrico Ambrosi</p>
                                                <p class="mt-3">Assuming that the referendum is not ignored completely, there are two possible futures for the UK...</p><a href="news.html#">Learn More &xrarr;</a></div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="background-white pb-4 h-100 radius-secondary"><img class="w-100 radius-tr-secondary radius-tl-secondary" src="http://markup.themewagon.com/tryelixir/assets/images/11.jpg" alt="Featured Image">
                                            <div class="px-4 pt-4"><a href="news.html"><h5>The growing meanace of social engineering fraud</h5></a>
                                                <p class="color-7">By Robson</p>
                                                <p class="mt-3">Social engineering involves the collection of information from various sources about a target...</p><a href="news.html#">Learn More &xrarr;</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row px-2 mt-5">
                        <div class="col">
                            <div class="background-white p-5 radius-secondary">
                                <h5>Tags</h5>
                                <ul class="nav tags mt-3 fs--1">
                                    <li><a class="btn btn-sm btn-outline-primary m-1 p-2" href="news.html#">Advisory</a></li>
                                    <li><a class="btn btn-sm btn-outline-primary m-1 p-2" href="news.html#">Finance</a></li>
                                    <li><a class="btn btn-sm btn-outline-primary m-1 p-2" href="news.html#">Ideas</a></li>
                                    <li><a class="btn btn-sm btn-outline-primary m-1 p-2" href="news.html#">Data</a></li>
                                    <li><a class="btn btn-sm btn-outline-primary m-1 p-2" href="news.html#">Market</a></li>
                                    <li><a class="btn btn-sm btn-outline-primary m-1 p-2" href="news.html#">Tax</a></li>
                                    <li><a class="btn btn-sm btn-outline-primary m-1 p-2" href="news.html#">Consulting</a></li>
                                    <li><a class="btn btn-sm btn-outline-primary m-1 p-2" href="news.html#">Accounting</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <a href="javascript:void(0);" onclick="showApplicationModal()">Click here to apply</a>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>

    <div class="modal" role="dialog" tabindex="-1" id="application-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    Fill in application information
                </div>
                <div class="modal-body">
                    <form method="post" action="{{ url('send/application/form') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Firstname</label>
                                    <input type="text" placeholder="Enter first name" name="firstname" class="form-control" required="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Lastname</label>
                                    <input type="text" placeholder="Enter last name" name="lastname" class="form-control" required="">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" placeholder="Enter your email address" name="email" class="form-control" required="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Mobile</label>
                                    <input type="text" maxlength="11" placeholder="Enter mobile number" name="mobile" class="form-control" required="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Describe more about your self</label>
                                    <textarea placeholder="Tell us about your self..." name="description" class="form-control" required=""></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Address</label>
                                    <textarea placeholder="Enter address here..." name="address" class="form-control" required=""></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Attach a CV <span class="small">(Only docs format)</span>
                                    </label>
                                    <input type="file" class="form-control" name="resume" required="">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <button class="btn btn-primary">
                                        Submit Application
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- scripts --}}
@section('scripts')
    @if(session('error'))
        <script type="text/javascript">
            swal(
                "Oops",
                "{{ session('error') }}",
                "error"
            );
        </script>
    @endif

    @if (session('success'))
        <script type="text/javascript">
            swal(
                "Sent!",
                "{{ session('success') }}",
                "success"
            );
        </script>
    @endif
	<script type="text/javascript">
        function showApplicationModal() {
            $("#application-modal").modal();        
        }   
    </script>
@endsection