@extends('layouts.web-skin')

{{-- title --}}
@section('title')
	MerrisCoop | Contact Us
@endsection

{{-- contents --}}
@section('contents')
    <section class="background-11">
        <div class="container">
            <div class="row align-items-stretch justify-content-center">
                <div class="col-lg-4 mb-4 mb-lg-0">
                    <div class="h-100 px-5 py-4 background-white radius-secondary">
                        124/140, Taiwo Villa,
                        <br>Ojo Road,
                        <br>Ajegunle Apapa, Lagos
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="h-100 px-5 py-4 background-white radius-secondary">
                        <h5>Socials</h5><a class="d-inline-block mt-2" href="contact.html#"><span class="fa fa-linkedin-square fs-2 mr-2 color-primary"></span></a><a class="d-inline-block mt-2" href="contact.html#"><span class="fa fa-twitter-square fs-2 mx-2 color-primary"></span></a><a class="d-inline-block mt-2" href="contact.html#"><span class="fa fa-facebook-square fs-2 mx-2 color-primary"></span></a><a class="d-inline-block mt-2" href="contact.html#"><span class="fa fa-google-plus-square fs-2 ml-2 color-primary"></span></a></div>
                </div>
                <div class="col-12 mt-4">
                    <div class="background-white p-5 h-100 radius-secondary">
                        <h5>Write to us</h5>
                        <form class="zform mt-3" method="post" onsubmit="return sendContactUs()">
                            <div class="row">
                                <div class="col-12">
                                    <input class="form-control background-white" id="c-name" type="text" placeholder="Your Name" required>
                                </div>
                                <div class="col-12 mt-4">
                                    <input class="form-control background-white" type="email" id="c-email" placeholder="Email" required>
                                </div>
                                <div class="col-12 mt-4">
                                    <textarea class="form-control background-white" id="c-message" rows="11" placeholder="Enter your descriptions here..." required></textarea>
                                </div>
                                <div class="col-12 mt-4">
                                    <div class="row">
                                        <div class="col-auto">
                                            <button class="btn btn-md-lg btn-primary" id="c-btn" type="Submit">
                                            	<span class="color-white fw-600">Send Now</span>
                                            </button>
                                        </div>
                                        <div class="col">
                                            <div class="zform-feedback"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>
@endsection

{{-- scripts --}}
@section('scripts')
	<script type="text/javascript">
		// send contact message
		// contact merriss for a meeting
        function sendContactUs(){
            $("#c-btn").html("Sending....");

            var token   = '{{ csrf_token() }}';
            var name    = $("#c-name").val();
            var email   = $("#c-email").val();
            var message = $("#c-message").val();

            var params  = {
                _token: token,
                name: name,
                message: message,
                email: email
            }


            $.post('{{ url('client/contact-us') }}', params, function(data, textStatus, xhr) {
                /*optional stuff to do after success */
                if(data.status === "success"){
                    swal(
                        "ok",
                        data.message,
                        data.status
                    );

                    // reset form
                    $(".zform")[0].reset();
                }else{
                    swal(
                        "oops",
                        data.message,
                        data.status
                    );
                }

                // submit 
                $("#c-btn").html("Submit");
            });

            // void
            return false;
        }
	</script>
@endsection