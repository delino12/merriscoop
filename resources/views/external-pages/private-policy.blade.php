@extends('layouts.web-skin')

{{-- title --}}
@section('title')
	MerrisCoop | Services
@endsection

{{-- contents --}}
@section('contents')

<div class="container">
	<div style="height: 110px;"></div>
	<div class="row">
		<div class="col-md-12">
			<p><strong>MerrisCoop Private Policy</strong></p>
<p><span style="font-weight: 400;">It&lsquo;s important to </span><strong>MerrisCoop Nigeria</strong><span style="font-weight: 400;"> to help our customers retain their privacy when they take advantage of all the internet has to offer through our platform.</span></p>
<p><span style="font-weight: 400;">We believe your privacy shouldn&rsquo;t be shared with someone else. Your privacy is important to you and to us. So we&rsquo;ll protect the information you share with us. </span><strong>MerrisCoop Nigeria</strong> <span style="font-weight: 400;">respects and protects the privacy of its members and customers as specified in the related laws of the Federal Republic of Nigeria.</span></p>
<p><span style="font-weight: 400;">To protect your privacy, </span><strong>MerrisCoop Nigeria</strong><span style="font-weight: 400;"> follows different principles in accordance with worldwide practices for customer privacy and data protection. </span></p>
<p><span style="font-weight: 400;">We won&rsquo;t sell or give away your name, mail address, phone number, email address, or any other information to anyone. We do not provide any personally identifiable user information to third party websites.</span></p>
<p><span style="font-weight: 400;">We will use state- of- the- art security measure to protect your information from unauthorized users.</span></p>
<p><strong>NOTICE</strong></p>
<p><span style="font-weight: 400;">We will ask you when we need information that personally identifies you (personal information) or allows us to contact you. Generally, this information is requested when you create a customer ID on the site, our App or when you order email newsletter, subscribe for other financial products or services on </span><strong>MerrisCoop Nigeria.</strong></p>
<p><span style="font-weight: 400;">We use your personal information for the following purposes:</span></p>
<ul>
<li style="font-weight: 400;"><span style="font-weight: 400;">To help you easily find information.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">To make the site easier for you to use by not having to enter information more than once.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">To help us create content most relevant to you.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">To alert you of special offers, promotions, update information and other new services or products.</span></li>
<li style="font-weight: 400;"><strong>MerrisCoop Nigeria</strong><span style="font-weight: 400;"> can use information about some of or all the customers for statistical purpose for its related operations.</span></li>
</ul>
<p><strong>CONSENT</strong></p>
<p><span style="font-weight: 400;">If you choose not to register or provide personal information, you can still browse most of the site, blog and social media windows. But you will not be able to access areas that require registration.</span></p>
<p><span style="font-weight: 400;">If you decide to register, you will be able to select the kinds of information you want to receive from us by subscribing to various services, like news letter. If you do not want us to communicate with you about other offers regarding products, services, events, by email, telephone, postal mail you may select the option stating that you do not wish to receive marketing message from us.</span></p>
<p><strong>ACCESS</strong></p>
<p><span style="font-weight: 400;">We will provide you with the means to ensure that your personal information is correct and current. You review and update this information at any time at your account centre. There, you can:</span></p>
<ul>
<li style="font-weight: 400;"><span style="font-weight: 400;">View and edit selected personal information you have already given us.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">Tell us whether you want us to send you marketing information.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">Sign up for electronic newsletter about our services and products.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">Once you register, you won&rsquo;t need to do it again wherever you go on the site; your information stays with you.</span></li>
</ul>
<p><strong>SECURITY</strong></p>
<p><strong>MerrisCoop Nigeria</strong><span style="font-weight: 400;"> has taken strong measure to protect the security of your personal information and to ensure that your choice for its intended use is honored. We take strong precautions to protect your data from loss, misuse, unauthorized access, or disclosure, alteration or destruction.</span></p>
<p><span style="font-weight: 400;">We guarantee your transactions to be 100% safe and secure. When you make registration or access your personal account information, you&lsquo;re utilizing secure and reliable server software SSL, which encrypts your personal information before it&rsquo;s sent over to the internet. SSL is one of the safest encryption technologies available.</span></p>
<p><span style="font-weight: 400;">Your personal information is never shared outside the company without your permission, except under the conditions explained below. Inside the company, data are stored in password &ndash;controlled servers or cloud with limited access. Your information may be stored and processed in Nigeria or any other country where MerrisCoop, its subsidiaries, business associates, affiliates or agents are located.</span></p>
<p><span style="font-weight: 400;">You also have a significant role in protecting your information. No one can see or edit your personal information without knowing your user name, password, so do not share these with others.</span></p>
<p><strong>ENFORCEMENT</strong></p>
<p><span style="font-weight: 400;">If for one reason you believe </span><strong>MerrisCoop Nigeria</strong><span style="font-weight: 400;"> has not adhered to these principles, please notify us by email at </span><a href="mailto:info@merriscoop.com"><span style="font-weight: 400;">info@merriscoop.com</span></a><span style="font-weight: 400;"> &nbsp;and we will do our best to determine and correct the problem promptly. Be sure the words PRIVACY POLICY are in the subject line.</span></p>
<p>&nbsp;</p>
<p><strong>WHAT WE DO WITH THE INFORMATION YOU SHARE</strong></p>
<p><span style="font-weight: 400;">When you join us, you provide us with your personal information, contact information, including your name and email address and others data. We use this information to send you update about your status, questionnaire to measure your satisfaction with our services and announcement about new and existing product or services which we offer. We will ask for your bank account details. We use this information only to transfer your daily micro savings, nano loans, micro loans or capital raised to finance your business or for personal needs. For convenience, we do save this information in case you want to deal with us again, but we don&rsquo;t use this information again without your permission.</span></p>
<p>&nbsp;</p>
<p><strong>MerrisCoop</strong> <strong>Nigeria</strong><span style="font-weight: 400;"> will disclose your personal information, without notice, only if required to do so by the law or in the good faith belief that such action is necessary to:</span></p>
<ol>
<li style="font-weight: 400;"><span style="font-weight: 400;">Conform to the edict of the law or comply with legal process served on</span><strong> MerrisCoop</strong><span style="font-weight: 400;"> or the site.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">Protect and defend the rights or property of </span><strong>MerrisCoop</strong> <strong>Nigeria</strong><span style="font-weight: 400;"> and its family of websites.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400;">Act in urgent circumstances to protect the personal safety of users of </span><strong>MerrisCoop Nigeria</strong><span style="font-weight: 400;">, its websites, or the public.</span></li>
</ol>
<p><span style="font-weight: 400;">Any dispute over privacy is subject to our terms of service, including resolution of dispute, limitations on damages and applicable laws of the Federal Republic of Nigeria.</span></p>
<p><span style="font-weight: 400;">If you have any concern about privacy at MerrisCoop Nigeria, please contact us on </span><span style="font-weight: 400;">info@merriscoop</span><a href="about:blank"><span style="font-weight: 400;">.com</span></a></p>
<p><br /><br /></p>
		</div>
	</div>
</div>
@endsection

{{-- scripts --}}
@section('scripts')
	
@endsection