@extends('layouts.web-skin')

{{-- title --}}
@section('title')
	MerrisCoop | Services
@endsection

{{-- contents --}}
@section('contents')
    <section id="our-services">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mb-4 mt-sm-0">
                    <h3 class="text-center fs-2 fs-lg-3">Our Services</h3>
                </div>
                <div class="col-md-6 mt-4 mt-sm-0">
                    <ul>
                        <li class="mb-4">
                            <h5>Cash Deposit</h5>
                            Driven by our customer philosophy, the customer always comes first. We are customer- centric organization. The customer is the driving force of our entire product.
                        </li>
                        <li class="mb-4">
                            <h5>Cash Withdrawal</h5>
                            Integrity is central in all our operations. Compliance with regulations and the host communities is primary while decisions are based purely on business model and ethics. 
                        </li>
                        <li class="mb-4"><h5>Fund Transfer</h5>
                            We pay thorough attention to details and strive at all times to attain and exceed the highest possible standards and expectations.</li>
                        <li class="mb-4">
                            <h5>Balance Enquiry</h5>
                            Innovation is core to our business.  We leverage technology to automated processes, develop unique platforms, create new financials products, systems, design procedures that are consistent and enduring. 
                        </li>
                    </ul>
                </div>
                <div class="col-md-6 mt-6 mt-sm-0">
                    <ul>
                        <li class="mb-4">
                            <h5>Bills Payment</h5>
                            At MerrisCoop, we hold the interest of team above those of individuals, while showing mutual respect for all employees and sharing information throughout our organization.
                        </li>

                        <li class="mb-4"><h5>Merris Alert</h5>
                            We understand conflict of interest is detrimental to our business. As an organization, we are answerable to our stakeholders on all actions and results. 
                        </li>
 
                    </ul>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>
@endsection

{{-- scripts --}}
@section('scripts')
	
@endsection