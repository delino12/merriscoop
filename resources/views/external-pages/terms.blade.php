@extends('layouts.web-skin')

{{-- title --}}
@section('title')
	MerrisCoop | Terms and Conditions
@endsection

{{-- contents --}}
@section('contents')

    <section id="vision">
        <div class="container">
            <div class="row mb-4">
                <div class="col-md-12 mt-6 mt-sm-0">
                    <h3 class="text-center fs-2 fs-lg-3">TERMS AND CONDITIONS </h3>
                </div>

                <div class="col-md-12 mt-6 mt-sm-0">
                    <p>
                         Welcome to MerrisCoop Nigeria!
                    </p>

                    <h4>Terms of Use</h4>
                    <p>These terms of use (“Terms of Use”) are provided on behalf of MerrisCoop Nigeria and its affiliates (“we, “us”, “our”) By using www.merriscoop.com, including any page thereof (the “site”), you acknowledge that you have read, understand and agree to be bound by these Terms of Use, regardless of whether you are a registered User of our products or services (each a “Service” collectively, referred to as “service”). If you do not wish to be bound by these Terms of Use, you should not access or use the Site or Services. If there is any inconsistency between these Terms of Use and another agreement you enter into that is applicable to Service offered on the Site then the other agreement will take precedence as it applies to the services. <br />

                    Change to These Terms of Use
                    We reserve the right, in our sole discretion, to modify these Terms of Use at any time. Changes to these Terms of Use will become effective immediately upon the posting thereof. Please review these Terms of Use often to keep your self-apprised of any changes. Your continued use of the Site following the posting of changes will constitute your acceptance of the revised Terms of Use.
                    </p>
                    <br />

                    <h4>Eligibility</h4>
                    <p>This Site is intended solely for users who are 18 years of age or older, and any registration or use of the Site by anyone under 18 is unauthorized and in violation of these Terms of Use. By using the Site, you represent you are 18 or older and that you agree to and to abide by all of the terms and conditions of these Terms of Use. If you violate any of these Terms of Use, or violate any other agreement with us, we may terminate your registration and/ or prohibit you from using or accessing our Services of Site.
                    </p>
                    <br />


                    <h4>Registrations and Applications</h4>
                    <p>You may be presented with the opportunity to register for an account and apply for a loan through the site. When you register for an account, scheme or apply for a loan, you agree to provide current, complete and accurate information about yourself. If any information you provide is untrue, inaccurate, not current, or in complete, we have the right to reject or cancel your account registration, reject any application you have submitted, terminated any agreement we have with you, and restrict your future use of the Site and our Service. We reserve the right to decline any application for a loan.
                    </p>
                    <br />

                    <h4>General Practices Regarding Use and Storage</h4>
                    <p>You acknowledge that MerrisCoop Nigeria may establish general practices and limits concerning use of the Services, including without limitation the maximum period of time that data or other content will be retained by the Service and the maximum storage space that will be allotted on MerrisCoop’s server on your behalf. You agree that MerrisCoop has no responsibility or liability for the deletion of or failure to store any data or other content maintained by or uploaded to the Services. You further acknowledge that MerrisCoop reserves the right to change these general practices and limits at any time, in our sole distraction, with or without notice.
                    </p>
                    <br />

                    <h4>Mobile Services</h4>
                    <p>The Services include certain services that may be available via a mobile device, including (i) the ability to submit information or upload content to the Services via mobile device and (ii) the ability to browse the Services and the Site from a mobile device (Collectively, the “ Mobile Services”). To the extent you access the Services through a mobile device, your wireless service carrier’s standard charges, data rates and other fees may apply. In addition, using certain Mobile Service may be prohibited or restricted by carrier, and not all Mobile Service may work with all carriers or devices. By using the Mobile Services and/ or providing your mobile telephone number to us through the Service, you agree that we may communicate with you regarding MerrisCoop, our products and/or our service providers by SMS, text message or other electronic means to your mobile device and that certain information about your usages of the Mobile Services may be communicated to us. In the event you change or deactivate your mobile telephone number, you agree to promptly update information that you have submitted to us to ensure that your messages are not sent to the person acquires your old number.
                    </p>
                    <br />

                    <h4>Electronic Notices</h4>
                    <p>By using the Services, you agree that we may communicate with you electronically via email regarding security, privacy and administrative issues relating to your use of the Services. If we learn of a security system’s breach, we may attempt to notify you electronically by posting a notice on the Service or sending an email to you. You may have a legal right to receive this notice in writing. To receive free written notice of a security breach (or to withdraw your consent from receiving electronic notice). Please notify us at info@merriscoop.com
                    </p>
                    <br />

                    <h4>Your Account</h4>
                    <p>To access our Services, you will be required to register and create an account on the site. You may not disclose your user name or password to any third party. If you learn of any unauthorized use of your password or account, please contact us immediately. You may be required to login in order to use certain parts of the Site.
                    </p>
                    <br />


                    <h4>Your Content and Information Sharing</h4>
                    <p>By providing information or content on the Site, you expressly agree to all of the following: You grant us a non- exclusive, transferable, sub-licensable, royalty-free, world- wide license to use any information or content that you provide in connection with your use of the Site and Services, subject to the privacy provisions described in our Privacy Policy. We have the right to review, delete, edit, modify, reformat, excerpt, or translate any of your information or content. You are solely responsible for the content and information you make available through or in connection with our services.
                    All the information and content posted on the Site or privately transmitted through the Site or via other means in connection with our services is the sole responsibility of the person from which that content originated. We will not be responsible for any errors or omission in any information or content posted by a user.
                    We will not share or sell any of your information with any third party, except as specifically described in our Privacy Policy, Which is incorporated by reference into these Terms of Use.
                    </p>
                    <br />

                    <h4>Restriction on Use</h4>
                    <p>You agree to abide by all applicable laws and regulations in your use of the Site and our Service. In addition,
                    You agree that you will not do any of the following:
                    </p>
                    <br />

                    <ul>
                        <li>register for more than one account, or register for an account on behalf of an individual other than yourself or on behalf of any group or entity;</li>
                        <li>post or otherwise make available content, or take any action on the Site, that may constitute libel or slander or that infringes or violates someone else’s rights or is protected by any copyright or trademark, or otherwise violates the law;</li>
                        <li>Post or otherwise make available content that in our judgment is objectionable, such as content that is harmful, threatening, inflammatory, obscene, fraudulent, invasive of privacy or publicity rights, hateful or otherwise objectionable, or which restricts or inhibits any other person using or enjoying the Site, or which may expose us of user to any harm or liability of any type;</li>
                        <li>Post or otherwise make available any unsolicited or unauthorized advertising, solicitations, promotional materials, or any other form of solicitation;</li>
                        <li>Use the information or content on our Site to send unwanted messages to any user;</li>
                        <li>Impersonate any person or entity, or falsely state or otherwise misrepresent yourself, your age or your affiliation with any person or entity;</li>
                        <li>Post or otherwise make publicly available on the Site any personal or financial information of any third party;</li>
                        <li>Solicit personal information from anyone under 18 or solicit password or personally identifying information for commercial or unlawful purpose;</li>
                        <li>use the Site or our Services in any manner that could damage, disable, overburden or impair the Site;</li>
                        <li>harvest or collect email addresses or other contact information of our users from the Site by electronic or other means, including via the use automated scripts; or </li>
                        <li>post or otherwise make available any material that contains software viruses or any other computer code, file or programs designed to interrupt, destroy or limit the functionality of any computer software or hardware or telecommunication equipment;</li>
                    </ul>
                    <br />

                    <h4>Intellectual Property Proprietary Right; Copyright; Trademarks</h4>
                    <p>All software, design, text information, data, databases, images, photographs, illustrations, audio clips, video clips, artwork, graphic material, or other copyrightable element(collectively, “content”), other than user content (as defined above), are the property of MerrisCoop Nigeria and /or its subsidiaries, affiliates, assigns, licensors, vendor, partners or other respective owners and are protected, without limitation, pursuant to copyright laws. No Content (other than your own User Content) may be reproduced modified, used to create derivatives works, displayed, performed, published, distributed, disseminated, broadcast or circulated to any third party without our express prior written consent.<br />
                        MerrisCoop Nigeria and all related logos (collectively, “the Trademarks”) constitute our trademarks or service mark. Other company, product, and services names and logos used and displayed on this site may be trademarks or service marks owned by us or others. You may not use copy, distribute, display, reproduce or modify any of the trademarks found on the Site unless in accordance with written authorization by us. The use of any of the Trademark as part of a link is prohibited unless we provide advance written approval in addition, all page headers, button icon, custom graphics, and scripts are our service mark, trademarks, and or trade dress. You may not imitate copy or use them without our prior written consent. Any questions concerning any trade marks, or whether any mark or logo is a trademark, should be referred to us.
                    </p>
                    <br />

                    <p>
                    All right, title and interest in and to the Website, any content thereon, our products and services, the technology related to our products and services, and any and all technology and any content created or derived from any of the foregoing is our or our licensors’ exclusive property.
                    </p>
                    <br />


                    <h4>Copyright Complaints</h4>
                    <p>If you believe that any material on the Site infringes upon any copyright that you own or control, you may send a written notification to us via NIPOST Postal Services. In your notification please:</p>
                    <ul>
                        <li>Confirm you are the owner, or authorized to act on behalf of the owner, of the copyright work that has been infringed.</li>
                        <li>Identify the copyright works you claim has been infringed.</li>
                        <li>Identify the material that you claim is infringing or is the subject of infringing activities and that is to removed; please include information reasonably sufficient to permit us to locate the material;</li>
                        <li>Provide your contact details, including an email address; and</li>
                        <li>Proving a statement that the information you have provided is accurate and that you have a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent nor the law.</li>
                    </ul>
                    <br />


                    <h4>Link to Third Party Websites</p>
                    <p>We are not responsible for the information practice employed by sites linked ti or from our website. In most cases, linked to third-party website are provided solely as pointer to information on topics that may be useful to our users. Since third-party web sites may have differed privacy policies and /or security standards governing their sites, we advise you to review the privacy policies and terms and conditions of these sites prior to providing any personal information.
                    </p>
                    <br />


                    <h4>Use of Personally Identification Information</h4>
                    <p>Our practice and policies with respect to the collection and use of personally identifiable information are governed by our Privacy Policy.
                    </p>
                    <br />

                    <h4>NO Warranty, Errors, Disclaimers</h4>
                    <p>The site and our products and services are provided “as is” and without any representation or warranty, whether express, implied or statutory. Any estimates or example that we provided on the Site may be different than actual amounts. You agree that we may promptly correct any error that we discover, including any error in calculating your loan rates. If the error results in your receipt of an incorrect interest rate, APR or fee, we will notify you and provide you with the correct rate. You agree to provide any additional consent necessary to correct any error that occur</p>
                    <br /><br />


                    <strong>ALTHOUGH, WE WILL USE REASONABLE EFFORTS TO PROVIDE AN ACCURATE SITE/SERVICE. ALL ASPECTS THEREOF ARE PROVIDED “AS IN” ‘WITH ALL FAULTS’ AND “AS AVAILABLE” WE AND OUR OFFICERS, DIRECTORS, EMPLOYEES AND THIRD PARTY SUPPLIERS (COLLECTIVELY, THE “PARTIES”) DISCLAIM ANY AND ALL REPRERSENTATATIONS, WARRANTIES OR GUARANTEES OF ANY KIND, WHETHER EXPRESS, IMPLIED OR STATOTORY, THE SITE, SERVICES, ANY DOCUMENTATATION PROVIDED OR MADE AVAILABLE TO YOU ANY OTHER PRODUCTS AND RELATED MATERIALS AND/OR SERVICES PROVIDED TO YOU BY ANY OF THE PARTIES INCLUDING, BUT NOT LIMITED TO, ANY WARRANTIES: (1) AS TO TITLE, MERCAHNTABILITY, FITNESS FOR ORDINARY PURPOSES, FITNESS FOR A PARTICULAR PURPOSE, NON INFRINGEMENT, SYSTEM INTEGRETTION AND WORKAMANLIKE EFFORT;(11) THE QUALITY, ACCURACY, TMELINESS OR COMPLETENESS OF THE SITE OR SERVICES OR ANY ASPECT THEREOF; (111) THOSE ARISING THROUGH COURSE OF DEALING, COURSE OF PERFOAMANCE OR USAGE OF TRADE; (1V) THE SITE OR SERVICES CONFORMING TO ANY FUNCTION, DEMOSTRATION OR PROMISE BY ANY PARTY, AND (V) THAT ACCESS TO OR USE OF THE SITE OR SERVICE WILL BE UNINTERRUPTED, ERROR, FREE OR COMPLETELY SECURE. ANY RELIANCE UPON THE SITE AND /OR SERVICES IS AT YOUR OWN RISK AND THE PARTIES MAKE NO WARRANTIES. THESE DISCLAIMERS ARE INDEPENDENT OF ANY OTHER TERM IN THESE TERMS OF USE.
                    </strong>
                    <br /><br />


                    <h4>Limitation of Liability</h4>
                    <p>You agree that all access and use of the Site and its contents and your use of the Services is at your own risk. Neither we, nor any third party involved in creating, producing or delivering the Site and /or Services, has or will have any responsibility for any consequences relating, directly or indirectly, to any action or inaction that you may take based on the Site and/ or Services, or any aspect thereof.</p>
                    <br /><br />

                    <strong>We WILL NOT BE HELD LIABLE FOR ANY DEFECTS, FAULTS, INTERRUPTIONS OR DELAYS IN THE OPERATION OR TRANSMISSION, PRODUCT, AND/OR OUT OF ANY INACCURACIES, ERRORS OR OMISSIONS IN THE INFORMATION CONTAINED IN THE SITE/SERVICES. UNDER NO CIRCUMTANCES WILL ANY OF THE PARTIES BE HELD LIABLE FOR ANY INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL, EXEMPLARY, PUNITIVE OR ANY OTHER DEMAGES, INCLUDING, BUT NOT LIMITED TO, LOST PROFITS, SRISING OUT OF, BASED ON, RESULTING FROM, OR IN CONNECTION WITH THE SITE/SERVICE OR PRODUCT, THESE TERMS OF USE OR YOUR USE OR INABILITY TO USE ANY OF THE FOREGOING, EVEN IF WE HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. ALL THESE LIMITATIONS APPLY REGARDLESS OF THE CAUSE OR FORM OF ACTION, WHETHER THE DAMAGES ARE CLAIMED UNDER THE TERMS OF A CONTRACT, TORT OR OTHERWISE AND EVEN IF WE OR OUR REPRESENTATIVES HAVE BEEN NEGLIGENT OR HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
                    </strong>
                    <br /><br />

                    <h4>Indemnity and Release</h4>
                    <p>You agree to release, indemnify, defend and hold MerrisCoop and our affiliates and our and their officers, employees, directors and agents harmless from any form and against any and all third party actions, suits, claims and/or demands and any associated losses, expenses, damages, costs and other liabilities (including reasonable attorney’s fees), arising out of or relating to your (and your users’) submitted content, use or misuse of any aspect of the Services or Site, or your violation of these Terms of Use. You will cooperate as fully as reasonably required in the defense of any such claim or damage. We and any third party involved in creating, producing or delivering the Site/Services reserves the right to assume the exclusive defense and control of any matter otherwise subject to indemnification by you, at your expense, and you will not in any event settle any such matter without our prior written consent and any such third party.
                    </p>
                    <br />

                    <h4>Miscellaneous</h4>
                    <p>These Terms of Use shall be governed in all respects by the laws of the Federal Republic of Nigeria and applicable 
                    </p>
                    <br />

                    <h4>State Law. </h4>
                    <p>Your obligations under these Terms of Use are binding on your successors, legal representatives and assigns. You may not assign or transfer (by operation of law or otherwise) your right to use the Site/ Services or any aspect here under, in whole or in part, without our prior written consent.</p>
                    <br />

                    <h4>Severability</h4>
                    <p>If any provision of this Terms and Conditions is found to be invalid or unenforceable, the remaining provisions shall be enforced to the fullest extent possible, and the remaining provision of the Agreement shall remain in full force and effect.</p>
                    <br />


                    <h4>Questions? Concerns? Suggestions?</h4>
                    <p>Please contact us at info@merriscoop.com to report any violations of these Terms of Service or to pose any question regarding these Terms of Use or the Services.
                    </p>
                    <br />
                    </p>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>
@endsection

{{-- scripts --}}
@section('scripts')
	
@endsection