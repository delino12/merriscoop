@extends('layouts.web-skin')

{{-- title --}}
@section('title')
	MerrisCoop | Complaints
@endsection

{{-- contents --}}
@section('contents')
    <section id="vision">
        <div class="container">
            <div class="row mb-4">
                <div class="col-md-12 mt-6 mt-sm-0">
                    <h3 class="text-center fs-2 fs-lg-3">Issues & Resolution Center </h3>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>
@endsection

{{-- scripts --}}
@section('scripts')
	
@endsection