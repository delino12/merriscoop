@extends('layouts.web-skin')

{{-- title --}}
@section('title')
	MerrisCoop | Savings
@endsection

{{-- contents --}}
@section('contents')
    <section id="savings">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mb-4 mt-sm-0">
                    <h3 class="text-center fs-2 fs-lg-3">Micro Savings</h3>
                </div>
                <div class="col-md-6 mt-4 mt-sm-0">
                    <ul>
                        <li class="mb-4">
                            <h5>Spare Change Savers</h5>
                            Driven by our customer philosophy, the customer always comes first. We are customer- centric organization. The customer is the driving force of our entire product.
                        </li>
                        <li class="mb-4">
                            <h5>Merriscoop Pay</h5>
                            Integrity is central in all our operations. Compliance with regulations and the host communities is primary while decisions are based purely on business model and ethics. 
                        </li>
                        <li class="mb-4"><h5>Ajo pro</h5>
                            We pay thorough attention to details and strive at all times to attain and exceed the highest possible standards and expectations.</li>
                        <li class="mb-4">
                            <h5>Ajo Regular</h5>
                            Innovation is core to our business.  We leverage technology to automated processes, develop unique platforms, create new financials products, systems, design procedures that are consistent and enduring. 
                        </li>
                        
                    </ul>
                </div>
                <div class="col-md-6 mt-6 mt-sm-0">
                    <ul>
                        <li class="mb-4">
                            <h5>Festival Savers Scheme</h5>
                            At MerrisCoop, we hold the interest of team above those of individuals, while showing mutual respect for all employees and sharing information throughout our organization.

                        </li>

                        <li class="mb-4"><h5>Kiddie Piggy Box</h5>
                            We understand conflict of interest is detrimental to our business. As an organization, we are answerable to our stakeholders on all actions and results. </li>
                        <li class="mb-4"><h5>Association Micro Target Savers</h5>
                            We believe that our organization and societies in which we do business are in a symbolic relationship. This is why we have taken Corporate Social Responsibility as core to our existence and would initiate several programmes to our host communities.</li>
                        
                        <li class="mb-4">
                            <h5>Micro Team Savers</h5> 
                            No tolerance for behavior that is inconsistent with these values.
                        </li>
                        <li class="mb-4">
                            <h5>Pilgrimage Micro Savers</h5> 
                            No tolerance for behavior that is inconsistent with these values.
                        </li>
                    </ul>
                </div>

                <div class="col-md-12 mt-6 mt-sm-0 text-center">
                	<a class="btn btn-primary mr-3 mt-3" href="{{ asset('contact') }}">Start Investing with MerrisCoop Today, Apply Now<span class="fa fa-chevron-right ml-2"></span>
                                            </a>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>
@endsection

{{-- scripts --}}
@section('scripts')
	
@endsection