@extends('layouts.web-skin')

{{-- title --}}
@section('title')
	MerrisCoop | Lending
@endsection

{{-- contents --}}
@section('contents')
    <section class="text-center">
        <div class="container">
            <h4>Ways to Borrow From Us</h4>
            <p>Connect with us on any of these lending channels</p>
            <div class="row">
                <div class="col-md-6 col-lg-3" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                    <div class="px-3 py-4 px-lg-4">
                        <div class="overflow-hidden"><span class="icon-Wallet-2 fs-3 color-primary fw-600" data-zanim='{"delay":0}'></span></div>
                        <div class="overflow-hidden">
                            <h5 class="mt-3" data-zanim='{"delay":0.1}'>Online Loans</h5></div>
                        <div class="overflow-hidden">
                            <p class="mb-0" data-zanim='{"delay":0.2}'>We transform brands, grow businesses, and tell brand and product stories in a most creative way.</p>
                            <a class="btn btn-primary mr-3 mt-3" href="{{ asset('contact') }}">Apply Now<span class="fa fa-chevron-right ml-2"></span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                    <div class="px-3 py-4 px-lg-4">
                        <div class="overflow-hidden"><span class="icon-Cloud-Tablet fs-3 color-primary fw-600" data-zanim='{"delay":0}'></span></div>
                        <div class="overflow-hidden">
                            <h5 class="mt-3" data-zanim='{"delay":0.1}'>Mobile Loans</h5></div>
                        <div class="overflow-hidden">
                            <p class="mb-0" data-zanim='{"delay":0.2}'>We cover a large range of creative platforms and digital projects with one purpose: to create experiences.</p>
                            <a class="btn btn-primary mr-3 mt-3" href="{{ asset('contact') }}">Apply Now<span class="fa fa-chevron-right ml-2"></span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                    <div class="px-3 py-4 px-lg-4">
                        <div class="overflow-hidden"><span class="icon-Affiliate fs-3 color-primary fw-600" data-zanim='{"delay":0}'></span></div>
                        <div class="overflow-hidden">
                            <h5 class="mt-3" data-zanim='{"delay":0.1}'>Social Media</h5></div>
                        <div class="overflow-hidden">
                            <p class="mb-0" data-zanim='{"delay":0.2}'>We guide you through the pipelines that generate new products with higher potential and lower risk.</p>
                            <a class="btn btn-primary mr-3 mt-3" href="{{ asset('contact') }}">Apply Now<span class="fa fa-chevron-right ml-2"></span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                    <div class="px-3 py-4 px-lg-4">
                        <div class="overflow-hidden"><span class="icon-Bank fs-3 color-primary fw-600" data-zanim='{"delay":0}'></span></div>
                        <div class="overflow-hidden">
                            <h5 class="mt-3" data-zanim='{"delay":0.1}'>Visit any of our Offices</h5></div>
                        <div class="overflow-hidden">
                            <p class="mb-0" data-zanim='{"delay":0.2}'>
                               
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br /><br /><br />
        <div class="container">
            <h4 class="mt-4">What We Offer</h4>
            <p>MerrisCoop got your financial needs covered, we offer a variety of loans.</p>
            <p>Choose a loan type that suit needs and apply</p>
            <div class="row">
                <div class="col-md-6 col-lg-6" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                    <div class="px-3 py-4 px-lg-4">
                        <div class="overflow-hidden"><span class="icon-Wallet-2 fs-3 color-primary fw-600" data-zanim='{"delay":0}'></span></div>
                        <div class="overflow-hidden">
                            <h5 class="mt-3" data-zanim='{"delay":0.1}'>Salary Overdraft</h5></div>
                        <div class="overflow-hidden">
                            <p class="mb-0" data-zanim='{"delay":0.2}'>We provide energy-efficient and environmentally conservative solutions to our clients to boost their business.</p>
                            <a class="btn btn-primary mt-3" href="{{ asset('contact') }}">
		                    	Apply Now<span class="fa fa-chevron-right ml-2"></span>
		                    </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                    <div class="px-3 py-4 px-lg-4">
                        <div class="overflow-hidden"><span class="icon-Money-2 fs-3 color-primary fw-600" data-zanim='{"delay":0}'></span></div>
                        <div class="overflow-hidden">
                            <h5 class="mt-3" data-zanim='{"delay":0.1}'>Salary Advance</h5></div>
                        <div class="overflow-hidden">
                            <p class="mb-0" data-zanim='{"delay":0.2}'>Based on solid strategic framework and real, relevant research, we create prototypes, not presentations.</p>
                            <a class="btn btn-primary mt-3" href="{{ asset('contact') }}">
		                    	Apply Now<span class="fa fa-chevron-right ml-2"></span>
		                    </a>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->

        <br /><br /><br />
        <div class="container">
            <h4 class="mt-4">Personal & Emergency Loans</h4>
            <div class="row">
                <div class="col-md-3" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                    <div class="px-3 py-4 px-lg-4">
                        <div class="overflow-hidden"><span class="icon-Tuxedo fs-3 color-primary fw-600" data-zanim='{"delay":0}'></span></div>
                        <div class="overflow-hidden">
                            <h5 class="mt-3" data-zanim='{"delay":0.1}'>Rental Loans</h5></div>
                        <div class="overflow-hidden">
                            <p class="mb-0" data-zanim='{"delay":0.2}'>We provide energy-efficient and environmentally conservative solutions to our clients to boost their business.</p>
                            <a class="btn btn-primary mt-3" href="{{ asset('contact') }}">
		                    	Apply Now<span class="fa fa-chevron-right ml-2"></span>
		                    </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                    <div class="px-3 py-4 px-lg-4">
                        <div class="overflow-hidden"><span class="icon-University fs-3 color-primary fw-600" data-zanim='{"delay":0}'></span></div>
                        <div class="overflow-hidden">
                            <h5 class="mt-3" data-zanim='{"delay":0.1}'>Education/School Fees Loans</h5></div>
                        <div class="overflow-hidden">
                            <p class="mb-0" data-zanim='{"delay":0.2}'>We provide energy-efficient and environmentally conservative solutions to our clients to boost their business.</p>
                            <a class="btn btn-primary mt-3" href="{{ asset('contact') }}">
		                    	Apply Now<span class="fa fa-chevron-right ml-2"></span>
		                    </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                    <div class="px-3 py-4 px-lg-4">
                        <div class="overflow-hidden"><span class="icon-Book fs-3 color-primary fw-600" data-zanim='{"delay":0}'></span></div>
                        <div class="overflow-hidden">
                            <h5 class="mt-3" data-zanim='{"delay":0.1}'>Consumer Assets Loans</h5></div>
                        <div class="overflow-hidden">
                            <p class="mb-0" data-zanim='{"delay":0.2}'>We provide energy-efficient and environmentally conservative solutions to our clients to boost their business.</p>
                            <a class="btn btn-primary mt-3" href="{{ asset('contact') }}">
		                    	Apply Now<span class="fa fa-chevron-right ml-2"></span>
		                    </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                    <div class="px-3 py-4 px-lg-4">
                        <div class="overflow-hidden"><span class="icon-Hospital fs-3 color-primary fw-600" data-zanim='{"delay":0}'></span></div>
                        <div class="overflow-hidden">
                            <h5 class="mt-3" data-zanim='{"delay":0.1}'>Health Care Bill Loans</h5></div>
                        <div class="overflow-hidden">
                            <p class="mb-0" data-zanim='{"delay":0.2}'>We provide energy-efficient and environmentally conservative solutions to our clients to boost their business.</p>
                            <a class="btn btn-primary mt-3" href="{{ asset('contact') }}">
		                    	Apply Now<span class="fa fa-chevron-right ml-2"></span>
		                    </a>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>
@endsection

{{-- scripts --}}
@section('scripts')
	
@endsection