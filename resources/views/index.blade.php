@extends('layouts.web-skin')

{{-- title --}}
@section('title')
	MerrisCoop
@endsection


{{-- contents --}}
@section('contents')
    <div class="flexslider flexslider-simple h-full loading">
        <ul class="slides">
            <li data-zanim-timeline="{}">
                <section class="py-0">
                    <div>
                        <div class="background-holder elixir-zanimm-scale" style="background-image:url(/site-assets/images/header-6.jpg);" data-zanimm='{"from":{"opacity":0.1,"filter":"blur(10px)","scale":1.05},"to":{"opacity":1,"filter":"blur(0px)","scale":1}}'></div>
                        <!--/.background-holder-->
                        <div class="container">
                            <div class="row h-full py-8 align-items-center" data-inertia='{"weight":1.5}'>
                                <div class="col-sm-8 col-lg-7 px-5 px-sm-3">
                                    <div class="overflow-hidden">
                                        <h1 class="fs-4 fs-md-5 zopacity" data-zanim='{"delay":0}'>
                                            We Make Borrowing Easy, Convenient and Affordable.       
                                        </h1>
                                    </div>
                                    <div class="overflow-hidden">
                                        <p class="color-primary mt-4 mb-5 lh-2 fs-1 fs-md-2 zopacity" data-zanim='{"delay":0.1}'>
                                            We help you respond quickly to your emergency needs and take your business to new heights.
                                        </p>
                                    </div>
                                    <div class="overflow-hidden">
                                        <div class="zopacity" data-zanim='{"delay":0.2}'>
                                            <a class="btn btn-primary mr-3 mt-3" href="{{ asset('contact') }}">Need a quick loan? Apply Now<span class="fa fa-chevron-right ml-2"></span>
                                            </a>
                                            <a class="btn btn-warning mt-3" href="{{ asset('contact') }}">Contact us<span class="fa fa-chevron-right ml-2"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/.row-->
                    </div>
                    <!--/.container-->
                </section>
            </li>
            <li data-zanim-timeline="{}">
                <section class="py-0">
                    <div>
                        <div class="background-holder elixir-zanimm-scale" style="background-image:url(/site-assets/images/header-5.jpg);" data-zanimm='{"from":{"opacity":0.1,"filter":"blur(10px)","scale":1.05},"to":{"opacity":1,"filter":"blur(0px)","scale":1}}'></div>
                        <!--/.background-holder-->
                        <div class="container">
                            <div class="row h-full py-8 align-items-center" data-inertia='{"weight":1.5}'>
                                <div class="col-sm-8 col-lg-7 px-5 px-sm-3">
                                    <div class="overflow-hidden">
                                        <h1 class="fs-4 fs-md-5 zopacity" data-zanim='{"delay":0}'>Get Funds for Your Business & Personal Needs</h1></div>
                                    <div class="overflow-hidden">
                                        <p class="color-primary mt-4 mb-5 lh-2 fs-1 fs-md-2 zopacity" data-zanim='{"delay":0.1}'>
                                            Be its start-up support, working capital, expansion, trade finance, debt consolidation or cheap and quick personal loans for emergency needs. We are here to help you.
                                        </p>
                                    </div>
                                    <div class="overflow-hidden">
                                        <div class="zopacity" data-zanim='{"delay":0.2}'>
                                            <a class="btn btn-primary mr-3 mt-3" href="{{ asset('contact') }}">Need express loan? Apply Now <span class="fa fa-chevron-right ml-2"></span>
                                            </a>
                                            <a class="btn btn-warning mt-3" href="{{ asset('contact') }}">Contact us<span class="fa fa-chevron-right ml-2"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/.row-->
                    </div>
                    <!--/.container-->
                </section>
            </li>
            <li data-zanim-timeline="{}">
                <section class="py-0">
                    <div>
                        <div class="background-holder elixir-zanimm-scale" style="background-image:url(/site-assets/images/header-1.jpg);" data-zanimm='{"from":{"opacity":0.1,"filter":"blur(10px)","scale":1.05},"to":{"opacity":1,"filter":"blur(0px)","scale":1}}'></div>
                        <!--/.background-holder-->
                        <div class="container">
                            <div class="row h-full py-8 align-items-center" data-inertia='{"weight":1.5}'>
                                <div class="col-sm-8 col-lg-7 px-5 px-sm-3">
                                    <div class="overflow-hidden">
                                        <h1 class="fs-4 fs-md-5 zopacity" data-zanim='{"delay":0}'>
                                            Be Smart! Make Us Your Piggy Bank
                                        </h1>
                                    </div>
                                    <div class="overflow-hidden">
                                        <p class="color-primary mt-4 mb-5 lh-2 fs-1 fs-md-2 zopacity" data-zanim='{"delay":0.1}'>
                                            Save daily on your expenses, spare money for the unexpected and for your goals. Secure your future!
                                        </p>
                                    </div>
                                    <div class="overflow-hidden">
                                        <div class="zopacity" data-zanim='{"delay":0.2}'>
                                            <a class="btn btn-primary mr-3 mt-3" href="{{ asset('contact') }}">
                                                Get Started with Smart Savings   <span class="fa fa-chevron-right ml-2"></span>
                                            </a>
                                            <a class="btn btn-warning mt-3" href="{{ asset('contact') }}">Contact us<span class="fa fa-chevron-right ml-2"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/.row-->
                    </div>
                    <!--/.container-->
                </section>
            </li>
        </ul>
    </div>
	
	<section class="background-white text-center pt-5 mb-0">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-10 col-md-12">
                    <h3 class="color-primary fs-2 fs-lg-3">Welcome to MerrisCoop</h3>
                    <p class="px-lg-12 mt-3">
                        A dynamic and innovative micro lending Institution that stands by your side always. 
                        From start to finish, we are professional, responsive, diligent and focused on your success. We are your Credible Choice of Success!
                    </p>
                    <hr class="short" data-zanim='{"from":{"opacity":0,"width":0},"to":{"opacity":1,"width":"4.20873rem"},"duration":0.8}' data-zanim-trigger="scroll" />
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-10 col-md-12">
                    <h3 class="color-primary fs-2 fs-lg-3">What We Do</h3>
                    <p class="px-lg-12 mt-3">
                        We are lenders, providing wide range easy, quick, flexible, accessible, affordable, customized nano and micro loans for businesses as well as personal loans.
                        <br /><br />

                        MerrisCoop facilitate accumulation of savings for our customers through custom daily micro money box products and promote supportive enterprise.

                        We enhance financial inclusion for the underserved and unbanked. 

                        <br /><br />
                        If you are a side walk vendor, a shop owner or have a stall in public markets, a hawker, artisan, low –income worker, freelance or self-employed, et al and cannot access funds from mainstream finance or conventional  bank, MerrisCoop is here to serve you!  Contact Us Now.
                    </p>
                    <hr class="short" data-zanim='{"from":{"opacity":0,"width":0},"to":{"opacity":1,"width":"4.20873rem"},"duration":0.8}' data-zanim-trigger="scroll" />
                </div>
            </div>
            <div class="row mt-4 mt-md-5">
                <div class="col-sm-6 col-lg-3 mt-4" data-zanim-timeline='{"delay":0.1}' data-zanim-trigger="scroll">
                    <div class="background-11 border border-color-9 radius-round mx-auto d-flex align-items-center" data-zanim='{"delay":0}' style="height:100px; width:100px;"><span class="icon-Bar-Chart5 fs-3 color-blueish mx-auto fw-600"></span></div>
                    <h5 class="mt-4" data-zanim='{"delay":0.1}'>Advisory</h5>
                    <p class="mb-0 mt-3 px-3" data-zanim='{"delay":0.2}'>
                        Our Advisors are ready                      
                        to help you make informed                      
                        decisions including markets                         ,            
                        trends, thereby reducing                            
                        associated risks
                    </p>
                </div>
                <div class="col-sm-6 col-lg-3 mt-4" data-zanim-timeline='{"delay":0.1}' data-zanim-trigger="scroll">
                    <div class="background-11 border border-color-9 radius-round mx-auto d-flex align-items-center" data-zanim='{"trigger":"scroll"}' style="height:100px; width:100px;"><span class="icon-Clock fs-3 color-blueish mx-auto fw-600"></span></div>
                    <h5 class="mt-4" data-zanim='{"delay":0.1}'>Micro Finance</h5>
                    <p class="mb-0 mt-3 px-3" data-zanim='{"delay":0.2}'>
                        Provide solution for every 
                         Nano, micro business related
                        problems skillfully and readily,
                        including instant credit, 
                        micro savings, payments         
                        and micro insurance, 
                    </p>
                </div>
                <div class="col-sm-6 col-lg-3 mt-4" data-zanim-timeline='{"delay":0.1}' data-zanim-trigger="scroll">
                    <div class="background-11 border border-color-9 radius-round mx-auto d-flex align-items-center" data-zanim='{"trigger":"scroll"}' style="height:100px; width:100px;"><span class="icon-Idea-2 fs-3 color-blueish mx-auto fw-600"></span></div>
                    <h5 class="mt-4" data-zanim='{"delay":0.1}'>Remittance</h5>
                    <p class="mb-0 mt-3 px-3" data-zanim='{"delay":0.2}'>
                        Sending money to Africa just
                        got easier, faster, safer and
                        instant. Direct to the beneficiary
                        bank account No hidden charges,
                        No surprises   No stories. 
                    </p>
                </div>
                <div class="col-sm-6 col-lg-3 mt-4" data-zanim-timeline='{"delay":0.1}' data-zanim-trigger="scroll">
                    <div class="background-11 border border-color-9 radius-round mx-auto d-flex align-items-center" data-zanim='{"trigger":"scroll"}' style="height:100px; width:100px;"><span class="icon-Headset fs-3 color-blueish mx-auto fw-600"></span></div>
                    <h5 class="mt-4" data-zanim='{"delay":0.1}'>Agency Banking</h5>
                    <p class="mb-0 mt-3 px-3" data-zanim='{"delay":0.2}'>
                        Quick banking services 
                        at your convenience and
                        so much more.
                    </p>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>

    <section class="pt-0">
        <div class="container">
            <div class="row mb-6">
                <div class="col">
                    <h3 class="text-center fs-2 fs-lg-3">Why Us</h3>
                    <hr class="short" data-zanim='{"from":{"opacity":0,"width":0},"to":{"opacity":1,"width":"4.20873rem"},"duration":0.8}' data-zanim-trigger="scroll">
                </div>
            </div>

            <div class="row mb-6">
                <div class="col">
                    <h4 class="text-center">Excellent Domain Competent</h4>
                    <p class="mt-3 text-center">MerrisCoop is a unique boutique, a lending institution of the future with integrity and excellence.</p>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 pr-0 pr-lg-3">
                    <img class="radius-secondary" src="/site-assets/images/why-choose-us-2.jpg" alt="" /><br /><br />
                    <img class="radius-secondary" src="/site-assets/images/why-choose-us.jpg" alt="" /> 
                    
                </div>
                <div class="col-lg-6 px-lg-5 mt-6 mt-lg-0" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                    <div class="overflow-hidden">
                        <div class="px-4 px-sm-0" data-zanim='{"delay":0}'>
                            <h5 class="fs-0 fs-lg-1">
                                <span class="icon-Clock fs-2 mr-2 icon-position-fix fw-800"></span>
                                Simplified Loan Process
                            </h5>
                            <p class="mt-3 px-lg-3">
                                Seamless, Simplified loan process solution.
                                At every loan level and fast response time.
                            </p>
                        </div>
                    </div>

                    <div class="overflow-hidden">
                        <div class="px-4 px-sm-0" data-zanim='{"delay":0}'>
                            <h5 class="fs-0 fs-lg-1">
                                <span class="icon-Calculator fs-2 mr-2 icon-position-fix fw-800"></span>
                                Low interest Rate
                            </h5>
                            <p class="mt-3 px-lg-3">
                                Enjoy market driven rate and funding options
                                up between N1, 000 -10million depending on experience and 
                                cash flow.
                            </p>
                        </div>
                    </div>

                    <div class="overflow-hidden">
                        <div class="px-4 px-sm-0 mt-5" data-zanim='{"delay":0.1}'>
                            <h5 class="fs-0 fs-lg-1">
                                <span class="icon-Money fs-2 mr-2 icon-position-fix fw-800"></span>
                                Quick Disbursement
                            </h5>
                            <p class="mt-3 px-lg-3">
                                Disbursement within 24 hours No stories, No hump, less 
                                paper work, simple terms 
                            </p>
                        </div>
                    </div>
                    <div class="overflow-hidden">
                        <div class="px-4 px-sm-0 mt-5" data-zanim='{"delay":0.1}'>
                            <h5 class="fs-0 fs-lg-1">
                                <span class="icon-Calendar-4 fs-2 mr-2 icon-position-fix fw-800"></span>We Are Convenience Repayment Option
                            </h5>
                            <p class="mt-3 px-lg-3">
                                Easy and convenient repayment schedule 
                                without hurting your cash flow. Be it daily, weekly, 
                                or monthly choose the plan that is suitable for you
                            </p>
                        </div>
                    </div>
                    <div class="overflow-hidden">
                        <div class="px-4 px-sm-0 mt-5" data-zanim='{"delay":0.2}'>
                            <h5 class="fs-0 fs-lg-1">
                                <span class="icon-Thumb fs-2 mr-2 icon-position-fix fw-600"></span>
                                Innovative Approach & Services
                            </h5>
                            <p class="mt-3 px-lg-3">
                                At MerrisCoop we treat you as a person not a number
                                experience unparalleled from beginning to the end, 
                                First class 24/7customer services support and client input.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>

    <section class="background-primary">
        <div class="container">
            <div class="row align-items-center color-white">
                <div class="col-lg-4">
                    <div class="border border-2x border-color-warning p-4 py-lg-5 text-center radius-secondary" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                        <div class="overflow-hidden">
                            <h4 class="color-white" data-zanim='{"delay":0}'>Request a call back</h4></div>
                        <div class="overflow-hidden">
                            <p class="px-lg-1 color-11 mb-0" data-zanim='{"delay":0.1}'>Would you like to speak to one of our financial advisers over the phone? Just submit your details and we’ll be in touch shortly. You can also email us if you would prefer.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 pl-lg-5 mt-6 mt-lg-0">
                    <h5 class="color-white">I would like to discuss:</h5>
                    <form class="zform mt-4" method="post" onsubmit="return sendDiscussNotification()">
                        <div class="row">
                            <div class="col-6 mt-4">
                                <input class="form-control" type="email" id="email" placeholder="Your Email" required="" />
                            </div>
                            <div class="col-6 mt-4">
                                <input class="form-control" type="text" id="name" placeholder="Your Name" aria-label="Text input with dropdown button" required="" />
                            </div>
                            <div class="col-6 mt-4">
                                <input class="form-control" type="text" id="phone" placeholder="Phone Number" aria-label="Text input with dropdown button" required="" />
                            </div>
                            <div class="col-6 mt-4">
                                <input class="form-control" type="text" id="subject" placeholder="Subject" aria-label="Text input with dropdown button" required="" />
                            </div>
                            <div class="col-6 mt-4">
                                <button class="btn btn-warning btn-block" id="d-btn" type="submit">Submit</button>
                            </div>
                            <div class="col-12">
                                <div class="zform-feedback mt-4"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>

    {{-- <section id="our-services">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mb-4 mt-sm-0">
                    <h3 class="text-center fs-2 fs-lg-3">Our Services</h3>
                </div>
                <div class="col-md-6 mt-4 mt-sm-0">
                    <ul>
                        <li class="mb-4">
                            <h5>Cash Deposit</h5>
                            Driven by our customer philosophy, the customer always comes first. We are customer- centric organization. The customer is the driving force of our entire product.
                        </li>
                        <li class="mb-4">
                            <h5>Cash Withdrawal</h5>
                            Integrity is central in all our operations. Compliance with regulations and the host communities is primary while decisions are based purely on business model and ethics. 
                        </li>
                        <li class="mb-4"><h5>Fund Transfer</h5>
                            We pay thorough attention to details and strive at all times to attain and exceed the highest possible standards and expectations.</li>
                        <li class="mb-4">
                            <h5>Balance Enquiry</h5>
                            Innovation is core to our business.  We leverage technology to automated processes, develop unique platforms, create new financials products, systems, design procedures that are consistent and enduring. 
                        </li>
                    </ul>
                </div>
                <div class="col-md-6 mt-6 mt-sm-0">
                    <ul>
                        <li class="mb-4">
                            <h5>Bills Payment</h5>
                            At MerrisCoop, we hold the interest of team above those of individuals, while showing mutual respect for all employees and sharing information throughout our organization.
                        </li>

                        <li class="mb-4"><h5>Merris Alert</h5>
                            We understand conflict of interest is detrimental to our business. As an organization, we are answerable to our stakeholders on all actions and results. 
                        </li>
 
                    </ul>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section> --}}

    <section class=" background-white">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p class="text-center">Our Quick Banking Services.</p>
                    <div class="owl-carousel owl-theme owl-dot-round" data-options='{"nav":true,"dots":false,"loop":true,"autoplay":true,"autoplayHoverPause":true,"items":1}'>
                        <div class="row px-lg-8">
                            <div class="col-md-9 ml-auto mt-4 mt-md-0 px-4 px-sm-3">
                                <h5>
                                    <span class="icon-Address-Book fs-3 color-blueish mx-auto fw-600"></span>
                                    Instant Account Opening
                                </h5>
                                Opening of any bank account without going through hump
                            </div>
                        </div>
                        <div class="row px-lg-8">
                            <div class="col-md-9 ml-auto mt-4 mt-md-0 px-4 px-sm-3">
                                <h5>
                                    <span class="icon-Coins fs-3 color-blueish mx-auto fw-600"></span> 
                                    Cash Deposit
                                </h5>
                                Deposit money into any bank account
                            </div>
                        </div>
                        <div class="row px-lg-8">
                            <div class="col-md-9 ml-auto mt-4 mt-md-0 px-4 px-sm-3">
                                <h5>
                                    <span class="icon-Cash-register2 fs-3 color-blueish mx-auto fw-600"></span>
                                    Cash Withdrawal
                                </h5>
                                Withdraw Cash from any bank account
                            </div>
                        </div>
                        <div class="row px-lg-8">
                            <div class="col-md-9 ml-auto mt-4 mt-md-0 px-4 px-sm-3">
                                <h5>
                                    <span class="icon-Repeat fs-3 color-blueish mx-auto fw-600"></span>
                                    Funds Transfer
                                </h5>
                                Send money to any bank account
                            </div>
                        </div>
                        
                        <div class="row px-lg-8">
                            <div class="col-md-9 ml-auto mt-4 mt-md-0 px-4 px-sm-3">
                                <h5>
                                    <span class="icon-University fs-3 color-blueish mx-auto fw-600"></span>
                                    Balance Enquiry
                                </h5>
                                Check bank balance at your convenience
                            </div>
                        </div>
                        <div class="row px-lg-8">
                            <div class="col-md-9 ml-auto mt-4 mt-md-0 px-4 px-sm-3">
                                <h5>
                                    <span class="icon-File-Copy2 fs-3 color-blueish mx-auto fw-600"></span>
                                    Bills Payment
                                </h5>
                                Pay Bills conveniently 
                                Electricity Bill, Federal Inland Revenue Tax, Lagos State Inland Revenue, GOTY, DSTV and more
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>

    <section>
        <div class="background-holder overlay overlay-elixir" style="background-image:url('/site-assets/images/background-15.jpg');"></div>
        <!--/.background-holder-->
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="media"><span class="ion-android-checkmark-circle fs-5 color-warning mr-3" style="transform: translateY(-1rem);"></span>
                        <div class="media-body">
                            <h2 class="color-warning fs-3 fs-lg-4">Take the right step,<br/><span class="color-white">do the big things.</span></h2>
                            <div class="row mt-4 pr-lg-10">
                                <div class="col-md-3 overflow-hidden" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                                    <div class="fs-3 fs-lg-4 mb-0 lh-2 fw-700 color-white mt-lg-5 mt-3" data-zanim='{"delay":0.1}'>210</div>
                                    <h6 class="fs-0 color-white" data-zanim='{"delay":0.2}'>Cases Solved</h6></div>
                                <div class="col col-lg-3 overflow-hidden" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                                    <div class="fs-3 fs-lg-4 mb-0 lh-2 fw-700 color-white mt-lg-5 mt-3" data-zanim='{"delay":0.1}'>6</div>
                                    <h6 class="fs-0 color-white" data-zanim='{"delay":0.2}'>Trained Experts</h6></div>
                                <div class="w-100 d-flex d-lg-none"></div>
                                <div class="col-md-3 overflow-hidden" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                                    <div class="fs-3 fs-lg-4 mb-0 lh-2 fw-700 color-white mt-lg-5 mt-3" data-zanim='{"delay":0.1}'>2</div>
                                    <h6 class="fs-0 color-white" data-zanim='{"delay":0.2}'>Branches</h6></div>
                                <div class="col col-lg-3 overflow-hidden" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                                    <div class="fs-3 fs-lg-4 mb-0 lh-2 fw-700 color-white mt-lg-5 mt-3" data-zanim='{"delay":0.1}'>100%</div>
                                    <h6 class="fs-0 color-white" data-zanim='{"delay":0.2}'>Satisfied Clients</h6></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>

    {{-- <section class=" background-white">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="owl-carousel owl-theme owl-dot-round" data-options='{"nav":true,"dots":false,"loop":true,"autoplay":true,"autoplayHoverPause":true,"items":1}'>
                        <div class="row px-lg-8">
                            <div class="col-4 col-md-3 mx-auto"><img class="radius-secondary mx-auto" src="assets/images/client1.png" alt="Member" style="width: auto;" /></div>
                            <div class="col-md-9 ml-auto mt-4 mt-md-0 px-4 px-sm-3">
                                <p class="lead fw-400">Their work on our website and Internet marketing has made a significant different to our business. We’ve seen a 425% increase in quote requests from the website which has been pretty remarkable – but I’d always like to see more!</p>
                                <h6 class="fs-0 mb-1 mt-4">Michael Clarke</h6>
                                <p class="mb-0 color-7">CEO, A.E.T Institute</p>
                            </div>
                        </div>
                        <div class="row px-lg-8">
                            <div class="col-4 col-md-3 mx-auto"><img class="radius-secondary mx-auto" src="assets/images/client2.png" alt="Member" style="width: auto;" /></div>
                            <div class="col-md-9 ml-auto mt-4 mt-md-0 px-4 px-sm-3">
                                <p class="lead fw-400">Writing case studies was a daunting task for us. We didn’t know where to begin or what questions to ask, and clients never seemed to follow through when we asked. Elixir team did everything – with almost no time or effort for me!</p>
                                <h6 class="fs-0 mb-1 mt-4">Maria Sharapova</h6>
                                <p class="mb-0 color-7">Managing Director, Themewagon Inc.</p>
                            </div>
                        </div>
                        <div class="row px-lg-8">
                            <div class="col-4 col-md-3 mx-auto"><img class="radius-secondary mx-auto" src="assets/images/client3.png" alt="Member" style="width: auto;" /></div>
                            <div class="col-md-9 ml-auto mt-4 mt-md-0 px-4 px-sm-3">
                                <p class="lead fw-400">As a sales gamification company, we were skeptical to work with a consultant to optimize our sales emails, but Elixir was highly recommended by many other Y-Combinator startups we knew. Elixir helped us run a ~6 week email campaign.</p>
                                <h6 class="fs-0 mb-1 mt-4">David Beckham</h6>
                                <p class="mb-0 color-7">Chairman, Harmony Corporation</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section> --}}

    <section class="background-11">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h3 class="text-center fs-2 fs-lg-3">Latest News</h3>
                    <hr class="short" data-zanim='{"from":{"opacity":0,"width":0},"to":{"opacity":1,"width":"4.20873rem"},"duration":0.8}' data-zanim-trigger="scroll" />
                </div>
            </div>
            <div class="row mt-lg-6 display-news">
                Loading...
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>

    <section style="background-color: #3D4C6F;">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="background-primary color-white p-5 p-lg-6 radius-secondary">
                        <h4 class="color-white fs-1 fs-lg-2 mb-1">Sign up for email alerts</h4>
                        <p class="color-white">Stay current with our latest insights</p>
                        <form class="mt-4" id="subscribe-form" method="post" onsubmit="return sendSubscription()">
                            <div class="row align-items-center">
                                <div class="col-md-7 pr-md-0">
                                    <div class="input-group">
                                        <input class="form-control" id="sub-email" type="email" placeholder="Enter Email Here" required="" />
                                    </div>
                                </div>
                                <div class="col-md-5 mt-3 mt-md-0">
                                    <button class="btn btn-warning btn-block" id="subcribe-btn" type="submit">
                                        <span class="color-primary fw-600" id="subscribe-text">Submit</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-6 mt-4 mt-lg-0">
                    <div class="row">
                        <div class="col-6 col-lg-4 color-white ml-lg-auto">
                            <ul class="list-unstyled">
                                <li class="mb-3"><a class="color-white" href="{{ url('contact') }}">Contact Us</a></li>
                                <li class="mb-3"><a class="color-white" href="{{ url('complaint') }}">Complaint</a></li>
                                {{-- <li class="mb-3"><a class="color-white" href="{{ url('faq') }}">FAQ</a></li> --}}
                                <li class="mb-3"><a class="color-white" href="{{ url('policy') }}">Cookie Policy</a></li>
                                <li class="mb-3"><a class="color-white" href="{{ url('private-policy') }}">Privacy Policy</a></li>
                                <li class="mb-3"><a class="color-white" href="{{ url('terms') }}">Terms of Use</a></li>
                            </ul>
                        </div>
                        <div class="col-6 col-sm-5 ml-sm-auto">
                            <a href="#">
                                <div class="row mb-3 align-items-center no-gutters">
                                    <div class="col-auto">
                                        <div class="background-primary text-center d-flex align-items-center radius-primary" style="height: 40px; width: 40px;"><span class="w-100 fa fa-linkedin color-white"></span></div>
                                    </div>
                                    <div class="col-6 pl-3">
                                        <h5 class="fs-0 color-white mb-0 d-inline-block">Linkedin</h5></div>
                                </div>
                            </a>
                            <a href="#">
                                <div class="row mb-3 align-items-center no-gutters">
                                    <div class="col-auto">
                                        <div class="background-primary text-center d-flex align-items-center radius-primary" style="height: 40px; width: 40px;"><span class="w-100 fa fa-twitter color-white"></span></div>
                                    </div>
                                    <div class="col-6 pl-3">
                                        <h5 class="fs-0 color-white mb-0 d-inline-block">Twitter</h5></div>
                                </div>
                            </a>
                            <a href="#">
                                <div class="row mb-3 align-items-center no-gutters">
                                    <div class="col-auto">
                                        <div class="background-primary text-center d-flex align-items-center radius-primary" style="height: 40px; width: 40px;"><span class="w-100 fa fa-facebook color-white"></span></div>
                                    </div>
                                    <div class="col-6 pl-3">
                                        <h5 class="fs-0 color-white mb-0 d-inline-block">Facebook</h5></div>
                                </div>
                            </a>
                            <a href="#">
                                <div class="row mb-3 align-items-center no-gutters">
                                    <div class="col-auto">
                                        <div class="background-primary text-center d-flex align-items-center radius-primary" style="height: 40px; width: 40px;"><span class="w-100 fa fa-google-plus color-white"></span></div>
                                    </div>
                                    <div class="col-6 pl-3">
                                        <h5 class="fs-0 color-white mb-0 d-inline-block">Google+</h5></div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>
@endsection

{{-- scripts --}}
@section('scripts')
	<script type="text/javascript">
        // contact merriss for a meeting
        function sendDiscussNotification(){
            $("#d-btn").html("Setting up your meeting....");

            var token   = '{{ csrf_token() }}';
            var name    = $("#name").val();
            var subject = $("#subject").val();
            var email   = $("#email").val();
            var phone   = $("#phone").val();

            var params  = {
                _token: token,
                name: name,
                subject: subject,
                email: email,
                phone: phone
            }


            $.post('{{ url('client/contact-desk') }}', params, function(data, textStatus, xhr) {
                /*optional stuff to do after success */
                if(data.status === "success"){
                    swal(
                        "ok",
                        data.message,
                        data.status
                    );

                    // reset form
                    $(".zform")[0].reset();
                }else{
                    swal(
                        "oops",
                        data.message,
                        data.status
                    );
                }

                // submit 
                $("#d-btn").html("Submit");
            });

            // void
            return false;
        }  

        // contact merriss for a meeting
        function sendSubscription(){
            $("#subcribe-text").html("We're adding you to our newsletter....");

            var token   = '{{ csrf_token() }}';
            var email   = $("#sub-email").val();

            var params  = {
                _token: token,
                email: email
            }

            $.post('{{ url('client/subscribe') }}', params, function(data, textStatus, xhr) {
                /*optional stuff to do after success */
                if(data.status === "success"){
                    swal(
                        "ok",
                        data.message,
                        data.status
                    );

                    // reset form
                    $(".zform")[0].reset();
                }else{
                    swal(
                        "oops",
                        data.message,
                        data.status
                    );
                }

                // submit 
                $("#subcribe-text").html("Submit");
            });

            // void
            return false;
        }

        // load global news
        function loadGlobalNews() {
            $.get('{{ url('load/global/news') }}', function(data) {
                $(".display-news").html("");
                var sn = 0;
                $.each(data, function(index, val) {
                    sn++;
                    $(".display-news").append(`
                        <div class="col-md-6 col-lg-4 py-0 mt-4 mt-lg-0">
                            <div class="background-white pb-4 h-100 radius-secondary"><img class="w-100 radius-tr-secondary radius-tl-secondary" src="${val.urlToImage}" alt="Featured Image" />
                                <div class="px-4 pt-4">
                                    <div>
                                        <a href="javascript:void(0);"><h5>${val.title}</h5></a>
                                    </div>
                                    <div>
                                        <p class="mt-3">${val.description}</p>
                                    </div>
                                    <div class="overflow-hidden">
                                        <div class="d-inline-block" data-zanim='{"delay":0.3}'><a class="d-flex align-items-center" href="#">Learn More<div class="overflow-hidden ml-2" data-zanim='{"from":{"opacity":0,"x":-30},"to":{"opacity":1,"x":0},"delay":0.8}'><span class="d-inline-block">&xrarr;</span></div></a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    `);

                    if(sn > 2){
                        // void loop
                        return false;
                    }
                });
            });
        }

        // display global news
        loadGlobalNews();
    </script>
@endsection