@extends('layouts.agent-skin')

{{-- title --}}
@section('title')
	MerrisCoop | Agent Transactions
@endsection

{{-- contents --}}
@section('contents')
    <section class="background-11">
        <div class="container">
            <div class="row">
                <div class="col-md-12 p-2">
                    <a href="{{ url('agent/dashboard') }}">
                        <i class="fa fa-arrow-left"></i> Home
                    </a>
                </div>
            </div>
            <div class="row">
              <div class="col-md-12 p-2">
                <h5 class="p-2">Transactions</h5>
                <hr />
                <table class="table small" id="transactions">
                    <thead>
                        <tr>
                            <th>Customer</th>
                            <th>Ref</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <tbody class="all-transactions"></tbody>
                </table>
              </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>
@endsection

{{-- scripts --}}
@section('scripts')
	<script type="text/javascript">
        // $(document).ready(function(d){
          loadAllTransactions();
        // });

        // load all transactions
        function loadAllTransactions() {
          $.get('{{ url('agent/all/transactions') }}', function(data) {
            $(".all-transactions").html("");
            $.each(data, function(index, val) {
              // console log value
              // console.log(val);
              $(".all-transactions").append(`
                <tr>
                  <td>${val.details.firstname} ${val.details.lastname}</td>
                  <td>${val.transaction.ref_no}</td>
                  <td>&#8358;${numeral(val.transaction.amount).format('0,0.00')}</td>
                </tr>
              `);
            });

            // $('#transactions').DataTable();
            $('#transactions').DataTable({
              'paging'      : true,
              'lengthChange': false,
              'searching'   : true,
              'ordering'    : false,
              'info'        : true,
              'autoWidth'   : false
            });
          });
        }
	</script>
@endsection