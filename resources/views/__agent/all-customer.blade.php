@extends('layouts.agent-skin')

{{-- title --}}
@section('title')
	MerrisCoop | Agent All Customer
@endsection

{{-- contents --}}
@section('contents')
    <section class="background-11">
        <div class="container">
            <div class="row">
                <div class="col-md-12 p-2">
                    <a href="{{ url('agent/dashboard') }}">
                        <i class="fa fa-arrow-left"></i> Home
                    </a>
                </div>
            </div>
            <div class="row">
              <div class="col-md-12 p-2">
                <h5 class="p-2">Customers</h5>
                <hr />
                <table class="table small" id="customers">
                    <thead>
                        <tr>
                            <th>Customer</th>
                            <th>Amount</th>
                            <th>Type</th>
                        </tr>
                    </thead>
                    <tbody class="load-all-customers"></tbody>
                </table>
              </div>
            </div>
          </div>
        <!--/.container-->
    </section>
@endsection

{{-- scripts --}}
@section('scripts')
	<script type="text/javascript">
        // load modules
        loadAllCustomers();

		// load all customers
        function loadAllCustomers() {
          $.get('{{ url('agent/all/customers') }}', function(data) {
            var sn = 0;
            $(".load-all-customers").html("");
            $.each(data, function(index, val) {
              sn++;

              var shadeRow;
              if(val.type == "savings"){
                shadeRow = `class="text-green"`; 
              }else{
                shadeRow = `class="text-info"`; 
              }

              // console log value
              // console.log(val);
              $(".load-all-customers").append(`
                <tr ${shadeRow}>
                  <td>${val.name}</td>
                  <td>&#8358;${numeral(val.account.balance).format('0,0.00')}</td>
                  <td>${val.type}</td>
                </tr>
              `);
            });
            // $('#transactions').DataTable();
            $('#customers').DataTable({
              'paging'      : true,
              'lengthChange': false,
              'searching'   : true,
              'ordering'    : true,
              'info'        : true,
              'autoWidth'   : false
            });
          });
        }
	</script>
@endsection