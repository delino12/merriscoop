@extends('layouts.agent-skin')

{{-- title --}}
@section('title')
	MerrisCoop | Agent Dashboard
@endsection

{{-- contents --}}
@section('contents')
    <section class="background-11">
        <div class="container">
            <div class="row">
                <table class="table text-center">
                    <tr>
                        <td>
                            <a href="{{ url('agent/check/balance') }}" style="text-decoration: none;">
                                <i class="fa fa-bank fa-2x"></i> <br />
                                Balance
                            </a>
                        </td>
                        <td>
                            <a href="{{ url('agent/new/customer') }}" style="text-decoration: none;">
                                <i class="fa fa-edit fa-2x"></i> <br />
                                New
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="{{ url('agent/deposit') }}" style="text-decoration: none;">
                                <i class="fa fa-money fa-2x"></i> <br />
                                Quick Deposit
                            </a>
                        </td>
                        <td>
                            <a href="{{ url('agent/loan-deposit') }}" style="text-decoration: none;">
                                <i class="fa fa-money fa-2x"></i> <br />
                                Loan Re-payment
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <a href="{{ url('agent/customers') }}" style="text-decoration: none;">
                                <i class="fa fa-user fa-2x"></i> <br />
                                My Customers
                            </a>
                        </td>
                        <td>
                            <a href="{{ url('agent/loans') }}" style="text-decoration: none;">
                                <i class="fa fa-database fa-2x"></i> <br />
                                Loans
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="{{ url('agent/transactions') }}" style="text-decoration: none;">
                                <i class="fa fa-file-o fa-2x"></i> <br />
                                Transactions
                            </a>
                        </td>
                        <td>
                            <a href="{{ url('agent/logout') }}" style="text-decoration: none;">
                                <i class="fa fa-sign-out fa-2x"></i> <br />
                                Logout
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>
@endsection

{{-- scripts --}}
@section('scripts')
	<script type="text/javascript">

	</script>
@endsection