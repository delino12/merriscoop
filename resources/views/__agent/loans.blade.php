@extends('layouts.agent-skin')

{{-- title --}}
@section('title')
	MerrisCoop | Agent Loans
@endsection

{{-- contents --}}
@section('contents')
    <section class="background-11">
        <div class="container">
            <div class="row">
                <div class="col-md-12 p-2">
                    <a href="{{ url('agent/dashboard') }}">
                        <i class="fa fa-arrow-left"></i> Home
                    </a>
                </div>
            </div>
            <div class="row">
              <div class="col-md-12 p-2">
                <h5 class="p-2">Loans</h5>
                <hr />
                <table class="table small" id="loans">
                    <thead>
                        <tr>
                            <th>Customer</th>
                            <th>Balance</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody class="load-all-loans"></tbody>
                </table>
              </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>
@endsection

{{-- scripts --}}
@section('scripts')
	<script type="text/javascript">
		// $(document).ready(function (){
        loadAllCustomers();
        // });
        
        // load all customers
        function loadAllCustomers() {
          $.get('{{ url('agent/all/loans') }}', function(data) {
            var sn = 0;
            $(".load-all-loans").html("");
            $.each(data, function(index, val) {
              sn++;

              var shadeRow;

              if(val.status == "settled"){
                shadeRow = `class="text-green"`;
              }else{
                shadeRow = `class="text-danger"`;
              } 

              // console log value
              $(".load-all-loans").append(`
                <tr ${shadeRow}>
                  <td>${val.details.name}</td>
                  <td>&#8358;${numeral(val.balance).format('0,0.00')}</td>
                  <td>&#8358;${numeral(val.total).format('0,0.00')}</td>
                </tr>
              `);
            });

            $('#loans').DataTable({
              'paging'      : true,
              'lengthChange': false,
              'searching'   : true,
              'ordering'    : true,
              'info'        : true,
              'autoWidth'   : false
            });
          });
        }
	</script>
@endsection