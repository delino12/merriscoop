@extends('layouts.agent-skin')

{{-- title --}}
@section('title')
	MerrisCoop | Agent Deposit
@endsection

{{-- contents --}}
@section('contents')
    <section class="background-11">
        <div class="container">
            <div class="row">
                <div class="col-md-12 p-2">
                    <a href="{{ url('agent/dashboard') }}">
                        <i class="fa fa-arrow-left"></i> Home
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 p-2">
                    <h5 class="p-2">Search Customer</h5>
                    <hr />
                    <form method="post" onsubmit="return searchCustomer()">
                        <div class="form-group">
                            <label>Enter customer name</label>
                            <input type="text" id="search_keyword" class="form-control" placeholder="Search customer name">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary">
                                <i class="fa fa-search"></i> Search
                            </button>
                        </div>
                    </form>
                </div>

                <div class="col-md-12 p-2">
                    <table class="table" width="100%">
                        <tbody class="load-search"></tbody>
                    </table>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>
@endsection

{{-- scripts --}}
@section('scripts')
	<script type="text/javascript">
		function searchCustomer() {
            // body..
            var token   = $("#token").val();
            var keyword = $("#search_keyword").val();

            var params = {
                _token: token,
                keyword: keyword
            };

            $.post('{{ url('agent/search/customer') }}', params, function(data, textStatus, xhr) {
                /*optional stuff to do after success */
                buildCustomerView(data);
                // console log value
                console.log(data);
            });

            // return void
            return false;
        }

        function buildCustomerView(data) {
            // body...
            var sn = 0;
            $(".load-search").html("")
            $(".load-search").append(`
                <tr>
                    <td>Customer</td>
                    <td>
                       Balance
                    </td>
                    <td>
                        Total
                    </td>
                </tr>
            `);
            $.each(data, function(index, val) {
                sn++;
                $(".load-search").append(`
                    <tr>
                        <td>${val.name}</td>
                        <td>
                           ${numeral(val.account.balance).format('0,0.00')}
                        </td>
                        <td>
                            ${numeral(val.account.total).format('0,0.00')}
                        </td>
                    </tr>
                `);
            });
        }
	</script>
@endsection