@extends('layouts.agent-skin')

{{-- title --}}
@section('title')
	MerrisCoop | Agent New Customer
@endsection

{{-- contents --}}
@section('contents')
    <section class="background-11">
        <div class="container">
            <div class="row">
                <div class="col-md-12 p-2">
                    <a href="{{ url('agent/dashboard') }}">
                        <i class="fa fa-arrow-left"></i> Home
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 p-4">
                  <h5 class="p-2">Add</h5>
                  <hr />
                  <!-- /.box-header -->
                  <form class="form" method="POST" onsubmit="return createNewAccount()">
                    <div class="box-body">
                      <h4 class="box-title text-info"><i class="ti-user mr-15"></i> Personal Info</h4>
                      <hr class="my-15">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>First Name</label>
                            <input type="text" class="form-control" id="firstname" placeholder="First Name" required="">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Last Name</label>
                            <input type="text" class="form-control" id="lastname" placeholder="Last Name" required="">
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label >E-mail</label>
                            <input type="text" class="form-control" id="email" id="loan-email" placeholder="E-mail">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label >Contact Number</label>
                            <input type="text" class="form-control" id="phone" maxlength="11" placeholder="Phone" required="">
                          </div>
                        </div>
                      </div>
                      <h4 class="box-title text-info"><i class="ti-save mr-15"></i> Requirements</h4>
                      <hr class="my-15">
                      <div class="form-group">
                        <label>Address</label>
                        <input type="text" class="form-control" id="address" placeholder="Enter valid house address..." required="">
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>What is your occupation</label>
                            <input type="text" class="form-control" id="occupation" placeholder="Enter occupation"  required="" />
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Target Amount or Loan Amount</label>
                            <div class="row">
                              <div class="col-md-6">
                                <input type="number" onkeyup="formatAmount()" class="form-control" id="amount" placeholder="&#8358; 10,000.00" required="">
                              </div>
                              <div class="col-md-6">
                                <div class="form-control" id="preview-amount"></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                        <div class="form-group">
                          <label>Nationality</label>
                          <select class="form-control" id="country">
                            <option value="Nigeria">Nigeria</option>
                          </select>
                        </div>
                        </div>
                        <div class="col-md-6">
                        <div class="form-group">
                          <label>State</label>
                          <select class="form-control" id="state" onchange="selectLocalGovt()">
                          </select>
                        </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Local Government Area (LGA)</label>
                            <select class="form-control" id="lga"></select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Account Type</label>
                            <select class="form-control" id="account_type" onchange="showOption()">
                              <option value="">--none--</option>
                              <option value="savings">Savings Account</option>
                              <option value="loans">Loans Account</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="account_option" style="display: none;"></div>

                      {{-- <div class="row"> --}}
                        <input type="hidden" id="idcard" value="https://img.freepik.com/free-psd/blue-corporate-business-card_1051-1590.jpg?size=480&ext=jpg">
                        {{-- <div class="col-md-6"> --}}
                          {{-- <div class="form-group"> --}}
                            {{-- <label>Select File (Passport or Valid ID card)</label> --}}
                            {{-- <a href="#" id="upload_widget_opener" class="btn btn-default">Upload multiple images</a> --}}
                            {{-- <input type="hidden" id="idcard" value=""> --}}
                          {{-- </div> --}}
                        {{-- </div> --}}
                        <div class="col-md-6">
                          <div class="loan-calculator-screen"></div>
                          <div class="investment-calculator-screen"></div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Why do i want to take this step?</label>
                        <textarea rows="5" class="form-control" id="narration" placeholder="Explain why you have decided to open an account?"></textarea>
                      </div>

                      <!-- /.box-body -->
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary btn-outline">
                        <i class="ti-save-alt"></i> Save
                      </button>
                    </div> 
                    </div> 
                  </form>
                </div>
            </div>
        </div>
        <!--/.container-->
    </section>
@endsection

{{-- scripts --}}
@section('scripts')
  {{-- <script src="//widget.cloudinary.com/global/all.js" type="text/javascript"></script>  --}}
  <script type="text/javascript">
    // $(document).ready(function (){
    getAllStates();
    // loadAllCustomers();
    getTypeHints();
    // });

    // get users state
    function getAllStates() {
      $.get('https://locationsng-api.herokuapp.com/api/v1/states', function(data) {
        $("#state").html("");
        $.each(data, function(index, val) {
          $("#state").append(`
            <option value="${val.name}">${val.name}</option>
          `);
        });
      });
    }

    // select local government
    function selectLocalGovt() {
      var state = $("#state").val();
      $.get('https://locationsng-api.herokuapp.com/api/v1/states/'+state+'/lgas', function(data) {
        $("#lga").html("");
        $.each(data, function(index, val) {
          $("#lga").append(`
            <option value="${val}">${val}</option>
          `);
        });
      });
    }

    // check account option
    function showOption() {
      var account_type = $("#account_type").val();

      if(account_type == ""){
        swal(
          "oops",
          "Account type is required!",
          "info"
        );
      }else if(account_type == "savings"){
        $(".account_option").html(`
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Interest Accrued(%)  <span class="small text-danger">Strict Mode Only</span></label>
                <input type="number" onkeyup="calculateInvestmentInterest()" id="interest" placeholder="Enter interest rate" class="form-control" />
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Investment Period (Days)</label>
                <input type="number" id="duration" min="1" placeholder="Enter loan duration" class="form-control" />
              </div>
            </div>
          </div>
        `).fadeIn();
      }else if(account_type == "loans"){
        $(".account_option").html(`
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Interest Rate(%)  <span class="small text-danger">Strict Mode Only</span></label>
                <input type="number" onkeyup="calculateLoanInterest()" id="interest" placeholder="Enter interest rate" class="form-control" />
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Loan Duration (Days)</label>
                <input type="number" id="duration" min="1" placeholder="Enter loan duration" class="form-control" />
              </div>
            </div>
          </div>
        `).fadeIn();
      }
    }

    // loan interest
    function calculateLoanInterest() {
      var interest  = $("#interest").val();
      var amount    = $("#amount").val();
      if(amount == ""){
        swal(
          "oops",
          "Amount is required, enter a valid amount!",
          "error"
        );
        // void calculation
        return false;
      }

      var percent = parseFloat(interest / 100);
      var charge  = parseFloat(percent * amount);
      var payable = parseFloat(charge) + parseFloat(amount);

      $(".loan-calculator-screen").html(`
        <table class="table">
          <tr>
            <td><span class="text-green">Details</span></td>
            <td><span class="text-green">Consideration</span></td>
          </tr>
          <tr>
            <td>Amount:</td>
            <td>&#8358;${numeral(amount).format("0,0.00")}</td>
          </tr>
          <tr>
            <td>Interest:</td>
            <td>&#8358;${numeral(charge).format("0,0.00")}</td>
          </tr>
          <tr>
            <td>Tax:</td>
            <td>&#8358;0.00</td>
          </tr>
          <tr>
            <td>Total:</td>
            <td>&#8358;${numeral(payable).format("0,0.00")}</td>
          </tr>
        </table>
      `);
    }

    // loan interest
    function calculateInvestmentInterest() {
      var interest  = $("#interest").val();
      var amount    = $("#amount").val();

      if(amount == ""){
        swal(
          "oops",
          "Amount is required, enter a valid amount!",
          "error"
        );
        // void calculation
        return false;
      }

      var percent = parseFloat(interest / 100);
      var charge  = parseFloat(percent * amount);
      var payable = parseFloat(charge) + parseFloat(amount);

      $(".investment-calculator-screen").html(`
        <table class="table">
          <tr>
            <td><span class="text-green">Details</span></td>
            <td><span class="text-green">Consideration</span></td>
          </tr>
          <tr>
            <td>Amount:</td>
            <td>&#8358;${numeral(amount).format("0,0.00")}</td>
          </tr>
          <tr>
            <td>Interest:</td>
            <td>&#8358;${numeral(charge).format("0,0.00")}</td>
          </tr>
          <tr>
            <td>Tax:</td>
            <td>&#8358;0.00</td>
          </tr>
          <tr>
            <td>Total:</td>
            <td>&#8358;${numeral(payable).format("0,0.00")}</td>
          </tr>
        </table>
      `);
    }

    // create new user
    function createNewAccount() {
      var token         = $("#token").val();
      var firstname     = $("#firstname").val();
      var lastname      = $("#lastname").val();
      var email         = $("#email").val();
      var phone         = $("#phone").val();
      var address       = $("#address").val();
      var occupation    = $("#occupation").val();
      var amount        = $("#amount").val();
      var country       = $("#country").val();
      var state         = $("#state").val();
      var lga           = $("#lga").val();
      var account_type  = $("#account_type").val();
      var idcard        = $("#idcard").val();
      var narration     = $("#narration").val();

      // loans only
      var interest     = $("#interest").val();
      var duration     = $("#duration").val();

      var params = {
        _token: token,
        firstname: firstname,
        lastname: lastname,
        email: email,
        phone: phone,
        address: address,
        occupation: occupation,
        amount: amount,
        country: country,
        state: state,
        lga: lga,
        account_type: account_type,
        idcard: idcard,
        narration: narration,
        interest: interest,
        duration: duration
      }

      // console
      // console.log(params);

      // create new account
      $.post('{{ url('admin/create/new/user') }}', params, function(data, textStatus, xhr) {
        if(data.status == "success"){
          swal(
            "completed",
            data.message,
            data.status
          );

          $(".form")[0].reset();
          $("#preview-idcard").html("");
          $(".account_option").html("");

          // refresh list
          loadAllCustomers();

        }else if(data.status == "error"){
          swal(
            "oops",
            data.message,
            data.status
          );
        }
      });

      // void form 
      return false;
    }

    // format amount
    function formatAmount() {
      var amount = $("#amount").val();

      // preview
      $("#preview-amount").html(`
        &#8358;${numeral(amount).format('0,0.00')}
      `);
    }

    // get type hints
    function getTypeHints() {
      $.get('{{ url('admin/occupation/category') }}', function(data) {
        $("#occupation").typeahead({
          source: data
        });
      });
    }
  </script>
@endsection