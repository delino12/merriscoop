@extends('layouts.agent-skin')

{{-- title --}}
@section('title')
	MerrisCoop | Agent Deposit
@endsection

{{-- contents --}}
@section('contents')
    <section class="background-11">
        <div class="container">
            <div class="row">
                <div class="col-md-12 p-2">
                    <a href="{{ url('agent/dashboard') }}">
                        <i class="fa fa-arrow-left"></i> Home
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 p-2">
                    <h5 class="p-2">Quick Deposit</h5>
                    <hr />
                    <form method="post" onsubmit="return searchCustomer()">
                        <div class="form-group">
                            <label>Enter customer name</label>
                            <input type="text" id="search_keyword" class="form-control" placeholder="Search customer name">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary">
                                <i class="fa fa-search"></i> Search
                            </button>
                        </div>
                    </form>
                </div>

                <div class="col-md-12 p-2">
                    <table class="table" width="100%">
                        <tbody class="load-search"></tbody>
                    </table>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>
@endsection

{{-- scripts --}}
@section('scripts')
	<script type="text/javascript">
		function searchCustomer() {
            // body..
            var token   = $("#token").val();
            var keyword = $("#search_keyword").val();

            var params = {
                _token: token,
                keyword: keyword
            };

            $.post('{{ url('agent/search/customer/2') }}', params, function(data, textStatus, xhr) {
                /*optional stuff to do after success */
                buildDepositView(data);
                // console log value
                console.log(data);
            });

            // return void
            return false;
        }

        function buildDepositView(data) {
            // body...
            var sn = 0;
            $(".load-search").html("");
            // $(".customer-info").html("");
            $.each(data, function(index, val) {
                sn++;
                $(".load-search").append(`
                    <tr>
                        <td>${val.name}</td>
                        <td>
                            <input type="text" class="form-control" id="amount_${sn}" placeholder="0.00" />
                        </td>
                        <td>
                            <a href="javascript:void(0);" id="repay-btn" onclick="acceptDeposit(${val.id}, ${val.account.id}, ${sn})">
                                Deposit
                            </a>
                        </td>
                    </tr>
                `);
            });
        }

        function acceptDeposit(user_id, account_id, sn) {
            // processing
            $("#repay-btn").html("Processing");

             // body..
            var token   = $("#token").val();
            var amount  = $("#amount_"+sn).val();

            var params = {
                _token: token,
                amount: amount,
                user_id: user_id,
                account_id: account_id
            };

            $.post('{{ url('agent/savings/deposit') }}', params, function(data, textStatus, xhr) {
                /*optional stuff to do after success */
                // buildDepositView(data);
                if(data.status == "success"){
                    swal(
                        "success",
                        data.message,
                        data.status
                    );
                    $("#amount_"+sn).val("");
                    $("#repay-btn").html("Deposit");
                    // clear sms queue
                    clearSmsQueue();
                }else{
                    swal(
                        "oops",
                        data.message,
                        data.status
                    );
                    $("#repay-btn").html("Deposit");
                }
            });
        }
	</script>
@endsection