
<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--  -->
    <!--    Document Title-->
    <!-- =============================================-->
    <title>@yield('title')</title>
    <!--  -->
    <!--    Favicons-->
    <!--    =============================================-->
    <link rel="icon" href="/site-logo/logo-transparent.png">
    <link rel="manifest" href="/site-assets/images/favicons/manifest.json">
    <meta name="msapplication-TileImage" content="/site-logo/logo-120x110.png">
    <meta name="theme-color" content="#5518ab">
    <!--  -->
    <!--    Stylesheets-->
    <!--    =============================================-->
    <!-- Default stylesheets-->
    <link href="/site-assets/lib/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Template specific stylesheets-->
    <link href="/site-assets/lib/loaders.css/loaders.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700|comic+sans:300,400,600,700,800" rel="stylesheet">
    <link href="/site-assets/lib/iconsmind/iconsmind.css" rel="stylesheet">
    <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
    <link href="/site-assets/lib/hamburgers/dist/hamburgers.min.css" rel="stylesheet">
    <link href="/site-assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="/site-assets/lib/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="/site-assets/lib/owl.carousel/dist/assets/owl.theme.default.min.css" rel="stylesheet">
    <link href="/site-assets/lib/remodal/dist/remodal.css" rel="stylesheet">
    <link href="/site-assets/lib/remodal/dist/remodal-default-theme.css" rel="stylesheet">
    <link href="/site-assets/lib/flexslider/flexslider.css" rel="stylesheet">
    <link href="/site-assets/lib/lightbox2/dist/css/lightbox.css" rel="stylesheet">
    <!-- Main stylesheet and color file-->
    <link href="/site-assets/css/style.css" rel="stylesheet">
    <link href="/site-assets/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
    <link rel="stylesheet" type="text/css" href="/css/xmass.css">

    <!-- Google Analytics -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-130934816-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-130934816-1');
    </script>


    <!-- Start of Async Drift Code -->
    <script>
        "use strict";

        !function() {
          var t = window.driftt = window.drift = window.driftt || [];
          if (!t.init) {
            if (t.invoked) return void (window.console && console.error && console.error("Drift snippet included twice."));
            t.invoked = !0, t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
            t.factory = function(e) {
              return function() {
                var n = Array.prototype.slice.call(arguments);
                return n.unshift(e), t.push(n), t;
              };
            }, t.methods.forEach(function(e) {
              t[e] = t.factory(e);
            }), t.load = function(t) {
              var e = 3e5, n = Math.ceil(new Date() / e) * e, o = document.createElement("script");
              o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + n + "/" + t + ".js";
              var i = document.getElementsByTagName("script")[0];
              i.parentNode.insertBefore(o, i);
            };
          }
        }();
        drift.SNIPPET_VERSION = '0.3.1';
        drift.load('pgk3dv6kmdk6');
    </script>
    <!-- End of Async Drift Code -->
</head>

<body data-spy="scroll" data-target=".inner-link" data-offset="60">
    <main>
    	<div class="loading" id="preloader">
	        <div class="loader h-100 d-flex align-items-center justify-content-center">
	            <div class="line-scale">
	                <div></div>
	                <div></div>
	                <div></div>
	                <div></div>
	                <div></div>
	            </div>
	        </div>
	    </div>

        <section class="background-primary py-3 d-none d-sm-block">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-auto d-none d-lg-block"><span class="fa fa-phone color-warning fw-800 icon-position-fix"></span>
                        <p class="ml-2 mb-0 fs--1 d-inline color-white fw-700">
                            <b><u>Phone:</u></b> +2348180283377, +2348031377010.  
                            <b><u>Working Hours:</u></b>  Monday-Friday 8am-5pm CAT, Sat 10am-3pm CAT, Sunday-Close
                        </p>
                    </div>
                    <div class="col-auto"><span class="fa fa-clock-o color-warning fw-800 icon-position-fix"></span><a class="ml-2 mb-0 fs--1 d-inline color-white fw-700" href="javascript:void(0);">Time: <span class="timer"></span></a>
                    </div>
                </div>
                <!--/.row-->
            </div>
            <!--/.container-->
        </section>

        <div class="znav-white znav-container sticky-top navbar-elixir" id="znav-container">
            <div class="container">
                <nav class="navbar navbar-expand-lg">
                    <a class="navbar-brand overflow-hidden pr-3" href="{{url('/')}}">
                        {{-- <img src="/site-assets/images/logo-dark.png" alt="" /> --}}
                        {{-- <img src="/site-logo/logo-transparent.png" width="42" height="32" alt="" /> --}}
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                        <div class="hamburger hamburger--emphatic">
                            <div class="hamburger-box">
                                <div class="hamburger-inner"></div>
                            </div>
                        </div>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul class="navbar-nav fs-0 fw-700">
                            <li>
                                <a href="{{ url('/') }}">
                                    <img src="{{asset('site-logo/logo-transparent.png')}}" width="42" height="28" alt="" />
                                    <span style="position: relative;margin-left:-5px;">errisCoop</span>
                                </a>
                            </li>

                            <li><a href="{{ url('about') }}">About Us</a>
                                <ul class="dropdown fs--1">
                                    <li><a href="{{ url('about') }}/#about">Who We Are</a></li>
                                    <li><a href="{{ url('about') }}/#vision">Vision</a></li>
                                    <li><a href="{{ url('about') }}/#mission">Mission</a></li>
                                    <li><a href="{{ url('about') }}/#objective">Objective</a></li>
                                    <li><a href="{{ url('about') }}/#core-value">Core Value</a></li>
                                    <li><a href="{{ url('about') }}/#leadership">Leadership Team</a></li>
                                </ul>
                            </li>

                            {{-- <li><a href="{{ url('services') }}">Services</a>
                                <ul class="dropdown fs--1">
                                    <li><a href="{{ url('services') }}">Cash Deposit</a></li>
                                    <li><a href="{{ url('services') }}">Cash Withdrawal</a></li>
                                    <li><a href="{{ url('services') }}">Funds Transfer</a></li>
                                    <li><a href="{{ url('services') }}">Balance Enquiry</a></li>
                                    <li><a href="{{ url('services') }}">Bills Payments</a></li>
                                    <li><a href="{{ url('services') }}">Alert</a></li>
                                </ul>
                            </li> --}}

                            <li><a href="{{ url('savings') }}">Micro Savings</a>
                                <ul class="dropdown fs--1">
                                    <li><a href="{{ url('savings') }}">Spare Change Savers</a></li>
                                    <li><a href="{{ url('savings') }}">Merriscoop Pay</a></li>
                                    <li><a href="{{ url('savings') }}">Ajo pro</a></li>
                                    <li><a href="{{ url('savings') }}">Ajo Regular</a></li>
                                    <li><a href="{{ url('savings') }}">Peer Contribution Scheme</a></li>
                                    <li><a href="{{ url('savings') }}">Festival Savers Scheme</a></li>
                                    <li><a href="{{ url('savings') }}">Kiddie Piggy Box</a></li>
                                    <li><a href="{{ url('savings') }}">Association Micro Target Savers</a></li>
                                    <li><a href="{{ url('savings') }}">Micro Team Savers</a></li>
                                    <li><a href="{{ url('savings') }}">Pilgrimage Micro Savers</a></li>
                                </ul>
                            </li>
                            <li><a href="{{ url('lending') }}">Lending</a>
                                <ul class="dropdown fs--1">
                                    <li>
                                        <a href="{{ url('lending') }}">
                                            E-lending Channels
                                        </a>
                                        <ul class="dropdown fs--1">
                                            <li class="pl-2"><a href="{{ url('lending') }}">- Online Loans</a></li>
                                            <li class="pl-2"><a href="{{ url('lending') }}">- Mobile Loans</a></li>
                                            <li class="pl-2"><a href="{{ url('lending') }}">- Social Lending Loans</a></li>
                                        </ul>
                                        
                                    </li>

                                    <li>
                                        <a href="{{ url('lending') }}">
                                            Pay Day Loans
                                        </a>
                                            <ul class="dropdown fs--1">
                                                <li class="pl-2">
                                                    <a href="{{ url('lending') }}">- Salary Overdraft </a>
                                                </li>
                                                <li class="pl-2">
                                                    <a href="{{ url('lending') }}">- Salary Advance </a>
                                                </li>
                                            </ul>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{ url('lending') }}">
                                            Personal & Emergency Loans
                                        </a>
                                        <ul class="dropdown fs--1">
                                            <li class="pl-2">
                                                <a href="{{ url('lending') }}">- Rental Loans</a>
                                            </li>
                                            <li class="pl-2">
                                                <a href="{{ url('lending') }}">- Education/ School Fees Loans</a>
                                            </li>
                                            <li class="pl-2">
                                                <a href="{{ url('lending') }}">- Consumer Assets Loans</a>
                                            </li>
                                            <li class="pl-2">
                                                <a href="{{ url('lending') }}">- Health Care Bill Loans</a>
                                            </li>

                                        </ul>
                                    </li>

                                    <li>
                                        <a href="{{ url('lending') }}">
                                            Nano & Micro Business Loans
                                        </a>
                                        <ul class="dropdown fs--1">
                                            <li class="pl-2">
                                                <a href="{{ url('lending') }}">- Individual Loans</a>
                                            </li>
                                            <li class="pl-2">
                                                <a href="{{ url('lending') }}">- Micro Loans</a>
                                            </li>
                                            <li class="pl-2">
                                                <a href="{{ url('lending') }}">- Freight Forwarders Loans</a>
                                            </li>
                                            <li class="pl-2">
                                                <a href="{{ url('lending') }}">- Commercial Vehicle purchase & Repairs Loans</a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li>
                                        <a href="{{ url('lending') }}">
                                            Cooperative Loans
                                        </a>
                                         <ul class="dropdown fs--1">
                                            <li class="pl-2">
                                                <a href="{{ url('lending') }}">- Group Loans</a>
                                            </li>
                                            <li class="pl-2">
                                                <a href="{{ url('lending') }}">- Cooperative Loans</a>
                                            </li>
                                        </ul>
                                    </li>
                                    
                                    <li>
                                        <a href="{{ url('lending') }}">
                                            Agricultural Loans
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="{{ url('careers') }}">Career</a></li>
                            <li><a class="d-block mr-md-9" href="{{ url('contact') }}">Contact</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        @yield('contents')

        <section class="background-primary text-center py-4">
            <div class="container">
                <div class="row align-items-center" style="opacity: 0.85;">
                    <div class="col-sm-3 text-sm-left">
                        <a href="{{ url('/') }}">
                            <img src="{{asset('site-logo/logo-transparent.png')}}" width="42" height="32" alt="" /><span class="text-white">errisCoop</span>
                        </a>
                    </div>
                    <div class="col-sm-6 mt-3 mt-sm-0">
                        <p class="color-white lh-6 mb-0 fw-600">&copy; Copyright 2017 - {{ Date("Y") }} MerrisCoop.</p>
                    </div>
                    <div class="col text-sm-right mt-3 mt-sm-0">
                        <a class="color-white" href="http://ekpotoliberty.com/" target="_blank">Developed by e-Devs</a>
                    </div>
                </div>
                <!--/.row-->
                <hr />
                <div class="row">
                    <div class="col-sm-12 text-center small">
                        <a href="javascript:void(0);" class="text-white">
                            MerrisCoop is authorized, regulated by government agencies powered as private lenders under unique identifier licensed #0000114.
                        </a>
                    </div>
                </div>
            </div>
            <!--/.container-->
        </section>
    </main>
    <!--  -->
    <!--    JavaScripts-->
    <!--    =============================================-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    <script src="/site-assets/lib/jquery/dist/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="/site-assets/lib/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/site-assets/lib/imagesloaded/imagesloaded.pkgd.min.js"></script>
    <script src="/site-assets/lib/gsap/src/minified/TweenMax.min.js"></script>
    <script src="/site-assets/lib/gsap/src/minified/plugins/ScrollToPlugin.min.js"></script>
    <script src="/site-assets/lib/CustomEase.min.js"></script>
    <script src="/site-assets/js/config.js"></script>
    <script src="/site-assets/js/zanimation.js"></script>
    <script src="/site-assets/lib/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="/site-assets/lib/remodal/dist/remodal.js"></script>
    <script src="/site-assets/lib/lightbox2/dist/js/lightbox.js"></script>
    <script src="/site-assets/lib/flexslider/jquery.flexslider-min.js"></script>
    <script src="/site-assets/js/core.js"></script>
    <script src="/site-assets/js/main.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
    <!-- Sweet-Alert  -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js"></script>
    <script src="/js/xmass.js"></script>
    <script type="text/javascript">
        setInterval(function (){
            var date = new Date();
            $('.timer').html(` ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()} `);
        }, 1000);

        window.addEventListener("load", function(){
        window.cookieconsent.initialise({
          "palette": {
            "popup": {
              "background": "#5518ab"
            },
            "button": {
              "background": "#e62576"
            }
          },
          "content": {
            "message": "This site uses cookies. By continuing to browse MerrisCoop, you are agreeing to use our site cookies. Read our Policy here",
            "href": "https://merriscoop.com/cookies-policy"
          }
        })});
    </script>
    @yield('scripts')
</body>

</html>