
<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--  -->
    <!--    Document Title-->
    <!-- =============================================-->
    <title>@yield('title')</title>
    <!--  -->
    <!--    Favicons-->
    <!--    =============================================-->
    <link rel="icon" href="/site-logo/logo-transparent.png">
    <link rel="manifest" href="/site-assets/images/favicons/manifest.json">
    <meta name="msapplication-TileImage" content="/site-logo/logo-120x110.png">
    <meta name="theme-color" content="#5518ab">
    <!--  -->
    <!--    Stylesheets-->
    <!--    =============================================-->
    <!-- Default stylesheets-->
    <link href="/site-assets/lib/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Template specific stylesheets-->
    <link href="/site-assets/lib/loaders.css/loaders.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700|comic+sans:300,400,600,700,800" rel="stylesheet">
    <link href="/site-assets/lib/iconsmind/iconsmind.css" rel="stylesheet">
    <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
    <link href="/site-assets/lib/hamburgers/dist/hamburgers.min.css" rel="stylesheet">
    <link href="/site-assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="/site-assets/lib/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="/site-assets/lib/owl.carousel/dist/assets/owl.theme.default.min.css" rel="stylesheet">
    <link href="/site-assets/lib/remodal/dist/remodal.css" rel="stylesheet">
    <link href="/site-assets/lib/remodal/dist/remodal-default-theme.css" rel="stylesheet">
    <link href="/site-assets/lib/flexslider/flexslider.css" rel="stylesheet">
    <link href="/site-assets/lib/lightbox2/dist/css/lightbox.css" rel="stylesheet">
    <!-- Main stylesheet and color file-->
    <link href="/site-assets/css/style.css" rel="stylesheet">
    <link href="/site-assets/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Data Table-->
    <link rel="stylesheet" type="text/css" href="/assets/vendor_components/datatable/datatables.min.css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

    <style type="text/css">
        body {
            font-family: 'Montserrat';
            font-size: 13px;
        }
    </style>
</head>

<body data-spy="scroll" data-target=".inner-link" data-offset="60">
    <input type="hidden" id="token" value="{{ csrf_token() }}" name="">
    <main>
    	<div class="loading" id="preloader">
	        <div class="loader h-100 d-flex align-items-center justify-content-center">
	            <div class="line-scale">
	                <div></div>
	                <div></div>
	                <div></div>
	                <div></div>
	                <div></div>
	            </div>
	        </div>
	    </div>

        <div class="znav-white znav-container sticky-top navbar-elixir" id="znav-container">
            <div class="container">
                <nav class="navbar navbar-expand-lg">
                    <a class="navbar-brand overflow-hidden pr-3" href="{{url('/')}}">
                        {{-- <img src="/site-assets/images/logo-dark.png" alt="" /> --}}
                        {{-- <img src="/site-logo/logo-transparent.png" width="42" height="32" alt="" /> --}}
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                        <div class="hamburger hamburger--emphatic">
                            <div class="hamburger-box">
                                <div class="hamburger-inner"></div>
                            </div>
                        </div>

                    </button>
                    
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul class="navbar-nav fs-0 fw-700">
                            <li>
                                @if(Auth::check())
                                    <a href="{{ url('/agent/dashboard') }}">
                                        Agent ID: {{ Auth::user()->email }}
                                    </a>
                                @endif
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        @yield('contents')
    </main>
    <!--  -->
    <!--    JavaScripts-->
    <!--    =============================================-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    <script src="/site-assets/lib/jquery/dist/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="/site-assets/lib/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/site-assets/lib/imagesloaded/imagesloaded.pkgd.min.js"></script>
    <script src="/site-assets/lib/gsap/src/minified/TweenMax.min.js"></script>
    <script src="/site-assets/lib/gsap/src/minified/plugins/ScrollToPlugin.min.js"></script>
    <script src="/site-assets/lib/CustomEase.min.js"></script>
    <script src="/site-assets/js/config.js"></script>
    <script src="/site-assets/js/zanimation.js"></script>
    <!-- Hotjar Tracking Code for http://markup.themewagon.com/tryelixir-->
    <script src="/site-assets/lib/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="/site-assets/lib/remodal/dist/remodal.js"></script>
    <script src="/site-assets/lib/lightbox2/dist/js/lightbox.js"></script>
    <script src="/site-assets/lib/flexslider/jquery.flexslider-min.js"></script>
    <script src="/site-assets/js/core.js"></script>
    <script src="/site-assets/js/main.js"></script>
    <!-- Sweet-Alert  -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js"></script>
    <!-- Numerial Js -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
    <!-- MomentJs-->
    <script src="https://momentjs.com/downloads/moment.js"></script>
    <!-- This is data table -->
    <script src="/assets/vendor_components/datatable/datatables.min.js"></script>
    <!-- Select2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <!-- Typeahead -->
    <script src="{{ asset('js/typeahead.js') }}"></script>
    <!-- Clear SMS Queue Macro-->
    <script type="text/javascript">
        function clearSmsQueue() {
            $.get('{{ url('api/clear/sms/queue') }}', function(data) {
                /*optional stuff to do after success */
                console.log(data);
            });
        }
    </script>
    @yield('scripts')
</body>

</html>