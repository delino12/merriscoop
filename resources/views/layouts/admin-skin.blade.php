<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="">
	    <meta name="author" content="">
	    <link rel="icon" href="/site-logo/logo-transparent.png">
	    <title>@yield('title')</title>
	    
		<!-- Bootstrap 4.0-->
		<link rel="stylesheet" href="/assets/vendor_components/bootstrap/dist/css/bootstrap.css">
		
		<!-- Bootstrap extend-->
		<link rel="stylesheet" href="/css/bootstrap-extend.css">
		
		<!-- theme style -->
		<link rel="stylesheet" href="/css/master_style.css">
		
		<!-- Superieur Admin skins -->
		<link rel="stylesheet" href="/css/skins/_all-skins.css">
		
		<!-- daterange picker -->	
		<link rel="stylesheet" href="/assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css">
		
		<!-- Morris charts -->
		<link rel="stylesheet" href="/assets/vendor_components/morris.js/morris.css">
		
		<!-- Data Table-->
		<link rel="stylesheet" type="text/css" href="/assets/vendor_components/datatable/datatables.min.css"/>

		<!-- Slick -->
		<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	</head>
	<input type="hidden" id="token" value="{{csrf_token()}}">
	<body class="hold-transition skin-blue sidebar-mini dark">
	  <div class="wrapper">
	    @include('__components.header')
	    @include('__components/sidebar')

	    <!-- Content Wrapper. Contains page content -->
	    <div class="content-wrapper">
	    
	      <!-- Content Header (Page header) -->	  
	    	<div class="content-header">
	    		<div class="d-flex align-items-center">
	    			<div class="mr-auto">
	    				<h3 class="page-title">Dashboard</h3>
	    				<div class="d-inline-block align-items-center">
	    					<nav>
	    						<ol class="breadcrumb">
	    							<li class="breadcrumb-item"><a href="javascript:void(0);"><i class="mdi mdi-home-outline"></i></a></li>
	    							<li class="breadcrumb-item active" aria-current="page">Control</li>
	    						</ol>
	    					</nav>
	    				</div>
	    			</div>
	    		</div>
	    	</div>

	      <!-- Main content -->
	      <section class="content">
	        @yield('contents')
	  	  </section>
	      <!-- /.content -->
	    </div>
	    @include('__components.footer')
	    
	    <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
	    <div class="control-sidebar-bg"></div>
	  </div>
	  <!-- ./wrapper -->
	  	
		 
		  
		<!-- jQuery 3 -->
		<script src="/assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js"></script>
		
		<!-- jQuery UI 1.11.4 -->
		<script src="/assets/vendor_components/jquery-ui/jquery-ui.js"></script>
		
		<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
		<script>
		  $.widget.bridge('uibutton', $.ui.button);
		</script>
		
		<!-- popper -->
		<script src="/assets/vendor_components/popper/dist/popper.min.js"></script>
		
		<!-- date-range-picker -->
		<script src="/assets/vendor_components/moment/min/moment.min.js"></script>
		<script src="/assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js"></script>
		
		<!-- Bootstrap 4.0-->
		<script src="/assets/vendor_components/bootstrap/dist/js/bootstrap.js"></script>	
		
		<!-- ChartJS -->
		<script src="/assets/vendor_components/chart.js-master/Chart.min.js"></script>
		
		<!-- Slimscroll -->
		<script src="/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js"></script>
		
		<!-- FastClick -->
		<script src="/assets/vendor_components/fastclick/lib/fastclick.js"></script>
			
		<!-- Morris.js charts -->
		<script src="/assets/vendor_components/raphael/raphael.min.js"></script>
		<script src="/assets/vendor_components/morris.js/morris.min.js"></script>

		<!-- This is data table -->
	  	<script src="/assets/vendor_components/datatable/datatables.min.js"></script>
		
		<!-- Superieur Admin App -->
		<script src="/js/template.js"></script>
		
		<!-- Superieur Admin dashboard demo (This is only for demo purposes) -->
		<script src="/js/pages/dashboard.js"></script>
		
		<!-- Superieur Admin for demo purposes -->
		<script src="/js/demo.js"></script>

		<!-- Sweet-Alert  -->
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.4/dist/sweetalert2.all.min.js"></script>
		
		<!-- Numerial Js -->
		<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>

		<!-- Slick -->
		<script src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

		<!-- MomentJs-->
		<script src="https://momentjs.com/downloads/moment.js"></script>

		<!-- Select2 -->
  		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

  		<!-- Typeahead -->
  		<script src="{{ asset('js/typeahead.js') }}"></script>

  		<!-- Clear SMS Queue Macro-->
  		<script type="text/javascript">
  			function clearSmsQueue() {
  				$.get('{{ url('api/clear/sms/queue') }}', function(data) {
  					/*optional stuff to do after success */
  					console.log(data);
  				});
  			}
  		</script>
	  	
	  	@yield('scripts')
	</body>
</html>
