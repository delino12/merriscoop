@extends('layouts.agent-skin')

{{-- title --}}
@section('title')
	MerrisCoop | Agent Login
@endsection

{{-- contents --}}
@section('contents')
    <section class="background-11">
        <div class="container">
            <div class="row align-items-stretch justify-content-center">
                <div class="col-12 mt-4">
                    <div class="background-white p-5 h-100 radius-secondary">
                        <h5>Teller Login</h5>
                        <form class="aform mt-3" method="post" onsubmit="return loginAgent()">
                            <div class="row">
                                <div class="col-12">
                                    <input class="form-control background-white" id="a-code" type="text" placeholder="Agent Code" required>
                                </div>
                                <div class="col-12 mt-4">
                                    <input class="form-control background-white" id="a-pin" type="password" placeholder="Agent Pin" required>
                                </div>
                                <div class="col-12 mt-4">
                                    <div class="row">
                                        <div class="col-auto">
                                            <button class="btn btn-md-lg btn-primary" id="a-btn" type="Submit">
                                            	<span class="color-white fw-600">Login</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>
@endsection

{{-- scripts --}}
@section('scripts')
	<script type="text/javascript">
		// send contact message
        function loginAgent(){
            $("#a-btn").html("Processing....");

            var token   = '{{ csrf_token() }}';
            var code    = $("#a-code").val();
            var pin     = $("#a-pin").val();

            var params  = {
                _token: token,
                code: code,
                pin: pin,
            };

            // post
            $.post('{{ url('agent/login') }}', params, function(data, textStatus, xhr) {
                /*optional stuff to do after success */
                if(data.status == "success"){
                    // reset form
                    window.location.href = "/agent/dashboard";
                }else{
                    swal(
                        "oops",
                        data.message,
                        data.status
                    );
                }

                // submit 
                $("#a-btn").html("Login");
            });

            // void
            return false;
        }
	</script>
@endsection