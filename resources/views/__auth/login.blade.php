@extends('layouts.auth-skin')

{{-- title --}}
@section('title')
	Merriscoop Admin - Login
@endsection


{{-- contents --}}
@section('contents')
	<div class="auth-2-outer row align-items-center h-p100 m-0">

		<div class="auth-2">
		  <div class="auth-logo font-size-40">
			<a href="javascript:void(0);" class="text-white"><b>Merriscoop</b> Admin</a>
		  </div>
		  <!-- /.login-logo -->
		  <div class="auth-body">
		  	<h1 style="position: absolute;left: 0; margin-top: 20px;margin-left: 30px;">
		  		Login Admin

		  		<ul style="font-size: 12px;">
		  			<li>Welcome to MerrisCoop Admin Panel</li>
		  			<li>Site Ip tracker is active ip: {{ Request::ip() }}</li>
		  			<li>Please ensure you are authorized to view this page.</li>
		  			<li>Software developed By E-Devs Limited</li>
		  		</ul>
		  	</h1>
			<p class="auth-msg">Sign in to start your session</p>

			<form onsubmit="return false" method="post" class="form-element">
			  <div class="form-group has-feedback">
				<input type="email" class="form-control" id="email" placeholder="Email" required="">
				<span class="ion ion-email form-control-feedback"></span>
			  </div>
			  <div class="form-group has-feedback">
				<input type="password" class="form-control" id="password" placeholder="Password" required="">
				<span class="ion ion-locked form-control-feedback"></span>
			  </div>
			  <div class="row">
				<div class="col-6">
				  <div class="checkbox">
					<input type="checkbox" id="basic_checkbox_1">
					<label for="basic_checkbox_1">Remember Me</label>
				  </div>
				</div>
				<!-- /.col -->
				<div class="col-6">
				 <div class="fog-pwd">
					<a href="javascript:void(0)" class="text-white"><i class="ion ion-locked"></i> Forgot pwd?</a><br>
				  </div>
				</div>
				<!-- /.col -->
				<div class="col-12 text-center">
				  <button type="submit" id="signin-btn" class="btn btn-block mt-10 btn-success">SIGN IN</button>
				</div>
				<!-- /.col -->
			  </div>
			</form>
		  </div>
		</div>
	</div>
@endsection

{{-- scripts --}}
@section('scripts')
	<script type="text/javascript">
		// process login
		$("#signin-btn").click(function(e){
			e.preventDefault();

			$("#signin-btn").html(`
				<i class="fa fa-spinner fa-spin"></i> processing...
			`);

			var token 		= $("#token").val();
			var email 		= $("#email").val();
			var password 	= $("#password").val();
			var remember 	= $("#basic_checkbox_1").val();

			var params = {
				_token: token,
				email: email,
				password: password,
				remember: remember
			}

			// console log value
			// console.log(params);

			$.post('{{ url('admin/login') }}', params, function(data, textStatus, xhr) {
				/*optional stuff to do after success */
				if(data.status == "success"){
					window.location.href = "/admin/dashboard";
				}else{
					swal("oops!", data.message, data.status);
					$("#signin-btn").html(`
						SIGN IN
					`);
				}
			});

			// void form
			return false;
		});
	</script>
@endsection