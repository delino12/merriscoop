<!-- /.content-wrapper -->
<footer class="main-footer">
	&copy; {{ Date("Y") }} <a href="http://www.ekpotoliberty.com" target="_blank">Developed by E-devs</a>. 
	All Rights Reserved.
</footer>