<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar-->
  <section class="sidebar">
    <!-- sidebar menu-->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="user-profile treeview">
        <a href="{{ url('/') }}">
		       <img src="/images/user-info.png" alt="user">
          <span>
    		    <span class="d-block font-weight-600 font-size-16">{{ Auth::user()->name }}</span>
            <small>{{ Auth::user()->name }}</small>
          </span>
          <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
          </span>
        </a>
	      <ul class="treeview-menu">
          <li><a href="javascript:void()"><i class="fa fa-user mr-5"></i>My Profile </a></li>
    			<li><a href="javascript:void()"><i class="fa fa-money mr-5"></i>My Balance</a></li>
    			<li><a href="javascript:void()"><i class="fa fa-envelope-open mr-5"></i>Inbox</a></li>
    			<li><a href="javascript:void()"><i class="fa fa-cog mr-5"></i>Account Setting</a></li>
		       <li><a href="{{ url('admin/logout') }}"><i class="fa fa-power-off mr-5"></i>Logout</a></li>
        </ul>
      </li>
      <li class="header nav-small-cap"><i class="mdi mdi-drag-horizontal mr-5"></i>ADMIN ACCESS</li>
      <li class="treeview active">
        <a href="javascript:void(0);">
          <i class="mdi mdi-view-dashboard"></i>
          <span>Dashboard</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="active">
            <a href="{{ url('/admin/dashboard') }}">
              <i class="mdi mdi-toggle-switch-off"></i>
              Dashboard
            </a>
          </li>
      </li> 
	
      <li>
        <a href="{{ url('admin/new-client') }}">
          <i class="mdi mdi-content-copy"></i>
          <span>Clients (New Customer)</span>
        </a>
      </li>

      <li>
        <a href="{{ url('admin/all-customers') }}">
          <i class="mdi mdi-content-copy"></i>
          <span>All Customers</span>
        </a>
      </li>

      <li>
        <a href="{{ url('admin/transactions') }}">
          <i class="mdi mdi-content-copy"></i>
          <span>Transactions</span>
        </a>
      </li>

      <li>
        <a href="{{ url('admin/active-savings') }}">
          <i class="mdi mdi-content-copy"></i>
          <span>Savings</span>
        </a>
      </li>

      <li>
        <a href="{{ url('admin/quick-deposit') }}">
          <i class="mdi mdi-content-copy"></i>
          <span>Quick Deposit (Savers)</span>
        </a>
      </li>

      <li>
        <a href="{{ url('admin/quick-withdraw') }}">
          <i class="mdi mdi-content-copy"></i>
          <span>Instant Withdraw (Savers)</span>
        </a>
      </li>

      <li>
        <a href="{{ url('admin/reports') }}">
          <i class="mdi mdi-content-copy"></i>
          <span>Reports</span>
        </a>
      </li>

      <li>
        <a href="{{ url('admin/audit/trails') }}">
          <i class="mdi mdi-content-copy"></i>
          <span>Audit Trail</span>
        </a>
      </li>

      <li class="header nav-small-cap"><i class="mdi mdi-drag-horizontal mr-5"></i> PEER SYSTEM</li>
      <li>
        <a href="{{ url('admin/ajo/general') }}">
          <i class="mdi mdi-mailbox"></i> 
          <span>
            Individuals
          </span>
        </a>
      </li>

      <li>
        <a href="{{ url('admin/ajo/group') }}">
          <i class="mdi mdi-mailbox"></i> 
          <span>
            Groups
          </span>
        </a>
      </li>

      <li class="header nav-small-cap"><i class="mdi mdi-drag-horizontal mr-5"></i> LOAN SYSTEM</li>
      <li>
        <a href="{{ url('admin/active-loans') }}">
          <i class="mdi mdi-content-copy"></i>
          <span>Loans</span>
        </a>
      </li>

      <li>
        <a href="{{ url('admin/approved-loans') }}">
          <i class="mdi mdi-content-copy"></i>
          <span>Approved Loans</span>
        </a>
      </li>

      <li>
        <a href="{{ url('admin/pending-loans') }}">
          <i class="mdi mdi-content-copy"></i>
          <span>Pending Loans</span>
        </a>
      </li>

      <li>
        <a href="{{ url('admin/settled-loans') }}">
          <i class="mdi mdi-content-copy"></i>
          <span>Settled Loans (Flags)</span>
        </a>
      </li>
    
    	<li class="header nav-small-cap"><i class="mdi mdi-drag-horizontal mr-5"></i>EXTRA</li>
      <li>
        <a href="{{ url('admin/settings') }}">
          <i class="mdi mdi-notification-clear-all"></i>
          <span>Settings</span>
        </a>
      </li>
    	<li>
        <a href="{{ url('admin/config-admin') }}">
          <i class="mdi mdi-notification-clear-all"></i>
          <span>Configuration</span>
        </a>
      </li>
    	<li>
        <a href="{{ url('admin/logout') }}">
          <i class="mdi mdi-directions"></i>
		        <span>Log Out</span>
        </a>
      </li> 
      
    </ul>
  </section>
</aside>