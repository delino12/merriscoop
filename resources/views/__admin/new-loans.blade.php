@extends('layouts.admin-skin')

{{-- title --}}
@section('title')
  {{ env("APP_NAME") }} | Loan Applications
@endsection

{{-- contents --}}
@section('contents')
  <div class="row">
    <div class="col-12 col-lg-12">
      <!-- AREA CHART -->
      <div class="box">
        <div class="box-header with-border">
          <h4 class="box-title"><i class="fa fa-edit"></i> Add New</h4>
          <ul class="box-controls pull-right">
            <li><a class="box-btn-close" href="javascript:void(0);"></a></li>
            <li><a class="box-btn-slide" href="javascript:void(0);"></a></li> 
            <li><a class="box-btn-fullscreen" href="javascript:void(0);"></a></li>
          </ul>
        </div>
        <div class="box-body">
          <!-- /.box-header -->
          <form class="form" method="POST" onsubmit="return applyNewLoan()">
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Select Loan Reciepient</label>
                    <select class="form-control" id="user_id" onchange="selectCustomerAccount()">
                      <option value=""> -- select customer -- </option>
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <label>Startup Amount</label>
                  <div class="row">
                    <div class="col-md-6">
                      <input type="number" onkeyup="formatAmount()" class="form-control" id="amount" placeholder="&#8358; 10,000.00" required="">
                    </div>
                    <div class="col-md-6">
                      <div class="form-control" id="preview-amount"></div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="account_option"></div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Why do i want to apply for this loan?</label>
                    <textarea rows="5" class="form-control" id="narration" placeholder="Explain why you have decided to open a new loan account?"></textarea>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="loan-calculator-screen"></div>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary btn-outline">
                <i class="ti-save-alt"></i> Send Request
              </button>
            </div>  
          </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>  

    <div class="col-12 col-lg-12">
        <div class="box">
              <div class="box-header with-border">
                <h4 class="box-title">
                  <i class="fa fa-users"></i> Customers Account
                </h4>
              </div>
          <div class="box-body">
            <div class="table-responsive">
              <table id="invoice-list" class="table table-hover no-wrap" data-page-size="10">
                  <thead>
                      <tr>
                          <th>#</th>
                          <th>Name</th>
                          <th>Phone</th>
                          <th>Account Type</th>
                          <th>Balance</th>
                          <th>Last updated</th>
                          <th>View</th>
                      </tr>
                  </thead>
                  <tbody class="load-all-customers">
                      <tr>
                          <td>Loading...</td>
                      </tr>
                  </tbody>
              </table>
          </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /. box -->
    </div>             
  </div>
@endsection

{{-- scripts section --}}
@section('scripts')
  <script type="text/javascript">
    $(document).ready(function (){
      loadAllCustomers();
    });

    // check account option
    function showOption() {
      $(".account_option").html(`
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Interest Rate(%)  <span class="small text-danger">Strict Mode Only</span></label>
              <input type="number" onkeyup="calculateLoanInterest()" id="interest" placeholder="Enter interest rate" class="form-control" />
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Loan Duration (Days)</label>
              <input type="number" id="duration" min="1" placeholder="Enter loan duration" class="form-control" />
            </div>
          </div>
        </div>
      `).fadeIn();
    }

    // loan interest
    function calculateLoanInterest() {
      var interest  = $("#interest").val();
      var amount    = $("#amount").val();
      if(amount == ""){
        swal(
          "oops",
          "Amount is required, enter a valid amount!",
          "error"
        );
        // void calculation
        return false;
      }

      var percent = parseFloat(interest / 100);
      var charge  = parseFloat(percent * amount);
      var payable = parseFloat(charge) + parseFloat(amount);

      $(".loan-calculator-screen").html(`
        <table class="table">
          <tr>
            <td><span class="text-green">Details</span></td>
            <td><span class="text-green">Consideration</span></td>
          </tr>
          <tr>
            <td>Amount:</td>
            <td>&#8358;${numeral(amount).format("0,0.00")}</td>
          </tr>
          <tr>
            <td>Interest:</td>
            <td>&#8358;${numeral(charge).format("0,0.00")}</td>
          </tr>
          <tr>
            <td>Tax:</td>
            <td>&#8358;0.00</td>
          </tr>
          <tr>
            <td>Total:</td>
            <td>&#8358;${numeral(payable).format("0,0.00")}</td>
          </tr>
        </table>
      `);
    }

    // load all customers
    function loadAllCustomers() {
      $.get('{{ url('admin/load/all/customers') }}', function(data) {
        var sn = 0;
        $(".load-all-customers").html("");
        $.each(data, function(index, val) {
          sn++;

          var shadeRow;
          if(val.type == "savings"){
            shadeRow = `class="text-green"`; 
          }else{
            shadeRow = `class="text-white"`; 
          }

          // console log value
          // console.log(val);
          $(".load-all-customers").append(`
            <tr ${shadeRow}>
              <td>${sn}</td>
              <td>${val.name.toUpperCase()}</td>
              <td>${val.details.phone}</td>
              <td>${val.description}</td>
              <td>&#8358;${numeral(val.account.balance).format('0,0.00')}</td>
              <td>${val.date}</td>
              <td>
                <a href="{{url('admin/view-client')}}/${val.id}">
                  <i class="fa fa-file-text-o" aria-hidden="true"></i>
                </a>
              </td>
            </tr>
          `);

          // load customer account
          $("#user_id").append(`
            <option value="${val.id}">${val.name}</option>
          `);
        });
      });
    }

    // load customer profile
    function selectCustomerAccount() {
      var user_id = $("#user_id").val();

      $.get('{{url('admin/load/one/customer')}}/'+user_id, function(data) {
        // console log value
        console.log(data);
      });

      // show option
      showOption();
    }

    // apply new loan
    function applyNewLoan() {
      var token    = $("#token").val();
      var user_id  = $("#user_id").val();
      var amount   = $("#amount").val();
      var interest = $("#interest").val();
      var duration = $("#duration").val();
      var naration = $("#naration").val();

      var params = {
        _token: token,
        user_id: user_id,
        amount: amount,
        interest: interest,
        duration: duration,
        naration: naration
      };

      $.post('{{ url('admin/new/loan/account') }}', params, function(data, textStatus, xhr) {
        if(data.status == "success"){
          swal(
            data.status,
            data.message,
            data.status
          );
        }else{
          swal(
            "oops",
            data.message,
            data.status
          );
        }
      });

      // return 
      return false;
    }

    // format amount
    function formatAmount() {
      var amount = $("#amount").val();

      // preview
      $("#preview-amount").html(`
        &#8358;${numeral(amount).format('0,0.00')}
      `);
    }
  </script>
@endsection