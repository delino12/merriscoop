@extends('layouts.admin-skin')

{{-- title --}}
@section('title')
    {{ env("APP_NAME") }} | Audit Trails
@endsection

{{-- contents --}}
@section('contents')
  <div class="row">
    <div class="col-12">
      <div class="box">
        <div class="box-header with-border">
          <h4 class="box-title">
            <i class="fa fa-money"></i> Audit Trail
          </h4>
        </div>
        <div class="box-body">
          <div class="table-responsive">
            <table id="transactions" class="table table-hover no-wrap product-order" data-page-size="10">
              <thead>
                <tr>
                   <th>By</th>
                   <th>Action</th>
                   <th>Description</th>
                   <th>Last Updated</th>
                </tr>
              </thead>
              <tbody class="all-audit-trails">
                <tr>
                  <td>Loading...</td>
                </tr>
              </tbody>            
            </table>
          </div>
        </div>
      </div>           
    </div>
  </div>
@endsection

{{-- scripts section --}}
@section('scripts')
  <script type="text/javascript">
    // load modules
    loadAuditTrails();

    // load all transactions
    function loadAuditTrails() {
      $.get('{{ url('admin/load/all/trails') }}', function(data) {
        $(".all-audit-trails").html("");
        $.each(data, function(index, val) {
          // console log value
          // console.log(val);
          $(".all-audit-trails").append(`
            <tr>
              <td>${val.admin.name.toUpperCase()}</td>
              <td>${val.process}</td>
              <td>${val.description}</td>
              <td>${val.last_updated}</td>
            </tr>
          `);
        });

        // $('#transactions').DataTable();
        $('#transactions').DataTable({
          'paging'      : true,
          'lengthChange': false,
          'searching'   : true,
          'ordering'    : false,
          'info'        : true,
          'autoWidth'   : false
        });
      });
    }
  </script>
@endsection