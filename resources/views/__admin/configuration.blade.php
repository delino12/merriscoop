@extends('layouts.admin-skin')

{{-- title --}}
@section('title')
    {{ env("APP_NAME") }} | Settings
@endsection


{{-- contents --}}
@section('contents')
	@if(Auth::user()->level == 'alpha')
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<h4 class="box-title">
			        		<i class="fa fa-users"></i> List admin
			      		</h4>
			      		<a href="javascript:void(0);" onclick="showCreateAdminModal()" class="float-right">
		        			<i class="fa fa-plus"></i> New
		        		</a>
					</div>
			      	<div class="box-body">
			      		<table class="table">
			      			<thead>
			                    <tr>
			                      <th>S/N</th>
			                      <th>Name</th>
			                      <th>Email</th>
			                      <th>Level</th>
			                      <th>Option</th>
			                    </tr>
			                </thead>
			      			<tbody class="load-all-admins"></tbody>
			      		</table>
			      	</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="box">
					<div class="box-header with-border">
						<h4 class="box-title">
			        		<i class="fa fa-file"></i> Change Password
			      		</h4>
					</div>
			      	<div class="box-body">
		                <form id="add-user-form" method="post" onsubmit="return changePassword()">
		                  <div class="form-group">
		                    <label>Password</label>
		                    <input type="password" class="form-control" id="old-password" placeholder="********" required="">
		                  </div>

		                  <div class="form-group">
		                    <label>New Password</label>
		                    <span class="pull-right">
		                      <a href="javascript:void(0);" onclick="showHidePass()"><i class="fa fa-eye"></i></a>
		                    </span>
		                    <input type="password" class="form-control" id="new-password" placeholder="********" required="">
		                  </div>

		                  <div class="form-group">
		                    <button class="btn btn-primary col-md-12" id="change-password-btn">
		                      Change Password
		                    </button>
		                  </div>
		                </form>
					</div>
				</div>
			</div>
		</div>
	@else
		<div class="row">
			<div class="col-md-6">
				<div class="box">
					<div class="box-header with-border">
						<h4 class="box-title">
			        		<i class="fa fa-file"></i> Change Password
			      		</h4>
					</div>
			      	<div class="box-body">
		                <form id="add-user-form" method="post" onsubmit="return changePassword()">
		                  <div class="form-group">
		                    <label>Password</label>
		                    <input type="password" class="form-control" id="old-password" placeholder="********" required="">
		                  </div>

		                  <div class="form-group">
		                    <label>Confirm Password</label>
		                    <span class="pull-right">
		                      <a href="javascript:void(0);" onclick="showHidePass()"><i class="fa fa-eye"></i></a>
		                    </span>
		                    <input type="password" class="form-control" id="new-password" placeholder="********" required="">
		                  </div>

		                  <div class="form-group">
		                    <button class="btn btn-primary col-md-12" id="change-password-btn">
		                      Change Password
		                    </button>
		                  </div>
		                </form>
					</div>
				</div>
			</div>
		</div>
	@endif

	{{-- create admin modal --}}
	<!-- modal Area -->            
	<div class="modal center-modal fade" id="create-admin-modal" role="dialog" tabindex="-1">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			  	<div class="modal-header">
			  		<h4 class="modal-title">New Ajo Journal</h4>
			  		<button type="button" class="close" data-dismiss="modal">
			    		<span aria-hidden="true">&times;</span>
			  		</button>
			  	</div>
			  	<div class="modal-body">
				    <form id="add-user-form" method="post" onsubmit="return addNewUser()">
		                <div class="form-group">
		                    <label>Admin Level</label>
		                    <select class="form-control" id="level" onchange="displayPermission()">
		                      <option value="alpha">Alpha</option>
		                      <option value="beta">Beta</option>
		                      <option value="omega">Omega</option>
		                      <option value="support">Customer Support</option>
		                    </select>
		                </div>

		                <div class="form-group">
		                    <label>Names</label>
		                    <input type="text" class="form-control" id="names" placeholder="Eg: John Doe" required="">
		                </div>

		                <div class="form-group">
		                    <label>Email</label>
		                    <input type="email" class="form-control" id="email" placeholder="Eg: username@yeelda.com" required="">
		                </div>

		                <div class="form-group">
		                    <label>password</label>
		                    <span class="pull-right">
		                      <a href="javascript:void(0);" onclick="showHidePass()"><i class="fa fa-eye"></i></a>
		                    </span>
		                    <input type="password" class="form-control" id="password" placeholder="password" required="">
		                </div>

		                <div class="form-group">
		                    <button class="btn btn-primary col-md-12">
		                      Add Admin 
		                      <img src="/svg/loading.svg" id="loading" class="pull-right" width="40" height="20" style="display: none;">
		                    </button>
		                </div>
		            </form>
			  	</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
@endsection


{{-- scripts section --}}
@section('scripts')
	<script type="text/javascript">
		// load modules
		refreshAdminList();

		// show modal
		function showCreateAdminModal() {
			$("#create-admin-modal").modal({
		        backdrop: false
		    });
		}

		// add new admin user
	    function addNewUser(argument) {
	      var token    = '{{ csrf_token() }}';
	      var names    = $("#names").val();
	      var email    = $("#email").val();
	      var password = $("#password").val();
	      var level    = $("#level").val();

	      var data = {
	        _token:token,
	        names:names,
	        email:email,
	        level:level,
	        password:password
	      };

	      $.post('/admin/create/admin/user', data, function(data, textStatus, xhr) {
	        /*optional stuff to do after success */
	        // console.log(data);
	        if(data.status == 'success'){
	          $(".success_msg").append(`
	            <span class="text-success">${data.message}</span>
	          `);

	          // reset form data
	          $("#add-user-form")[0].reset();

	          swal(
	            data.status,
	            data.message,
	            'success'
	          );
	          $("#create-admin-modal").modal('hide');
	          // refresh list
	          refreshAdminList();
	        }else{
	          swal(
	            data.status,
	            data.message,
	            'error'
	          );
	        }
	      });

	      // void form
	      return false;
	    }

	    // show/hide password
	    function showHidePass() {
	      let passwordArea = document.getElementById("password");

	      if(passwordArea.type === 'password'){
	        passwordArea.type = 'text';
	      }else{
	        if(passwordArea.type === 'text'){
	          passwordArea.type = 'password'
	        }
	      }
	    }

	    // load admin list
	    function refreshAdminList() {
	      // body...
	      $.get('/admin/load/admin', function(data) {
	        $(".load-all-admins").html("");
	        var sn = 0;
	        $.each(data, function(index, val) {
	          sn++;
	          $('.load-all-admins').append(`
	            <tr>
	              <td>`+sn+`</td>
	              <td>`+val.names.toUpperCase()+`</td>
	              <td>`+val.email+`</td>
	              <td>`+val.level+`</td>
	              <td><a href="/admin/users/`+val.id+`/view">view</a></td>
	            </tr>
	          `);
	        });
	        $('#load-admin-table').DataTable();
	      });
	    }

	    // change password
	    function changePassword() {
	    	var token 			= $("#token").val();
	    	var old_password 	= $("#old-password").val();
	    	var new_password 	= $("#new-password").val();

	    	var params = {
	    		_token: token,
	    		old_password: old_password,
				new_password: new_password
	    	};

	    	$.post('{{ url('admin/change/password') }}', params, function(data, textStatus, xhr) {
	    		if(data.status == "success"){
	    			swal(
	    				"success",
	    				data.message,
	    				data.status
	    			);
	    			window.location.reload();
	    		}else{
	    			swal(
	    				"oops",
	    				data.message,
	    				data.status
	    			);
	    		}
	    	});

	    	// void form
	    	return false;
	    }
	</script>
@endsection