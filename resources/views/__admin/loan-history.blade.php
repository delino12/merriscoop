@extends('layouts.admin-skin')

{{-- title --}}
@section('title')
  {{ env("APP_NAME") }} | Loan History
@endsection

{{-- contents --}}
@section('contents')
  <div class="row">
    <div class="col-lg-6 col-md-6">
      <div class="box">
        <div class="box-header with-border">
          <h4 class="box-title text-info"><i class="ti-book mr-15"></i> Loan Details</h4>      
          <ul class="box-controls pull-right">
            <li><a class="box-btn-close" href="javascript:void(0);"></a></li>
            <li><a class="box-btn-slide" href="javascript:void(0);"></a></li> 
            <li><a class="box-btn-fullscreen" href="javascript:void(0);"></a></li>
          </ul>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div id="image-preview" class="mb-4">
            <img src="{{asset('images/user-info.png')}}" style="border:5px;" width="80px" height="auto">
          </div>

          <h4 class="box-title text-info"><i class="ti-user mr-15"></i> 
            <span id="full-names"></span>
          </h4>
          <hr class="my-15">
          
          <table class="table">
            <thead>
              <tr>
                <th>Details</th>
                <th>Amount(&#8358;)</th>
              </tr>
              <tbody class="loan-details"></tbody>
            </thead>
          </table>
          <hr class="my-15">

          <form method="POST" id="pay-form" onsubmit="return makePayment()" style="display: none;">

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <input type="hidden" id="user_id" value="">
                  <input type="hidden" id="loan_id" value="{{$id}}">
                  <input type="number" min="1" id="amount" onkeyup="formatAmount()" class="form-control" placeholder="Enter amount" required="">
                </div>
              </div>
              <div class="col-md-6">
                <span class="form-control" id="preview-amount"></span>
              </div>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-info btn-outline mr-1">
                <i class="ti-wallet"></i> Pay
              </button>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <button type="button" id="show-form" class="btn btn-info btn-outline mr-1">
            <i class="ti-save-alt"></i> Make Payment
          </button>
        </div>  
      </div>
      <!-- /.box -->      
    </div> 
    <div class="col-lg-6 col-md-6">
      <div class="box small">
        <div class="box-header with-border">
          <h4 class="box-title text-info"><i class="ti-book mr-15"></i> Payment History</h4>   
          <ul class="box-controls pull-right">
            <li><a class="box-btn-close" href="javascript:void(0);"></a></li>
            <li><a class="box-btn-slide" href="javascript:void(0);"></a></li> 
            <li><a class="box-btn-fullscreen" href="javascript:void(0);"></a></li>
          </ul>
        </div>
        <!-- /.box-header -->
        <div class="box-body p-2">
          <table class="table table-hover no-wrap product-order" id="loan-table" data-page-size="10">
            <thead>
              <tr>
                 <th>Ref ID</th>
                 <th>Amount (&#8358;)</th>
                 <th>Date</th>
                 <th>Option</th>
              </tr>
            </thead>
            <tbody class="load-payment-history">
              <tr>
                <td>Loading...</td>
              </tr>
            </tbody>            
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>           
  </div>
@endsection

{{-- scripts section --}}
@section('scripts')
  <script type="text/javascript">
    // $(document).ready(function (){
    loadLoanHistory();
    // });

    // load loan history
    function loadLoanHistory() {
      // console.log(loan_id);
      $.get('{{ url('admin/loan/history')}}/{{$id}}', function(data) {
        // console log value
        // load loan information
        loadOneCustomer(data.loan.user_id);
        $('#loan_id').val(data.loan.id);
        $('#user_id').val(data.loan.user_id);
        $('#account_balance').val(numeral(data.loan.balance).format('0,0.00'));
        $('#amount_payable').val(numeral(data.loan.total).format('0,0.00'));

        $(".loan-details").html(`
          <tr>
            <td>Loan Amount</td>
            <td>&#8358;${numeral(data.loan.balance).format('0,0.00')}</td>
          </tr>
          <tr>
            <td>Loan Interest</td>
            <td>&#8358;${numeral(data.loan.interest).format('0,0.00')}</td>
          </tr>
          <tr>
            <td>Loan Balance</td>
            <td>&#8358;${numeral(data.loan.total).format('0,0.00')}</td>
          </tr>

          <tr>
            <td>Loan Issued Date</td>
            <td>${moment(data.loan.created_at).format('MMMM Do YYYY, h:mm:ss a')}</td>
          </tr>
          <tr>
            <td>Loan Due Date</td>
            <td>${moment(data.loan.duration).format('MMMM Do YYYY, h:mm:ss a')}</td>
          </tr>
        `);


        $(".load-payment-history").html("");
        $.each(data.transactions, function(index, val) {
          // console log value
          // console.log(val);
          $(".load-payment-history").append(`
            <tr>
              <td>${val.ref_no}</td>
              <td>&#8358;${numeral(val.amount).format('0,0.00')}</td>
              <td>${val.created_at}</td>
              <td>
                <a href="{{url('admin/view-payment')}}/${val.id}">
                  <i class="fa fa-file-text-o" aria-hidden="true"></i> View
                </a>
              </td>
            </tr>
          `);
        });

        $('#loan-table').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
          });
      });
    }


    // load all customers
    function loadOneCustomer(user_id) {
      // console.log(user_id);
      $.get('{{url('admin/load/one/customer')}}/'+user_id, function(data) {
        // console.log(data);
        // console log value
        $('#full-names').html(data.name.toUpperCase());
        $('#firstname').val(data.details.firstname.toUpperCase());
        $('#lastname').val(data.details.lastname.toUpperCase());
        $('#email').val(data.email);
        $('#phone').val(data.details.phone);
        $('#occupation').val(data.details.occupation);
        $('#account_type').val(data.description);
        $('#passport').val(data.details.avatar);
        $('#address').val(data.details.address);
      });
    }

    // show form
    $("#show-form").click(function (c){
      c.preventDefault();
      $("#pay-form").toggle();
    });

    // make payment 
    function makePayment() {
      var token   = $("#token").val();
      var amount  = $("#amount").val();
      var user_id = $("#user_id").val();
      var loan_id = $("#loan_id").val();

      var params = {
        _token: token,
        amount: amount,
        user_id: user_id,
        loan_id: loan_id
      };

      // post payment
      $.post('{{ url('admin/init/payment') }}', params, function(data) {
        $('#loan-table').DataTable().destroy();
        loadLoanHistory();

        // clear sms queue
        clearSmsQueue();

        if(data.status == "success"){
          $("#preview-amount").html("");
          $("#amount").val("");
          // prompt
          swal(
            "success",
            data.message,
            data.status
          );

          // clear sms queue
          clearSmsQueue();

        }else{
          swal(
            'oops',
            data.message,
            data.status
          );
        }
      });

      // return
      return false;
    }

    // format amount
    function formatAmount() {
      var amount = $("#amount").val();

      // preview
      $("#preview-amount").html(`
        &#8358;${numeral(amount).format('0,0.00')}
      `);
    }
  </script>
@endsection