@extends('layouts.admin-skin')

{{-- title --}}
@section('title')
    {{ env("APP_NAME") }} | Transactions
@endsection


{{-- contents --}}
@section('contents')
  <div class="row">
    <div class="col-12">
      <div class="box">
        <div class="box-header with-border">
          <h4 class="box-title">
            <i class="fa fa-money"></i> Transactions
          </h4>
        </div>
        <div class="box-body p-2">
          <div class="table-responsive">
            <table id="transactions" class="table table-hover no-wrap product-order" data-page-size="10">
              <thead>
                <tr>
                   <th>Customer</th>
                   <th>Ref ID</th>
                   <th>Type</th>
                   <th>Amount (&#8358;)</th>
                   <th>Date</th>
                   <th>History</th>
                </tr>
              </thead>
              <tbody class="all-transactions">
                <tr>
                  <td>Loading...</td>
                </tr>
              </tbody>            
            </table>
          </div>
        </div>
      </div>           
    </div>
  </div>
@endsection


{{-- scripts section --}}
@section('scripts')
  <script type="text/javascript">
    // $(document).ready(function(d){
      loadAllTransactions();
    // });

    // load all transactions
    function loadAllTransactions() {
      $.get('{{ url('admin/load/all/transactions') }}', function(data) {
        $(".all-transactions").html("");
        $.each(data, function(index, val) {
          // console log value
          // console.log(val);
          var trackBtn;
          var shadeRow;
          if(val.account_type == "savings"){
            shadeRow = `class="text-info"`;
            trackBtn = `
              <a href="{{url('admin/view-savings')}}/${val.account.id}">
                <i class="fa fa-file-text-o" aria-hidden="true"></i> Track
              </a>
            `;
          }else{
            shadeRow = `class="text-warning"`;
            trackBtn = `
              <a href="{{url('admin/view-loan')}}/${val.loan.id}">
                <i class="fa fa-file-text-o" aria-hidden="true"></i> Track
              </a>
            `;
          }

          $(".all-transactions").append(`
            <tr ${shadeRow}>
              <td>${val.details.firstname.toUpperCase()} ${val.details.lastname.toUpperCase()}</td>
              <td>${val.transaction.ref_no}</td>
              <td>${val.transaction.name.toUpperCase()}</td>
              <td>${numeral(val.transaction.amount).format('0,0.00')}</td>
              <td>${val.transaction.created_at}</td>
              <td>
                ${trackBtn}
              </td>
            </tr>
          `);
        });

        // $('#transactions').DataTable();
        $('#transactions').DataTable({
          'paging'      : true,
          'lengthChange': false,
          'searching'   : true,
          'ordering'    : false,
          'info'        : true,
          'autoWidth'   : false
        });
  
      });
    }
  </script>
@endsection