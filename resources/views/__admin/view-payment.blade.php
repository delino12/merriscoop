@extends('layouts.admin-skin')

{{-- title --}}
@section('title')
    {{ env("APP_NAME") }} | Loan Payment
@endsection

{{-- contents --}}
@section('contents') 
  <!-- Main content -->
  <section class="invoice printableArea">
    <!-- title row -->
    <div class="row">
      <div class="col-12">
    <div class="page-header">
    <h2 class="d-inline"><span class="font-size-30">Reciept</span></h2>
    <div class="pull-right text-right">
      <h3 class="payment_date"></h3>
    </div>  
    </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-md-6 invoice-col">
        <strong>From</strong> 
    <address>
    <strong class="text-blue font-size-24">{{ env("APP_NAME") }} Admin</strong><br>
    <strong class="d-inline">124/140, TAIWO VILLA ITIRE BUS - STOP,OJO ROAD, AJEGUNLE APAPA,LAGOS</strong><br>
    <strong>
      <b>Phone:</b> 08180283377 &nbsp;&nbsp;&nbsp;&nbsp; <b>Email:</b> info@merriscoop.com
    </strong>  
        </address>
      </div>
      <!-- /.col -->
      <div class="col-md-6 invoice-col text-right">
        <strong>To</strong>
        <address>
          <strong class="text-blue font-size-24">
            <span class="client_name"></span>
          </strong>
            <br>
              <span class="client_address"></span>
            <br>
          <strong>Phone: <span class="client_phone"></span> &nbsp;&nbsp;&nbsp;&nbsp; Email: <span class="client_email"></span></strong>
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-12 invoice-col mb-15">
    <div class="invoice-details row no-margin">
      <div class="col-md-6"><b>Transaction ID:</b> <span class="text-green trans_ref"></span></div>
      <div class="col-md-6"><b>Payment Due:</b> <span class="text-green loan_duration"></span></div>
    </div>
    </div>
    <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-12 table-responsive">
        <table class="table table-bordered">
          <tbody>
          <tr>
            <th>#</th>
            <th>Description</th>
            <th class="text-right">Transaction Type</th>
            <th class="text-right">Amount Paid</th>
            <th class="text-right">Loan Balance</th>
          </tr>
          <tr>
            <td>1</td>
            <td><span class="description"></span></td>
            <td class="text-right"><span class="type"></span></td>
            <td class="text-right"><span class="amount"></span></td>
            <td class="text-right"><span class="total"></span></td>
          </tr>
          </tbody>
        </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
      <div class="col-12 text-right">
        <p class="lead">
          <b>Payment Due</b><span class="text-danger">  </span>
        </p>
        <div>
          <p>Total amount  :  <span class="amount"></span></p>
          <p>Tax (0.00%)  :  0.00</p>
        </div>
        <div class="total-payment">
          <h3><b>Total :</b> <span class="amount"></span></h3>
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- this row will not appear when printing -->
    <div class="row no-print">
      <div class="col-12">
        <button id="print" class="btn btn-warning" type="button"> <span><i class="fa fa-print"></i> Print</span> </button>
      </div>
    </div>
  </section>
@endsection

{{-- scripts section --}}
@section('scripts')
  <script type="text/javascript">
    $(document).ready(function (){
      loadPaymentReciept();
    });

    // load payment reciept
    function loadPaymentReciept(argument) {
      // body...
      $.get('{{ url('admin/one/transaction')}}/{{$id}}', function(data) {
        
        $(".client_name").html(`${data.details.firstname.toUpperCase()} ${data.details.lastname.toUpperCase()}`);
        $(".client_address").html(data.details.address);
        $(".client_email").html(data.details.email);
        $(".client_phone").html(data.details.phone);
        $(".trans_ref").html(data.transaction.ref_no);
        $(".loan_duration").html(moment(data.loan.duration).format('MMMM Do YYYY, h:mm:ss a'));
        $(".payment_date").html(moment(data.transaction.created_at).format('MMMM Do YYYY'));


        $(".description").html(data.transaction.name.toUpperCase());
        $(".type").html(data.transaction.type);
        $(".amount").html('&#8358;'+numeral(data.transaction.amount).format("0,0.00"));
        $(".total").html('&#8358;'+numeral(data.loan.total).format("0,0.00"));

        // console log value
        console.log(data);
      });
    }

    // print receipt
    $("#print").click(function(c){
      c.preventDefault();

      window.print();
    });
  </script>
@endsection