@extends('layouts.admin-skin')

{{-- title --}}
@section('title')
    {{ env("APP_NAME") }} | Ajo General
@endsection


{{-- contents --}}
@section('contents')
  <div class="row">
    <div class="col-12">
      <div class="box">
        <div class="box-header with-border">
          <h4 class="box-title">
            <i class="fa fa-users"></i> {{ $ajobook->name }}
          </h4>
          <span class="pull-right">
            <i class="fa fa-plus"></i> 
            <a href="javascript:void(0);" onclick="addMemberModal()">Add Member</a>
          </span>
        </div>
        <div class="box-body">
          <div class="table-responsive">
            <table id="transactions" class="table table-hover no-wrap product-order" data-page-size="10">
              <thead>
                <tr>
                   <th>S/N</th>
                   <th>Member Name</th>
                   <th>Turn collected</th>
                   <th>Unit Amount (&#8358;)</th>
                   <th>Status</th>
                   <th>Option</th>
                </tr>
              </thead>
              <tbody class="all-ajo-general">
                <tr>
                  <td>Loading...</td>
                </tr>
              </tbody>            
            </table>
          </div>
        </div>
      </div>           
    </div>
  </div>

  <!-- modal Area -->              
  <div class="modal center-modal fade" id="add-member-modal" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <h4 class="modal-title">New Member</h4>
      <button type="button" class="close" data-dismiss="modal">
        <span aria-hidden="true">&times;</span>
      </button>
      </div>
      <div class="modal-body">
        <form method="post" onsubmit="return addAjoMember()">
          <div class="from-group">
            <label>Select Member</label>
            <select class="form-control" id="ajo_member"></select>
          </div>

          <div class="from-group">
            <input type="submit" class="form-control" value="Add Member">
          </div>
        </form>
      </div>

    </div>
    <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
@endsection


{{-- scripts section --}}
@section('scripts')
  <script type="text/javascript">
    // load modules
    getAllMemberSchedule();
    getAllUsers();

    // add book modal
    function addMemberModal() {
      $("#add-member-modal").modal({
        backdrop: false
      });
    }

    // create ajo book
    function addAjoMember() {
      var token       = $("#token").val();
      var ajo_member  = $("#ajo_member").val();
      var book_id     = '{{ $book_id }}';

      var params = {
        _token: token,
        user_id: ajo_member,
        ajobook_id: book_id,
        status: '{{ $ajobook->status }}',
        amount: '{{ $ajobook->amount }}',
        charge: 0,
        next_payout_date: 'none',
        next_payout_user: 'none',
        duration: '{{ $ajobook->quota }}'
      };

      $.post('{{ url('admin/add/members') }}', params, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
        if(data.status == "success"){
          swal(
            "success",
            data.message,
            data.status
          );
          $("#add-member-modal").modal('hide');
          getAllMemberSchedule();
        }else{
          swal(
            "oops",
            data.message,
            data.status
          );
        }
      });

      return false;
    }

    // get all book
    function getAllMemberSchedule() {
      var params = {
        book_id: '{{ $book_id }}'
      };

      $.get('{{ url('admin/get-ajo/members') }}', params, function(data) {
        $(".all-ajo-general").html("");
        var sn = 0;
        $.each(data, function(index, val) {
          sn++;
          $(".all-ajo-general").append(`
            <tr>
              <td>${sn}</td>
              <td>${val.details.name}</td>
              <td>${val.turn_collected}</td>
              <td>${val.amount}</td>
              <td>${val.status}</td>
              <td>
                <a href="{{ url('admin/pay/member') }}/${val.id}">Payment Due</a>
              </td>
            </tr>
          `);
        });
      });
    }

    // get all users
    function getAllUsers() {
      $.get('{{ url('admin/fetch/user') }}', function(data) {
        $("#ajo_member").html("");
        $.each(data, function(index, val) {
          $("#ajo_member").append(`
            <option value="${val.id}"> ${val.name} </option>
          `);
        });

        // $("#ajo_member").select2();
      });
    }
  </script>
@endsection