@extends('layouts.admin-skin')

{{-- title --}}
@section('title')
    {{ env("APP_NAME") }} | Reports
@endsection


{{-- contents --}}
@section('contents')
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h4 class="box-title">
            <i class="fa fa-money"></i> Loans Reports
          </h4>
        </div>
        <div class="box-body">
          <form method="post" onsubmit="return fetchLoansReports()">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>From</label>
                  <input type="date" class="form-control" id="loan_start_date">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>To</label>
                  <input type="date" class="form-control" id="loan_end_date">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>You can also export report to excel or pdf</label>
                  <input type="submit" class="form-control" value="Generate Loan Reports" name="">
                </div>
              </div>
            </div>
          </form>

          <div class="table-responsive">
            <table id="l-report-table" class="table table-hover no-wrap product-order" data-page-size="10">
              <thead>
                <tr>
                   <th>Customer</th>
                   <th>Account Type</th>
                   <th>Balance (&#8358;)</th>
                   <th>Total (&#8358;)</th>
                   <th>Status</th>
                   <th>Date</th>
                   <th>Option</th>
                </tr>
              </thead>
              <tbody class="load-loans-reports"></tbody>            
            </table>
          </div>
        </div>
      </div>         
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h4 class="box-title">
            <i class="fa fa-money"></i> Investment & Savings Reports
          </h4>
        </div>
        <div class="box-body">
          <form method="post" onsubmit="return fetchInvestmentReports()">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>From</label>
                  <input type="date" class="form-control" id="investment_start_date">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>To</label>
                  <input type="date" class="form-control" id="investment_end_date">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>You can also export report to excel or pdf</label>
                  <input type="submit" class="form-control" value="Generate Investment Reports" name="">
                </div>
              </div>
            </div>
          </form>

          <div class="table-responsive">
            <table id="s-report-table" class="table table-hover no-wrap product-order" data-page-size="10">
              <thead>
                <tr>
                   <th>Customer</th>
                   <th>Account Type</th>
                   <th>Balance (&#8358;)</th>
                   <th>Total (&#8358;)</th>
                   <th>Status</th>
                   <th>Date</th>
                   <th>Option</th>
                </tr>
              </thead>
              <tbody class="load-savings-reports"></tbody>            
            </table>
          </div>
        </div>
      </div>           
    </div>

    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h4 class="box-title">
            <i class="fa fa-money"></i> Transactions
          </h4>
        </div>
        <div class="box-body">
          <div class="table-responsive">
            <table id="transactions" class="table table-hover no-wrap product-order" data-page-size="10">
              <thead>
                <tr>
                   <th>Customer</th>
                   <th>Ref ID</th>
                   <th>Type</th>
                   <th>Amount (&#8358;)</th>
                   <th>Date</th>
                   <th>History</th>
                </tr>
              </thead>
              <tbody class="all-transactions"></tbody>            
            </table>
          </div>
        </div>
      </div>           
    </div>
  </div>
@endsection


{{-- scripts section --}}
@section('scripts')
  <script type="text/javascript">
    // load all ajo set
    getAllTransactions();
    loadAllTransactions();

    // fetch investment reports for savings
    function fetchInvestmentReports() {
      var token       = $("#token").val();
      var start_date  = $("#investment_start_date").val();
      var end_date    = $("#investment_end_date").val();
      var report_type = "savings";

      var params = {
        _token: token,
        start_date: start_date,
        end_date: end_date,
        report_type: report_type
      };

      $.post('{{ url('admin/generate/reports') }}', params, function(data) {
        // console log value
        // console.log(data);
        $(".load-savings-reports").html("");
        $.each(data, function(index, val) {
          $(".load-savings-reports").append(`
            <td>${val.details.name.toUpperCase()}</td>
            <td>${val.details.account_type}</td>
            <td>${numeral(val.balance).format('0,0.00')}</td>
            <td>${numeral(val.total).format('0,0.00')}</td>
            <td>${val.status}</td>
            <td>${val.date}</td>
            <td>
              <a href="{{url('admin/view-savings')}}/${val.id}">
                <i class="fa fa-file-text-o" aria-hidden="true"></i> Track
              </a>
            </td>
          `);
        });

        $("#s-report-table").DataTable({
          'paging'      : true,
          'lengthChange': false,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : false,
          'dom'         : 'Bfrtip',
          'buttons'     : [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
          ]
        });
      });

      return false;
    }

    // fetch loans reports
    function fetchLoansReports() {
      var token       = $("#token").val();
      var start_date  = $("#loan_start_date").val();
      var end_date    = $("#loan_end_date").val();
      var report_type = "loans";

      var params = {
        _token: token,
        start_date: start_date,
        end_date: end_date,
        report_type: report_type
      };

      $.post('{{ url('admin/generate/reports') }}', params, function(data) {
        $(".load-loans-reports").html("");
        $.each(data, function(index, val) {
          $(".load-loans-reports").append(`
            <tr>
              <td>${val.details.name.toUpperCase()}</td>
              <td>${val.details.account_type}</td>
              <td>${numeral(val.balance).format('0,0.00')}</td>
              <td>${numeral(val.total).format('0,0.00')}</td>
              <td>${val.status}</td>
              <td>${val.date}</td>
              <td>
                <a href="{{url('admin/view-savings')}}/${val.id}">
                  <i class="fa fa-file-text-o" aria-hidden="true"></i> Track
                </a>
              </td>
            </tr>
          `);
        });

        $("#l-report-table").DataTable({
          'paging'      : true,
          'lengthChange': false,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : false,
          'dom'         : 'Bfrtip',
          'buttons'     : [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
          ]
        });
      });
      return false;
    }

    // load all transactions
    function getAllTransactions() {
      $.get('{{ url('admin/load/all/ajo-pro') }}', function(data) {
        $(".all-transactions").html("");
        $.each(data, function(index, val) {
          // console log value
          // console.log(val);
          $(".all-transactions").append(`
            <tr>
              <td>${val.details.firstname.toUpperCase()} ${val.details.lastname.toUpperCase()}</td>
              <td>${val.transaction.ref_no}</td>
              <td>${val.transaction.name.toUpperCase()}</td>
              <td>${numeral(val.transaction.amount).format('0,0.00')}</td>
              <td>${val.transaction.created_at}</td>
              <td class="text-capitalize">${val.transaction.status}</td>
              <td>
                <a href="{{url('admin/view-payment')}}/${val.loan.id}">
                  <i class="fa fa-file-text-o" aria-hidden="true"></i> View
                </a>
              </td>
              <td>
                <a href="{{url('admin/view-loan')}}/${val.loan.id}">
                  <i class="fa fa-file-text-o" aria-hidden="true"></i> Track
                </a>
              </td>
            </tr>
          `);
        });

        // $('#transactions').DataTable();
        $('#transactions').DataTable({
          'paging'      : true,
          'lengthChange': false,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : false
        });
      });
    }

    // load all transactions
    function loadAllTransactions() {
      $.get('{{ url('admin/load/all/transactions') }}', function(data) {
        $(".all-transactions").html("");
        $.each(data, function(index, val) {
          // console log value
          // console.log(val);
          var trackBtn;
          var shadeRow;
          if(val.account_type == "savings"){
            shadeRow = `class="text-info"`;
            trackBtn = `
              <a href="{{url('admin/view-savings')}}/${val.account.id}">
                <i class="fa fa-file-text-o" aria-hidden="true"></i> Track
              </a>
            `;
          }else{
            shadeRow = `class="text-warning"`;
            trackBtn = `
              <a href="{{url('admin/view-loan')}}/${val.loan.id}">
                <i class="fa fa-file-text-o" aria-hidden="true"></i> Track
              </a>
            `;
          }

          $(".all-transactions").append(`
            <tr ${shadeRow}>
              <td>${val.details.firstname.toUpperCase()} ${val.details.lastname.toUpperCase()}</td>
              <td>${val.transaction.ref_no}</td>
              <td>${val.transaction.name.toUpperCase()}</td>
              <td>${numeral(val.transaction.amount).format('0,0.00')}</td>
              <td>${val.transaction.created_at}</td>
              <td>
                ${trackBtn}
              </td>
            </tr>
          `);
        });

        // $('#transactions').DataTable();
        $('#transactions').DataTable({
          'paging'      : true,
          'lengthChange': false,
          'searching'   : true,
          'ordering'    : false,
          'info'        : true,
          'autoWidth'   : false,
          'dom'         : 'Bfrtip',
          'buttons'     : [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
          ]
        });
      });
    }
  </script>
@endsection