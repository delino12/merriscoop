@extends('layouts.admin-skin')

{{-- title --}}
@section('title')
  {{ env("APP_NAME") }} | New Application Form
@endsection

{{-- contents --}}
@section('contents')
  <div class="row">
    <div class="col-lg-6 col-12">
      <div class="box">
        <div class="box-header with-border">
          <h4 class="box-title" id="full-names"></h4>      
          <ul class="box-controls pull-right">
            <li><a class="box-btn-close" href="javascript:void(0);"></a></li>
            <li><a class="box-btn-slide" href="javascript:void(0);"></a></li> 
            <li><a class="box-btn-fullscreen" href="javascript:void(0);"></a></li>
          </ul>
        </div>
        <!-- /.box-header -->
        <form class="form" method="post" onsubmit="return false">
          <div class="box-body">
            <div id="image-preview" class="mb-4">
              <img src="{{asset('images/user-info.png')}}" style="border:5px;" width="80px" height="auto">
            </div>

            <h4 class="box-title text-info"><i class="ti-user mr-15"></i> Personal Info</h4>
            <hr class="my-15">
            <div class="row">
              <div class="col-md-6">
              <div class="form-group">
                <label>First Name</label>
                <input type="text" id="firstname" class="form-control" placeholder="First Name">
              </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                <label>Last Name</label>
                <input type="text" id="lastname" class="form-control" placeholder="Last Name">
              </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
              <div class="form-group">
                <label >E-mail</label>
                <input type="text" id="email" class="form-control" placeholder="E-mail" readonly="">
              </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                <label >Contact Number</label>
                <input type="text" id="phone" class="form-control" placeholder="Phone">
              </div>
              </div>
            </div>
            <h4 class="box-title text-info"><i class="ti-save mr-15"></i> About</h4>
            <hr class="my-15">
            <div class="form-group">
              <label>Occupation</label>
              <input type="text" id="occupation" class="form-control" placeholder="Job Description" readonly="">
            </div>
            <div class="row">
              <div class="col-md-6">
              <div class="form-group">
                <label>Account Type</label>
                <input type="text" id="account_type" class="form-control" placeholder="Account Type" name="" readonly="">
              </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Account Balance (&#8358;)</label>
                  <input type="text" id="account_balance" class="form-control" placeholder="Account Balance" name="" readonly="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Amount Payable (&#8358;)</label>
                  <input type="text" id="amount_payable" class="form-control" placeholder="Amount Payable" name="" readonly="">
                </div>
              </div>
            </div>
            <div class="form-group">
              <label>ID Card</label>
              <div id="passport"></div>
            </div>
            <div class="form-group">
              <label>Address</label>
              <textarea rows="5" class="form-control" id="address" placeholder="About Project"></textarea>
            </div>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <button type="button" id="update-profile-btn" class="btn btn-warning btn-outline mr-1">
              <i class="ti-save-alt"></i> Update
            </button>
          </div>  
        </form>
      </div>
      <!-- /.box -->      
    </div> 
    <div class="col-lg-6 col-12">
      <div class="box">
        <div class="box-header with-border">
          <h4 class="box-title" id="full-names"></h4>      
          <ul class="box-controls pull-right">
            <li><a class="box-btn-close" href="javascript:void(0);"></a></li>
            <li><a class="box-btn-slide" href="javascript:void(0);"></a></li> 
            <li><a class="box-btn-fullscreen" href="javascript:void(0);"></a></li>
          </ul>
        </div>
        <!-- /.box-header -->
        <form class="form">
          <div class="box-body">
            <h4 class="box-title text-info"><i class="ti-user mr-15"></i> Background Info</h4>
            <hr class="my-15">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Country</label>
                  <input type="text" id="country" class="form-control" placeholder="Country" value="Nigeria" readonly="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>State</label>
                  <input type="text" id="state" class="form-control" placeholder="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Local Govt</label>
                  <input type="text" id="lga" class="form-control" placeholder="Status">
                </div>
              </div>
            </div>
            <h4 class="box-title text-info"><i class="ti-user mr-15"></i> Bank Verification</h4>
            <hr class="my-15">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>BVN (Verification)</label>
                  <input type="text" id="bvn" class="form-control" placeholder="BVN No.">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Status</label>
                  <input type="text" id="bvn-status" class="form-control" placeholder="Status">
                </div>
              </div>
            </div>
            <h4 class="box-title text-info"><i class="ti-save mr-15"></i> Next of Kind</h4>
            <hr class="my-15">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Next of kind firstname</label>
                  <input type="text" id="nok_firstname" class="form-control" placeholder="Firstname">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Next of kind lastname</label>
                  <input type="text" id="nok_lastname" class="form-control" placeholder="Lastname">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
              <div class="form-group">
                <label >Next of Kin phone</label>
                <input type="text" id="nok_phone" class="form-control" placeholder="Contact no.">
              </div>
              </div>
            </div>
            <div class="form-group">
              <label>Next of Kin Address</label>
              <textarea rows="2" class="form-control" id="nok_address" placeholder="Next of kin address Project"></textarea>
            </div>
            <h4 class="box-title text-info"><i class="ti-save mr-15"></i> Guarantor</h4>
            <hr class="my-15">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Guarantor firstname</label>
                  <input type="text" id="gad_firstname" class="form-control" placeholder="Firstname">
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label>Guarantor lastname</label>
                  <input type="text" id="gad_lastname" class="form-control" placeholder="Lastname">
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Guarantor Occupation</label>
                  <input type="text" id="gad_occupation" class="form-control" placeholder="Job Description">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label >Guarantor contact</label>
                  <input type="text" id="gad_phone" class="form-control" placeholder="Contact no" maxlength="11">
                </div>
              </div>
            </div>
            <div class="form-group">
              <label>Guarantor Address</label>
              <textarea rows="2" class="form-control" id="gad_address" placeholder="Guarantor address"></textarea>
            </div>
                  
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <button type="button" class="btn btn-warning btn-outline mr-1">
              <i class="ti-save-alt"></i> Update
            </button>
          </div>  
        </form>
      </div>
      <!-- /.box -->
    </div>           
  </div>
@endsection

{{-- scripts section --}}
@section('scripts')
  <script type="text/javascript">
    // $(document).ready(function (){
    loadOneCustomer();
    // });

    // load all customers
    function loadOneCustomer() {
      $.get('{{url('admin/load/one/customer')}}/{{$id}}', function(data) {
        // console log value
        $('#full-names').html(data.name.toUpperCase());
        $('#firstname').val(data.details.firstname.toUpperCase());
        $('#lastname').val(data.details.lastname.toUpperCase());
        $('#email').val(data.email);
        $('#phone').val(data.details.phone);
        $('#occupation').val(data.details.occupation);
        $('#account_type').val(data.description);
        $('#account_balance').val(numeral(data.account.balance).format('0,0.00'));
        $('#passport').html(`
          <img src="${data.details.passport_id}" width="350" height="250" />
        `);
        $('#address').val(data.details.address);
        $('#amount_payable').val(numeral(data.account.total).format('0,0.00'));
        // console.log(data);
      });
    }

    // update profile btn
    $("#update-profile-btn").click(function(event) {
      var token       = $("#token").val();
      var firstname   = $("#firstname").val();
      var lastname    = $("#lastname").val();
      var email       = $("#email").val();
      var phone       = $("#phone").val();
      var occupation  = $("#occupation").val();
      var address     = $("#address").val();
      var user_id     = '{{ $id }}';

      var params = {
        _token: token,
        firstname: firstname,
        lastname: lastname,
        email: email,
        phone: phone,
        occupation: occupation,
        address: address,
        user_id: user_id
      };

      // update profile
      $.post('{{ url('admin/update/profile') }}', params, function(data, textStatus, xhr) {
        if(data.status == "success"){
          loadOneCustomer();
          swal(
            "success",
            data.message,
            data.status
          );
        }else{
          swal(
            "oops",
            data.message,
            data.status
          );
        }
      });
    });
  </script>
@endsection