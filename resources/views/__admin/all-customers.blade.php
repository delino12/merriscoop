@extends('layouts.admin-skin')

{{-- title --}}
@section('title')
  {{ env("APP_NAME") }} | Search for customers
@endsection

{{-- contents --}}
@section('contents')
  <div class="row">
    <div class="col-12 col-lg-12">
        <div class="box">
              <div class="box-header with-border">
                <h4 class="box-title">
                  <i class="fa fa-users"></i> Customers Account
                </h4>
              </div>
          <div class="box-body p-2">
            <div class="table-responsive">
              <table id="all-customers" class="table table-hover no-wrap" data-page-size="10">
                  <thead>
                      <tr>
                          <th>#</th>
                          <th>Name</th>
                          <th>Phone</th>
                          <th>Account Type</th>
                          <th>Balance</th>
                          <th>Last updated</th>
                          <th>View</th>
                      </tr>
                  </thead>
                  <tbody class="load-all-customers">
                      <tr>
                          <td>Loading...</td>
                      </tr>
                  </tbody>
              </table>
          </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /. box -->
    </div>             
  </div>
@endsection

{{-- scripts section --}}
@section('scripts')
  <script type="text/javascript">
    // load modules
    loadAllCustomers();

    // load all customers
    function loadAllCustomers() {
      $.get('{{ url('admin/load/all/customers') }}', function(data) {
        var sn = 0;
        $(".load-all-customers").html("");
        $.each(data, function(index, val) {
          sn++;

          var shadeRow;
          if(val.type == "savings"){
            shadeRow = `class="text-green"`; 
          }else{
            shadeRow = `class="text-white"`; 
          }

          // console log value
          $(".load-all-customers").append(`
            <tr ${shadeRow}>
              <td>${sn}</td>
              <td>${val.name.toUpperCase()}</td>
              <td>${val.details.phone}</td>
              <td>${val.description}</td>
              <td>&#8358;${numeral(val.account.balance).format('0,0.00')}</td>
              <td>${val.date}</td>
              <td>
                <a href="{{url('admin/view-client')}}/${val.id}">
                  <i class="fa fa-file-text-o" aria-hidden="true"></i>
                  view
                </a>
              </td>
            </tr>
          `);
        });

        // $('#transactions').DataTable();
        $('#all-customers').DataTable({
          'paging'      : true,
          'lengthChange': false,
          'searching'   : true,
          'ordering'    : false,
          'info'        : true,
          'autoWidth'   : false
        });
      });
    }

    // format amount
    function formatAmount() {
      var amount = $("#amount").val();

      // preview
      $("#preview-amount").html(`
        &#8358;${numeral(amount).format('0,0.00')}
      `);
    }

    // get type hints
    function getTypeHints() {
      $.get('{{ url('admin/occupation/category') }}', function(data) {
        $("#occupation").typeahead({
          source: data
        });
      });
    }
  </script>
@endsection