@extends('layouts.admin-skin')

{{-- title --}}
@section('title')
    {{ env("APP_NAME") }} | Settings
@endsection


{{-- contents --}}
@section('contents')
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border">
					<h4 class="box-title">
		        		<i class="fa fa-file"></i> Select Excel file
		      		</h4>
				</div>
		      	<div class="box-body">
					<form method="POST" action="{{ url('admin/upload/customers/info') }}" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="form-group row">
							<div class="col-md-6">
								<label>Upload File</label>
								<input type="file" class="form-control" name="customers_information" required="">
							</div>
						</div>

						<div class="form-group row">
							<div class="col-md-6">
								<button class="btn btn-default btn-md">
									<i class="fa fa-file-o"></i> Upload file
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border">
					<h4 class="box-title">
		        		<i class="fa fa-file"></i> Select Excel file for Loans
		      		</h4>
				</div>
		      	<div class="box-body">
					<form method="POST" action="{{ url('admin/upload/customers/loans') }}" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="form-group row">
							<div class="col-md-6">
								<label>Upload File</label>
								<input type="file" class="form-control" name="loans_file" required="">
							</div>
						</div>

						<div class="form-group row">
							<div class="col-md-6">
								<button class="btn btn-default btn-md">
									<i class="fa fa-file-o"></i> Upload Loan file
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-8">
	    <div class="box">
			<div class="box-header with-border">
				<h4 class="box-title">
	        		<i class="fa fa-cogs"></i> Loans Configuration
	      		</h4>
			</div>
	      	<div class="box-body">
	      		<form method="POST" onsubmit="return updateLoansConfig()">
	          		<table class="table">
	          			<thead>
	          				<tr>
	          					<th>Details</th>
	          					<th>Configuration</th>
	          				</tr>
	          			</thead>
	          			<tbody>
	          				<tr>
	          					<td>Maximun Loans Limit</td>
	          					<td>
	          						<input type="number" step="any" min="1" id="l_max_amount" class="form-control" placeholder="Enter digit">
	          					</td>
	          				</tr>
	          				<tr>
	          					<td>Minimum Loans Limit</td>
	          					<td>
	          						<input type="number" step="any" min="1" id="l_min_amount" class="form-control" placeholder="Enter digit">
	          					</td>
	          				</tr>
	          				<tr>
	          					<td>
	          						Interest between (<span>&#8358;1,000.00</span> - <span>&#8358;10,000.00</span>) above
	          					</td>
	          					<td>
	          						<input type="number" step="any" min="1" id="l_amount_1" class="form-control" placeholder="Enter interest percent">
	          					</td>
	          				</tr>
	          				<tr>
	          					<td>Minimum Loans Limit (<span>&#8358;20,000.00</span> - <span>&#8358;50,000.00</span>) above</td>
	          					<td>
	          						<input type="number" step="any" min="1" id="l_amount_2" class="form-control" placeholder="Enter interest percent">
	          					</td>
	          				</tr>
	          				<tr>
	          					<td>Minimum Loans Limit (<span>&#8358;50,000.00</span> - <span>&#8358;100,000.00</span>) above</td>
	          					<td>
	          						<input type="number" step="any" min="1" id="l_amount_3" class="form-control" placeholder="Enter interest percent">
	          					</td>
	          				</tr>
	          				<tr>
	          					<td>Minimum Loans Limit (<span>&#8358;100,000.00</span> - <span>&#8358;500,000.00</span>) above</td>
	          					<td>
	          						<input type="number" step="any" min="1" id="l_amount_4" class="form-control" placeholder="Enter interest percent">
	          					</td>
	          				</tr>
	          				<tr>
	          					<td></td>
	          					<td>
	          						<button class="btn btn-primary">
	          							<i class="fa fa-edit"></i> Update Loans
	          						</button>
	          					</td>
	          				</tr>
	          			</tbody>
	          		</table>
	      		</form>
	      	</div>
	    </div>
		</div> 
		<div class="col-md-4">
	    <div class="box">
			<div class="box-header with-border">
				<h4 class="box-title">
	        		<i class="fa fa-cog"></i> Details
	      		</h4>
			</div>
	      	<div class="box-body">
	      		<table class="table">
	      			<thead>
	      				<tr>
	      					<th>Details</th>
	      				</tr>
	      			</thead>
	      			<tbody>
	      				<tr>
	      					<td>
	      						<div class="loan_max"></div>
	      					</td>
	      				</tr>
	      				<tr>
	      					<td>
	      						<div class="loan_min"></div>
	      					</td>
	      				</tr>
	      				<tr>
	      					<td>
	      						<div class="loan_percent_1"></div>
	      					</td>
	      				</tr>
	      				<tr>
	      					<td>
	      						<div class="loan_percent_2"></div>
	      					</td>
	      				</tr>
	      				<tr>
	      					<td>
	      						<div class="loan_percent_3"></div>
	      					</td>
	      				</tr>
	      				<tr>
	      					<td>
	      						<div class="loan_percent_4"></div>
	      					</td>
	      				</tr>
	      				<tr>
	      					<td></td>
	      				</tr>
	      			</tbody>
	      		</table>
	      	</div>
	    </div>
		</div>  
	</div>
	<div class="row">
		<div class="col-md-8">
	    <div class="box">
			<div class="box-header with-border">
				<h4 class="box-title">
	        		<i class="fa fa-cogs"></i> Savings Settings
	      		</h4>
			</div>
	      	<div class="box-body">
	      		<form method="POST" onsubmit="return updateSavingConfig()">
	          		<table class="table">
	          			<thead>
	          				<tr>
	          					<th>Details</th>
	          					<th>Configuration</th>
	          				</tr>
	          			</thead>
	          			<tbody>
	          				<tr>
	          					<td>Maximun Savings Limit</td>
	          					<td>
	          						<input type="number" step="any" min="1" id="s_max_amount" class="form-control" placeholder="Enter digit">
	          					</td>
	          				</tr>
	          				<tr>
	          					<td>Minimum Savings Limit</td>
	          					<td>
	          						<input type="number" step="any" min="1" id="s_min_amount" class="form-control" placeholder="Enter digit">
	          					</td>
	          				</tr>
	          				<tr>
	          					<td>
	          						Interest between (<span>&#8358;1,000.00</span> - <span>&#8358;10,000.00</span>) above
	          					</td>
	          					<td>
	          						<input type="number" step="any" min="1" id="s_amount_1" class="form-control" placeholder="Enter interest percent">
	          					</td>
	          				</tr>
	          				<tr>
	          					<td>Minimum Savings Limit (<span>&#8358;20,000.00</span> - <span>&#8358;50,000.00</span>) above</td>
	          					<td>
	          						<input type="number" step="any" min="1" id="s_amount_2" class="form-control" placeholder="Enter interest percent">
	          					</td>
	          				</tr>
	          				<tr>
	          					<td>Minimum Savings Limit (<span>&#8358;50,000.00</span> - <span>&#8358;100,000.00</span>) above</td>
	          					<td>
	          						<input type="number" step="any" min="1" id="s_amount_3" class="form-control" placeholder="Enter interest percent">
	          					</td>
	          				</tr>
	          				<tr>
	          					<td>Minimum Savings Limit (<span>&#8358;100,000.00</span> - <span>&#8358;500,000.00</span>) above</td>
	          					<td>
	          						<input type="number" step="any" min="1" id="s_amount_4" class="form-control" placeholder="Enter interest percent">
	          					</td>
	          				</tr>
	          				<tr>
	          					<td></td>
	          					<td>
	          						<button class="btn btn-primary">
	          							<i class="fa fa-edit"></i> Update Investment
	          						</button>
	          					</td>
	          				</tr>
	          			</tbody>
	          		</table>
	          	</form>
	      	</div>
	    </div>
		</div>
		<div class="col-md-4">
	    <div class="box">
			<div class="box-header with-border">
				<h4 class="box-title">
	        		<i class="fa fa-cog"></i> Details
	      		</h4>
			</div>
	      	<div class="box-body">
	      		<table class="table">
	      			<thead>
	      				<tr>
	      					<th>Details</th>
	      				</tr>
	      			</thead>
	      			<tbody>
	      				<tr>
	      					<td>
	      						<div class="saving_max"></div>
	      					</td>
	      				</tr>
	      				<tr>
	      					<td>
	      						<div class="saving_min"></div>
	      					</td>
	      				</tr>
	      				<tr>
	      					<td>
	      						<div class="saving_percent_1"></div>
	      					</td>
	      				</tr>
	      				<tr>
	      					<td>
	      						<div class="saving_percent_2"></div>
	      					</td>
	      				</tr>
	      				<tr>
	      					<td>
	      						<div class="saving_percent_3"></div>
	      					</td>
	      				</tr>
	      				<tr>
	      					<td>
	      						<div class="saving_percent_4"></div>
	      					</td>
	      				</tr>
	      				<tr>
	      					<td></td>
	      				</tr>
	      			</tbody>
	      		</table>
	      	</div>
	    </div>
		</div>   
	</div>
@endsection


{{-- scripts section --}}
@section('scripts')
	@if(session('message'))
		<!-- modal Area -->              
		<div class="modal center-modal fade" id="upload-modal" role="dialog" tabindex="-1">
		    <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			      <h4 class="modal-title">Upload Successful!</h4>
			      <button type="button" class="close" data-dismiss="modal">
			        <span aria-hidden="true">&times;</span>
			      </button>
			      </div>
			      <div class="modal-body">
			        {{ session('message') }}
			      </div>
			    </div>
			    <!-- /.modal-content -->
		    </div>
		    <!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
		<script type="text/javascript">
			$("#upload-modal").modal();
		</script>
	@endif

	<script type="text/javascript">
		// $(document).ready(function (){
			getFomularConfiguration();
		// });

		// updates loan configuration
		function updateLoansConfig() {
			var token 		= $("#token").val();
			var min_amount 	= $("#l_min_amount").val();
			var max_amount 	= $("#l_max_amount").val();
			var amount_1 	= $("#l_amount_1").val();
			var amount_2 	= $("#l_amount_2").val();
			var amount_3 	= $("#l_amount_3").val();
			var amount_4 	= $("#l_amount_4").val();

			var params = {
				_token: token,
				min_amount: min_amount,
				max_amount: max_amount,
				amount_1: amount_1,
				amount_2: amount_2,
				amount_3: amount_3,
				amount_4: amount_4,
				f_type: 'loans'
			}

			$.post('{{ url('admin/update/formular') }}', params, function(data) {
				/*optional stuff to do after success */
				if(data.status == "success"){
					swal(
						data.status,
						data.message,
						data.status,
					);
				}else{
					swal(
						"oops",
						data.message,
						data.status,
					);
				}
			});

			// return
			return false;
		}

		// updates savings configuration
		function updateSavingConfig() {
			var token 		= $("#token").val();
			var min_amount 	= $("#s_min_amount").val();
			var max_amount 	= $("#s_max_amount").val();
			var amount_1 	= $("#s_amount_1").val();
			var amount_2 	= $("#s_amount_2").val();
			var amount_3 	= $("#s_amount_3").val();
			var amount_4 	= $("#s_amount_4").val();

			var params = {
				_token: token,
				min_amount: min_amount,
				max_amount: max_amount,
				amount_1: amount_1,
				amount_2: amount_2,
				amount_3: amount_3,
				amount_4: amount_4,
				f_type: 'savings'
			}

			$.post('{{ url('admin/update/formular') }}', params, function(data) {
				/*optional stuff to do after success */
				if(data.status == "success"){
					swal(
						data.status,
						data.message,
						data.status,
					);
				}else{
					swal(
						"oops",
						data.message,
						data.status,
					);
				}
			});

			// return
			return false;
		}

		// get configuration
		function getFomularConfiguration() {
			$.get('{{ url('admin/get/formular') }}', function(data) {
				// console log value
				$.each(data, function(index, val) {
					if(val.formular_type == 'loans'){
						$("#l_min_amount").val(val.min_amount);
						$("#l_max_amount").val(val.max_amount);
						$("#l_amount_1").val(val.amount_1);
						$("#l_amount_2").val(val.amount_2);
						$("#l_amount_3").val(val.amount_3);
						$("#l_amount_4").val(val.amount_4);

						$(".loan_max").html('&#8358;' + numeral(val.max_amount).format('0,0.00'));
						$(".loan_min").html('&#8358;' + numeral(val.min_amount).format('0,0.00'));

						let loan1 = parseFloat(val.amount_1 / 100);
						let loan2 = parseFloat(val.amount_2 / 100);
						let loan3 = parseFloat(val.amount_3 / 100);
						let loan4 = parseFloat(val.amount_4 / 100);

						$(".loan_percent_1").html(`
							${loan1}%
						`);
						$(".loan_percent_2").html(`
							${loan2}%
						`);
						$(".loan_percent_3").html(`
							${loan3}%
						`);
						$(".loan_percent_4").html(`
							${loan4}%
						`);
					}

					if(val.formular_type == 'savings'){
						$("#s_min_amount").val(val.min_amount);
						$("#s_max_amount").val(val.max_amount);
						$("#s_amount_1").val(val.amount_1);
						$("#s_amount_2").val(val.amount_2);
						$("#s_amount_3").val(val.amount_3);
						$("#s_amount_4").val(val.amount_4);


						$(".saving_max").html('&#8358;' + numeral(val.max_amount).format('0,0.00'));
						$(".saving_min").html('&#8358;' + numeral(val.min_amount).format('0,0.00'));

						let loan1 = parseFloat(val.amount_1 / 100);
						let loan2 = parseFloat(val.amount_2 / 100);
						let loan3 = parseFloat(val.amount_3 / 100);
						let loan4 = parseFloat(val.amount_4 / 100);

						$(".saving_percent_1").html(`
							${loan1}%
						`);
						$(".saving_percent_2").html(`
							${loan2}%
						`);
						$(".saving_percent_3").html(`
							${loan3}%
						`);
						$(".saving_percent_4").html(`
							${loan4}%
						`);
					}
				});
			});
		}
	</script>
@endsection