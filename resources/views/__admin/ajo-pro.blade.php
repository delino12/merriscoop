@extends('layouts.admin-skin')

{{-- title --}}
@section('title')
    {{ env("APP_NAME") }} | Ajo Pro
@endsection


{{-- contents --}}
@section('contents')
  <div class="row">
    <div class="col-12">
      <div class="box">
        <div class="box-header with-border">
          <h4 class="box-title">
            <i class="fa fa-money"></i> Ajo Scheduled
          </h4>
        </div>
        <div class="box-body">
          <div class="table-responsive">
            <table id="transactions" class="table table-hover no-wrap product-order" data-page-size="10">
              <thead>
                <tr>
                   <th>Customer</th>
                   <th>Ref ID</th>
                   <th>Type</th>
                   <th>Amount (&#8358;)</th>
                   <th>Date</th>
                   <th>Status</th>
                   <th>Actions</th>
                   <th>History</th>
                </tr>
              </thead>
              <tbody class="all-transactions">
                <tr>
                  <td>Loading...</td>
                </tr>
              </tbody>            
            </table>
          </div>
        </div>
      </div>           
    </div>
  </div>
@endsection


{{-- scripts section --}}
@section('scripts')
  <script type="text/javascript">
    // load all ajo set
    loadAllAjoSet();

    // load all transactions
    function loadAllAjoSet() {
      $.get('{{ url('admin/load/all/ajo-pro') }}', function(data) {
        $(".all-transactions").html("");
        $.each(data, function(index, val) {
          // console log value
          // console.log(val);
          $(".all-transactions").append(`
            <tr>
              <td>${val.details.firstname} ${val.details.lastname}</td>
              <td>${val.transaction.ref_no}</td>
              <td>${val.transaction.name}</td>
              <td>${numeral(val.transaction.amount).format('0,0.00')}</td>
              <td>${val.transaction.created_at}</td>
              <td class="text-capitalize">${val.transaction.status}</td>
              <td>
                <a href="{{url('admin/view-payment')}}/${val.loan.id}">
                  <i class="fa fa-file-text-o" aria-hidden="true"></i> View
                </a>
              </td>
              <td>
                <a href="{{url('admin/view-loan')}}/${val.loan.id}">
                  <i class="fa fa-file-text-o" aria-hidden="true"></i> Track
                </a>
              </td>
            </tr>
          `);
        });

        // $('#transactions').DataTable();
        $('#transactions').DataTable({
          'paging'      : true,
          'lengthChange': false,
          'searching'   : true,
          'ordering'    : true,
          'info'        : true,
          'autoWidth'   : false
        });
      });
    }
  </script>
@endsection