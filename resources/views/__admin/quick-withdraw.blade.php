@extends('layouts.admin-skin')

{{-- title --}}
@section('title')
  {{ env("APP_NAME") }} | Instant Withdraw
@endsection

{{-- contents --}}
@section('contents')
  <div class="row">
    <div class="col-12 col-lg-12">
       <div class="row">
            <div class="col-md-12 p-2">
                <a href="{{ url('agent/dashboard') }}">
                    <i class="fa fa-arrow-left"></i> Home
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 p-2">
                <h5 class="p-2">Instant Withdraw</h5>
                <hr />
                <form method="post" onsubmit="return searchCustomer()">
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                          <label>Enter customer name</label>
                          <input type="text" id="search_keyword" class="form-control" placeholder="Search customer name">
                      </div>
                    </div>
                  </div>
                    <div class="form-group">
                        <button class="btn btn-primary">
                            <i class="fa fa-search"></i> Search
                        </button>
                    </div>
                </form>
            </div>

            <div class="col-md-12 p-2">
                <table class="table" width="100%">
                    <thead>
                      <tr>
                        <td>Customer Name</td>
                        <td>Account Type</td>
                        <td>Balance (&#8358;)</td>
                        <td>Total (&#8358;)</td>
                        <td>Status</td>
                        <td>Enter Amount</td>
                        <td>Action</td>
                      </tr>
                    </thead>
                    <tbody class="load-search"></tbody>
                </table>
            </div>
        </div>
        <!--/.row-->
    </div>             
  </div>
@endsection

{{-- scripts section --}}
@section('scripts')
    <script type="text/javascript">
        function searchCustomer() {
            // body..
            var token   = $("#token").val();
            var keyword = $("#search_keyword").val();

            var params = {
                _token: token,
                keyword: keyword
            };

            $.post('{{ url('admin/search/customer') }}', params, function(data, textStatus, xhr) {
                /*optional stuff to do after success */
                buildWithdrawalView(data);
                // console log value
                console.log(data);
            });

            // return void
            return false;
        }

        function buildWithdrawalView(data) {
            // body...
            var sn = 0;
            $(".load-search").html("");
            // $(".customer-info").html("");
            $.each(data, function(index, val) {
                sn++;

                var accountType;
                if(val.type == 'loans'){
                  accountType = 'Loan Account';
                }else{
                  accountType = 'Savings Account';
                }

                $(".load-search").append(`
                    <tr>
                        <td>${val.name.toUpperCase()}</td>
                        <td>${accountType}</td>
                        <td>&#8358;${numeral(val.account.balance).format('0,0.00')}</td>
                        <td>&#8358;${numeral(val.account.total).format('0,0.00')}</td>
                        <td style="text-transform:capitalised;">${val.account.status}</td>
                        <td>
                            <input type="text" class="form-control" id="amount_${sn}" placeholder="0.00" />
                        </td>
                        <td>
                            <a href="javascript:void(0);" class="btn btn-default btn-sm" id="repay-btn-${sn}" onclick="acceptWithdraw(${val.id}, ${val.account.id}, ${sn})">
                                Withdraw
                            </a>
                        </td>
                    </tr>
                `);
            });
        }

        function acceptWithdraw(user_id, account_id, sn) {
            // processing
            $("#repay-btn-"+sn).html("Processing");

            // body..
            var token   = $("#token").val();
            var amount  = $("#amount_"+sn).val();

            var params = {
                _token: token,
                amount: amount,
                user_id: user_id,
                account_id: account_id
            };

            $.post('{{ url('admin/accept/withdraw') }}', params, function(data, textStatus, xhr) {
                /*optional stuff to do after success */
                // buildDepositView(data);
                if(data.status == "success"){
                    swal(
                        "success",
                        data.message,
                        data.status
                    );
                    $("#amount_"+sn).val("");
                    $("#repay-btn-"+sn).html("Withdraw");
                    // clear sms queue
                    clearSmsQueue();
                }else{
                    swal(
                        "oops",
                        data.message,
                        data.status
                    );
                    $("#repay-btn-"+sn).html("Withdraw");
                }
            });
        }
  </script>
@endsection