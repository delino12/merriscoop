@extends('layouts.admin-skin')

{{-- title --}}
@section('title')
    {{ env("APP_NAME") }} | Active Savings
@endsection

{{-- contents --}}
@section('contents')
  <div class="row">
    <div class="col-12 col-lg-12">
        <div class="box">
            <div class="box-header with-border">
              <h4 class="box-title">
                <i class="fa fa-users"></i> Savings Account
              </h4>

              <span class="pull-right">
              <a href="{{ url('admin/apply/new/savings') }}" class="btn btn-outline">
                <i class="fa fa-plus"></i>
                New Savings
              </a>
            </span>
            </div>
          <div class="box-body p-2">
            <div class="table-responsive">
              <table id="all-customers" class="table table-hover no-wrap" data-page-size="10">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Account No</th>
                      <th>Name</th>
                      <th>Phone</th>
                      <th>Account Type</th>
                      <th>Balance</th>
                      <th>Last updated</th>
                      <th>Option</th>
                      <th>Track</th>
                    </tr>
                  </thead>
                  <tbody class="load-all-customers">
                      <tr>
                          <td>Loading...</td>
                      </tr>
                  </tbody>
              </table>
          </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /. box -->
    </div>             
  </div>
@endsection

{{-- scripts section --}}
@section('scripts')
  <script type="text/javascript">
    // $(document).ready(function (){
    loadAllCustomers();
    // });

    // load all customers
    function loadAllCustomers() {
      $.get('{{ url('admin/load/all/customers') }}', function(data) {
        var sn = 0;
        $(".load-all-customers").html("");
        $.each(data, function(index, val) {
          sn++;
          var shadeRow;
          if(val.type == "savings"){
            // console log value
            $(".load-all-customers").append(`
              <tr ${shadeRow}>
                <td>${sn}</td>
                <td>${val.account_info.account_id.toUpperCase()}</td>
                <td>${val.name.toUpperCase()}</td>
                <td>${val.details.phone}</td>
                <td>${val.description}</td>
                <td>&#8358;${numeral(val.account.balance).format('0,0.00')}</td>
                <td>${val.date}</td>
                <td>
                  <a href="{{url('admin/view-client')}}/${val.id}">
                    <i class="fa fa-file-text-o" aria-hidden="true"></i> View
                  </a>
                </td>
                <td>
                  <a href="{{url('admin/view-savings')}}/${val.account_info.id}">
                    <i class="fa fa-file-text-o" aria-hidden="true"></i> Track
                  </a>
                </td>
              </tr>
            `);
          }
        });

        // $('#transactions').DataTable();
        $('#all-customers').DataTable({
          'paging'      : true,
          'lengthChange': false,
          'searching'   : true,
          'ordering'    : false,
          'info'        : true,
          'autoWidth'   : false
        });
      });
    }
  </script>
@endsection