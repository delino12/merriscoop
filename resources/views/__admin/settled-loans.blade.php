@extends('layouts.admin-skin')

{{-- title --}}
@section('title')
    {{ env("APP_NAME") }} | Settled Loans
@endsection

{{-- contents --}}
@section('contents') 
  <div class="row">
    <div class="col-12 col-lg-12">
        <div class="box">
            <div class="box-header with-border">
              <h4 class="box-title">
                <i class="fa fa-users"></i> Settled Loans
              </h4>
            </div>
          <div class="box-body p-2">
            <div class="table-responsive">
              <table id="invoice-list" class="table table-hover no-wrap" data-page-size="10">
                  <thead>
                      <tr>
                          <th>#</th>
                          <th>Name</th>
                          <th>Phone</th>
                          <th>Account Type</th>
                          <th>Balance</th>
                          <th>Last updated</th>
                          <th>View</th>
                          <th>Signed</th>
                      </tr>
                  </thead>
                  <tbody class="load-settled-loans">
                      <tr>
                          <td>Loading...</td>
                      </tr>
                  </tbody>
              </table>
          </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /. box -->
    </div>             
  </div>
@endsection

{{-- scripts section --}}
@section('scripts')
  <script type="text/javascript">
    loadSettledLoans();

    // load all customers
    function loadSettledLoans() {
      $.get('{{ url('admin/load/settled/loans') }}', function(data) {
        // console log value
        // console.log(data);
        var sn = 0;
        $(".load-settled-loans").html("");
        $.each(data, function(index, val) {
          sn++;
          var shadeRow = `class="text-info"`; 
          // console log value
          $(".load-settled-loans").append(`
            <tr ${shadeRow}>
              <td>${sn}</td>
              <td>${val.bio_info.lastname.toUpperCase()} ${val.bio_info.firstname.toUpperCase()} </td>
              <td>${val.bio_info.phone}</td>
              <td>${val.description}</td>
              <td>&#8358;${numeral(val.balance).format('0,0.00')}</td>
              <td>${val.date}</td>
              <td>
                <a href="{{url('admin/view-client')}}/${val.user_id}">
                  <i class="fa fa-file-text-o" aria-hidden="true"></i> View
                </a>
              </td>
              <td>
                <a href="javascript:void(0);">
                  <i class="fa fa-check-circle-o" aria-hidden="true"></i> Settled (MerrisCoop)
                </a>
              </td>
            </tr>
          `);
        });
      });
    }
  </script>
@endsection