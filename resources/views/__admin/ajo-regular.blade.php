@extends('layouts.admin-skin')

{{-- title --}}
@section('title')
    {{ env("APP_NAME") }} | Ajo General
@endsection


{{-- contents --}}
@section('contents')
  <div class="row">
    <div class="col-12">
      <div class="box">
        <div class="box-header with-border">
          <h4 class="box-title">
            <i class="fa fa-money"></i> Ajo General
          </h4>
          <span class="pull-right">
            <i class="fa fa-plus"></i> 
            <a href="javascript:void(0);" onclick="addBookModal()">Ajo Book</a>
          </span>
        </div>
        <div class="box-body">
          <div class="table-responsive">
            <table id="transactions" class="table table-hover no-wrap product-order" data-page-size="10">
              <thead>
                <tr>
                   <th>Book Name</th>
                   <th>Ajo Type</th>
                   <th>Memeber's Found</th>
                   <th>Unit Amount (&#8358;)</th>
                   <th>Turnover Amount (&#8358;)</th>
                   <th>Date</th>
                   <th>Option</th>
                </tr>
              </thead>
              <tbody class="all-ajo-general">
                <tr>
                  <td>Loading...</td>
                </tr>
              </tbody>            
            </table>
          </div>
        </div>
      </div>           
    </div>
  </div>

  <!-- modal Area -->              
  <div class="modal center-modal fade" id="add-book-modal" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <h4 class="modal-title">New Ajo Journal</h4>
      <button type="button" class="close" data-dismiss="modal">
        <span aria-hidden="true">&times;</span>
      </button>
      </div>
      <div class="modal-body">
        <form method="post" onsubmit="return createAjoBook()">
          <div class="from-group">
            <label>Enter Name</label>
            <input type="text" class="form-control" id="book_name" placeholder="Type a book name" required="">
          </div>

          <div class="from-group">
            <label>Enter Amount</label>
            <input type="number" class="form-control" id="book_amount" placeholder="Eg. 5000" required="">
          </div>

          <div class="from-group">
            <label>Select Category</label>
            <select id="book_type" class="form-control">
              <option value="ajo-regular">Ajo Regular</option>
              <option value="ajo-pro">Ajo Pro</option>
            </select>
          </div>

          <div class="from-group">
            <label>Select Collection Quota</label>
            <select id="book_quota" class="form-control">
              <option value="daily">Daily</option>
              <option value="weekly">Weekly</option>
              <option value="monthly">Monthly</option>
            </select>
          </div>

          <div class="from-group">
            <br />
            <input type="submit" class="form-control" value="Create Book">
          </div>
        </form>
      </div>

    </div>
    <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
@endsection


{{-- scripts section --}}
@section('scripts')
  <script type="text/javascript">
    // load modules
    getAllBook();

    // add book modal
    function addBookModal() {
      $("#add-book-modal").modal({
        backdrop: false
      });
    }

    // create ajo book
    function createAjoBook() {
      var token     = $("#token").val();
      var book_name = $("#book_name").val();
      var book_type = $("#book_type").val();
      var amount    = $("#book_amount").val();
      var quota     = $("#book_quota").val();

      var params = {
        _token: token,
        book_name: book_name,
        book_type: book_type,
        amount: amount,
        quota: quota
      };

      $.post('{{ url('admin/create/book') }}', params, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
        if(data.status == "success"){
          swal(
            "success",
            data.message,
            data.status
          );
          $("#add-book-modal").modal('hide');
          getAllBook();
        }else{
          swal(
            "oops",
            data.message,
            data.status
          );
        }
      });

      return false;
    }

    // get all book
    function getAllBook() {
      $.get('{{ url('admin/get-all/book') }}', function(data) {
        $(".all-ajo-general").html("");
        $.each(data, function(index, val) {
          $(".all-ajo-general").append(`
            <tr>
              <td>${val.name}</td>
              <td>${val.ajo_type}</td>
              <td>${val.members}</td>
              <td>${val.amount}</td>
              <td>${val.turnover}</td>
              <td>${val.last_seen}</td>
              <td>
                <a href="{{ url('admin/view/ajo') }}/${val.id}">View</a>
              </td>
            </tr>
          `);
        });
      });
    }
  </script>
@endsection