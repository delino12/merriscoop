@extends('layouts.admin-skin')

{{-- title --}}
@section('title')
  {{ env("APP_NAME") }} | Active Loans
@endsection

{{-- contents --}}
@section('contents')
  <div class="row">
    <div class="col-12 col-lg-12">
        <div class="box">
          <div class="box-header with-border">
            <h4 class="box-title">
              <i class="fa fa-users"></i> Loans Account
            </h4>

            
            <span class="pull-right">
              <a href="{{ url('admin/apply/new/loan') }}" class="btn btn-outline">
                <i class="fa fa-plus"></i>
                New loan
              </a>
            </span>
          </div>
          <div class="box-body p-2">
            <div class="table-responsive">
              <table id="invoice-list" class="table table-hover no-wrap" data-page-size="10">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Loan Amount (&#8358;)</th>
                    <th>Total Payable (&#8358;)</th>
                    <th>Loan Due Date</th>
                    <th>Status</th>
                    <th>Last Updated</th>
                    <th>History</th>
                  </tr>
                </thead>
                <tbody class="load-all-customers">
                  <tr>
                    <td>Loading...</td>
                  </tr>
                </tbody>
              </table>
          </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /. box -->
    </div>             
  </div>
@endsection

{{-- scripts section --}}
@section('scripts')
  <script type="text/javascript">
    // $(document).ready(function (){
      loadAllCustomers();
    // });

    // load all customers
    function loadAllCustomers() {
      $.get('{{ url('admin/load/active/loans') }}', function(data) {
        var sn = 0;
        $(".load-all-customers").html("");
        $.each(data, function(index, val) {
          sn++;

          var shadeRow;

          if(val.status == "settled"){
            shadeRow = `class="text-green"`;
          }else{
            shadeRow = `class="text-white"`;
          } 

          // console log value
          $(".load-all-customers").append(`
            <tr ${shadeRow}>
              <td>${sn}</td>
              <td>${val.details.name.toUpperCase()}</td>
              <td>&#8358;${numeral(val.balance).format('0,0.00')}</td>
              <td>&#8358;${numeral(val.total).format('0,0.00')}</td>
              <td>${val.duration}</td>
              <td class="text-capitalize">${val.status}</td>
              <td>${val.date}</td>
              <td>
                <a href="{{url('admin/view-loan')}}/${val.id}">
                  <i class="fa fa-file-text-o" aria-hidden="true"></i> Track
                </a>
              </td>
            </tr>
          `);
          
        });
      });
    }
  </script>
@endsection