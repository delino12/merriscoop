@extends('layouts.admin-skin')

{{-- title --}}
@section('title')
    {{ env("APP_NAME") }} | Home
@endsection


{{-- contents --}}
@section('contents')
  <div class="row">
      <div class="col-12 col-lg-6">
          <div class="row">
              <div class="col-12">
                <div class="show-news-updates"></div>
              </div>
              <div class="col-md-6 col-12">
                <div class="box box-body">
                  <h6 class="mb-30">
                    <span class="text-uppercase">Interest on Loans</span>
                  </h6>
                  
                  <p class="font-size-26 total_interest"></p>

                  <div class="progress progress-xxs mt-0 mb-10">
                    <div class="progress-bar bg-danger" role="progressbar" style="width: 35%; height: 4px;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-12">
                  <div class="box box-body">
                    <h6 class="mb-30">
                      <span class="text-uppercase">Total Loans</span>
                    </h6>
                    
                    <p class="font-size-26 total_request"></p>

                    <div class="progress progress-xxs mt-0 mb-10">
                      <div class="progress-bar bg-success" role="progressbar" style="width: 75%; height: 4px;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </div>
              </div>
          </div>      
      </div>

      <div class="col-12 col-lg-6">
          <div class="row">
              <div class="col-12">
                <div class="show-news-updates"></div>
              </div>
              <div class="col-md-6 col-12">
                <div class="box box-body">
                  <h6 class="mb-30">
                    <span class="text-uppercase">Interest on Savings</span>
                  </h6>
                  
                  <p class="font-size-26 total_interest_savings"></p>

                  <div class="progress progress-xxs mt-0 mb-10">
                    <div class="progress-bar bg-danger" role="progressbar" style="width: 35%; height: 4px;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-12">
                  <div class="box box-body">
                    <h6 class="mb-30">
                      <span class="text-uppercase">Total Savings</span>
                    </h6>
                    
                    <p class="font-size-26 total_request_savings"></p>

                    <div class="progress progress-xxs mt-0 mb-10">
                      <div class="progress-bar bg-success" role="progressbar" style="width: 75%; height: 4px;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </div>
              </div>
          </div>      
      </div>
        
      <div class="col-12 col-lg-8">
          <div class="box">
              <div class="box-inverse bg-danger bg-bubbles-dark"> 
              <div class="box-header no-border">
                  <h4>Overall Loan request</h4>
                  <ul class="box-controls pull-right">
                    <li class="dropdown">
                      <a data-toggle="dropdown" href="javascript:void(0);" class="btn btn-rounded btn-outline btn-white px-10">Stats</a>
                      <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="javascript:void(0);"><i class="ti-import"></i> Import</a>
                        <a class="dropdown-item" href="javascript:void(0);"><i class="ti-export"></i> Export</a>
                        <a class="dropdown-item" href="javascript:void(0);"><i class="ti-printer"></i> Print</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="javascript:void(0);"><i class="ti-settings"></i> Settings</a>
                      </div>
                    </li>
                  </ul>
              </div>
              <div class="box-body pb-60">
                  <h1 class="text-center font-size-30 total_request"></h1>
              </div>
              </div>
              <div class="box-body">
                  <div class="text-center py-10 bb-1 bb-dashed">
                     <h4>Loans</h4>
                     <ul class="flexbox flex-justified text-center my-20">
                      <li class="px-10">
                        <h6 class="mb-0 text-bold count_pending"></h6>
                        <small>Pending</small>
                      </li>

                      <li class="br-1 bl-1 px-10">
                        <h6 class="mb-0 text-bold count_approved"></h6>
                        <small>Approved</small>
                      </li>

                      <li class="px-10">
                        <h6 class="mb-0 text-bold count_settled"></h6>
                        <small>Settled</small>
                      </li>
                    </ul>                     
                  </div>
                  <div class="text-center py-10 bb-1 bb-dashed">
                    <h4>Savings & Investment</h4>
                    <ul class="flexbox flex-justified text-center my-20">
                      <li class="px-10">
                        <h6 class="mb-0 text-bold count_pending"></h6>
                        <small>Completed</small>
                      </li>
                      <li class="br-1 bl-1 px-10">
                        <h6 class="mb-0 text-bold count_approved"></h6>
                        <small>Active</small>
                      </li>
                    </ul>                     
                  </div>
              </div>
          </div>
      </div>
        
      <div class="col-12 col-lg-4">
        <div class="box">
          <div class="box-body">
            <div class="flexbox">
              <h5>Active Loan</h5>
            </div>

            <div class="text-center my-2">
              <div class="font-size-60 count_all"></div>
              <span>Users</span>
            </div>
          </div>

          <div class="box-body bg-gray-light py-20">
            <span class="text-muted mr-1">Overdue:</span>
            <span class="text-dark">0</span>
          </div>

          <div class="progress progress-sm mt-0 mb-0">
            <div class="progress-bar bg-info-gradient-animet" role="progressbar" style="width: 72%;" aria-valuenow="72" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
        </div>

        <div class="box">
          <div class="box-body">
            <div class="flexbox">
              <h5>Active Savings</h5>
            </div>

            <div class="text-center my-2">
              <div class="font-size-60 count_savings"></div>
              <span>Users</span>
            </div>
          </div>

          <div class="box-body bg-gray-light py-20">
            <span class="text-muted mr-1">Overdue:</span>
            <span class="text-dark">0</span>
          </div>

          <div class="progress progress-sm mt-0 mb-0">
            <div class="progress-bar bg-info-gradient-animet" role="progressbar" style="width: 72%;" aria-valuenow="72" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
        </div>
      </div>
  </div>    
      
  <div class="row">
    <div class="col-12">
      <div class="box">
        <div class="box-header with-border">
          <h4 class="box-title">
            <i class="fa fa-money"></i> Transactions
          </h4>
        </div>
        <div class="box-body">
          <div class="table-responsive">
            <table id="transactions" class="table table-hover no-wrap product-order" data-page-size="10">
              <thead>
                <tr>
                   <th>Customer</th>
                   <th>Ref ID</th>
                   <th>Type</th>
                   <th>Amount (&#8358;)</th>
                   <th>Date</th>
                   <th>History</th>
                </tr>
              </thead>
              <tbody class="all-transactions">
                <tr>
                  <td>Loading...</td>
                </tr>
              </tbody>            
            </table>
          </div>
        </div>
      </div>           
    </div>
  </div>
@endsection


{{-- scripts section --}}
@section('scripts')
  <script type="text/javascript">
    // $(document).ready(function(d){
      loadDashboardStatistic();
      loadAllTransactions();
      getNewsUpdates();
    // });

    // load dashboard statistic
    function loadDashboardStatistic() {
      $.get('{{ url('admin/load/statistic') }}', function(data) {
        // console log value
        // console log value
        // console.log(data.loan);
        $(".total_interest").html('&#8358;'+ data.loan.total_interest);
        $(".total_request").html('&#8358;'+ data.loan.total_request);
        $(".total_settled").html('&#8358;'+ data.loan.total_settled);
        $(".total_approved").html('&#8358;'+ data.loan.total_approved);
        $(".total_request").html('&#8358;'+ data.loan.total_request);

        $(".count_pending").html(data.loan.count_pending);
        $(".count_approved").html(data.loan.count_approved);
        $(".count_settled").html(data.loan.count_settled);
        $(".count_all").html(data.loan.count_active);

        $(".total_interest_savings").html('&#8358;'+ data.account.total_interest);
        $(".total_request_savings").html('&#8358;'+ data.account.total_savings);
        $(".count_savings").html(data.account.count_active);
      });
    }

    // load all transactions
    function loadAllTransactions() {
      $.get('{{ url('admin/load/all/transactions') }}', function(data) {
        $(".all-transactions").html("");
        $.each(data, function(index, val) {
          // console log value
          // console.log(val);
          var trackBtn;
          var shadeRow;
          if(val.account_type == "savings"){
            shadeRow = `class="text-info"`;
            trackBtn = `
              <a href="{{url('admin/view-savings')}}/${val.account.id}">
                <i class="fa fa-file-text-o" aria-hidden="true"></i> Track
              </a>
            `;
          }else{
            shadeRow = `class="text-warning"`;
            trackBtn = `
              <a href="{{url('admin/view-loan')}}/${val.loan.id}">
                <i class="fa fa-file-text-o" aria-hidden="true"></i> Track
              </a>
            `;
          }

          $(".all-transactions").append(`
            <tr ${shadeRow}>
              <td>${val.details.firstname.toUpperCase()} ${val.details.lastname.toUpperCase()}</td>
              <td>${val.transaction.ref_no}</td>
              <td>${val.transaction.name.toUpperCase()}</td>
              <td>${numeral(val.transaction.amount).format('0,0.00')}</td>
              <td>${val.transaction.created_at}</td>
              <td>
                ${trackBtn}
              </td>
            </tr>
          `);
        });

        // $('#transactions').DataTable();
        $('#transactions').DataTable({
          'paging'      : true,
          'lengthChange': false,
          'searching'   : true,
          'ordering'    : false,
          'info'        : true,
          'autoWidth'   : false
        });
  
      });
    }

    // get news updates
    function getNewsUpdates() {
      $.get('{{ url('admin/load/business/news') }}', function(data) {
        let sn = 0;
        $.each(data, function(index, val) {
          // console log value
          // console.log(val);
          sn++;
          $(".show-news-updates").html(`
            <div class="box bg-img box-inverse" style="background-image: url('${val.urlToImage}');" data-overlay="7">             
              <div class="box-header with-border">
                <h4 class="box-title">Latest</h4>
                <ul class="box-controls pull-right">
                  <li><a class="box-btn-close" href="javascript:void(0);"></a></li>
                  <li><a class="box-btn-fullscreen" href="javascript:void(0);"></a></li>
                </ul>
              </div>
              <div class="box-body mt-20">
                <span class="badge" data-overlay="5">New</span>
                <h2 class="font-weight-200 mb-0">${val.title}</h2>
                <p>${val.description}</p>
              </div>
            </div>
          `);
          if(sn > 0){
            // break
            return false;
          }
        });
      });
    }
  </script>
@endsection