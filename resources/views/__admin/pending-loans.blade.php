@extends('layouts.admin-skin')

{{-- title --}}
@section('title')
    {{ env("APP_NAME") }} | Pending Loans
@endsection

{{-- contents --}}
@section('contents') 
  <div class="row">
    <div class="col-12 col-lg-12">
        <div class="box">
            <div class="box-header with-border">
              <h4 class="box-title">
                <i class="fa fa-users"></i> Pending Loans
              </h4>
            </div>
          <div class="box-body">
            <div class="table-responsive">
              <table id="invoice-list" class="table table-hover no-wrap" data-page-size="10">
                  <thead>
                      <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Account Type</th>
                        <th>Balance</th>
                        <th>Status</th>
                        <th>Last updated</th>
                        <th>View</th>
                        <th>Action</th>
                      </tr>
                  </thead>
                  <tbody class="load-all-customers">
                      <tr>
                          <td>Loading...</td>
                      </tr>
                  </tbody>
              </table>
          </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /. box -->
    </div>             
  </div>
@endsection

{{-- scripts section --}}
@section('scripts')
  <script type="text/javascript">
    $(document).ready(function (){
      loadAllCustomers();
    });

    // load all customers
    function loadAllCustomers() {
      $.get('{{ url('admin/load/pending/loans') }}', function(data) {
        var sn = 0;
        $(".load-all-customers").html("");
        $.each(data, function(index, val) {
          sn++;

          var shadeRow;
          if(val.status == "pending"){
            shadeRow = `class="text-white"`; 

            // console log value
            $(".load-all-customers").append(`
              <tr ${shadeRow}>
                <td>${sn}</td>
                <td>${val.details.name.toUpperCase()}</td>
                <td>${val.bio_info.phone}</td>
                <td>${val.description}</td>
                <td>&#8358;${numeral(val.balance).format('0,0.00')}</td>
                <td class="text-capitalize">${val.status}</td>
                <td>${val.date}</td>
                <td>
                  <a href="{{url('admin/view-client')}}/${val.user_id}">
                    <i class="fa fa-file-text-o" aria-hidden="true"></i> View
                  </a>
                </td>
                <td>
                  <a href="javascript:void(0);" onclick="approveClientLoan('${val.details.name}', ${val.user_id}, ${val.id})">
                    <i class="fa fa-edit" aria-hidden="true"></i> Approve
                  </a>
                </td>
              </tr>
            `);
          }
        });
      });
    }

    // approve customer loan
    function approveClientLoan(name, user_id, loan_id) {
      // body...
      const swalWithBootstrapButtons = swal.mixin({
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false,
      })

      swalWithBootstrapButtons({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Approve!',
        cancelButtonText: 'Decline!',
        reverseButtons: false
      }).then((result) => {
        if (result.value) {

          // params
          var params = {
            _token: $("#token").val(),
            user_id: user_id,
            loan_id: loan_id
          };

          // params
          // console.log(params);

          // upload section
          $.post('{{ url('admin/approve-client/loan') }}', params, function(data) {
            /*optional stuff to do after success */
            if(data.status == "success"){

              // refresh customers
              loadAllCustomers();

              swalWithBootstrapButtons(
                'Approved!',
                name+' Loan request has been approved!',
                'success'
              );
            }else{
              swalWithBootstrapButtons(
                'Approved!',
                name+' Loan approval was not successful',
                'error'
              );
            }
          });
        } else if (
          // Read more about handling dismissals
          result.dismiss === swal.DismissReason.cancel

        ) {
          swalWithBootstrapButtons(
            'Cancelled',
            name+' Loan was declined',
            'error'
          )
        }
      })
    }
  </script>
@endsection