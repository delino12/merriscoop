<?php

namespace Merriscoop;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    /*
    |---------------------------------------------
    | CREATE DEFAULT AGENT
    |---------------------------------------------
    */
    public static function createDefaultAgent(){
    	$code 	= "M1234";
    	$pin 	= "M1234";

    	$agent_exist = Agent::where("email", $code)->first();
    	if($agent_exist == null){
    		$new_agent 				= new Agent();
    		$new_agent->email 		= $code;
    		$new_agent->password 	= bcrypt($pin);
    		$new_agent->save();
    	}
    }
}
