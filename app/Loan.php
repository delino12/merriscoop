<?php

namespace Merriscoop;

use Illuminate\Database\Eloquent\Model;
use Merriscoop\Formular;
use Merriscoop\BioData;
use Merriscoop\Transaction;
use Merriscoop\User;
use Carbon\Carbon;

class Loan extends Model
{
    /*
    |---------------------------------------------
    | CREATE LOAN ACCOUNT & PENDING APPROVAL
    |---------------------------------------------
    */
    public function createLoanAccount($payload, $user_id){

        $mutable  = Carbon::now();
        $duration = $mutable->addDays($payload->duration);

        // get formular
        $percent    = $this->getPercent($payload->interest);

        $interest   = $percent * $payload->amount;
        $total      = $payload->amount + $interest;

    	$new_loan 			= new Loan();
    	$new_loan->user_id	= $user_id;
    	$new_loan->balance 	= $payload->amount;
    	$new_loan->status 	= "pending";
        $new_loan->total    = $total;
        $new_loan->interest = $interest;
        $new_loan->duration = $duration;
    	if($new_loan->save()){
            return true;
        }else{
            return false;
        }
    }

    /*
    |---------------------------------------------
    | FETCH LOAN DATA
    |---------------------------------------------
    */
    public function getUserById($user_id){
        $loan_data = Loan::where("user_id", $user_id)->first();

        // return
        return $loan_data;
    }

    /*
    |---------------------------------------------
    | FETCH LOAN PERCENT
    |---------------------------------------------
    */
    public function getPercent($interest){
        $data = $interest / 100;

        // return
        return $data;
    }

    /*
    |---------------------------------------------
    | GET LOAN BY ID
    |---------------------------------------------
    */
    public function getLoanById($loan_id){
        $loan_data = Loan::where("id", $loan_id)->first();
        if($loan_data !== null){
            // get transaction history
            $transactions = Transaction::where("related_id", $loan_id)->get();
            $trans_box = [];
            if(count($transactions) > 0){
                foreach ($transactions as $el) {
                    # code...
                    $data = [
                        'id'            => $el->id,
                        'user_id'       => $el->user_id,
                        'related_id'    => $el->related_id,
                        'ref_no'        => $el->ref_no,
                        'signature'     => $el->signature,
                        'name'          => $el->name,
                        'type'          => $el->type,
                        'note'          => $el->note,
                        'amount'        => $el->amount,
                        'status'        => $el->status,
                        'created_at'    => $el->created_at->diffForHumans(),
                        'updated_at'    => $el->updated_at->diffForHumans()
                    ];

                    array_push($trans_box, $data);
                }
            }

            $history = [
                'loan'          => $loan_data,
                'transactions'  => $trans_box
            ];
        }

        // return
        return $history;
    }

    /*
    |---------------------------------------------
    | APPLY NEW LOAN
    |---------------------------------------------
    */
    public function applyExistingLoan($payload){
        $mutable  = Carbon::now();
        $duration = $mutable->addDays($payload->duration);

        // get formular
        $percent    = $this->getPercent($payload->interest);

        $interest   = $percent * $payload->amount;
        $total      = $payload->amount + $interest;

        $new_loan           = new Loan();
        $new_loan->user_id  = $payload->user_id;
        $new_loan->balance  = $payload->amount;
        $new_loan->status   = "pending";
        $new_loan->total    = $total;
        $new_loan->interest = $interest;
        $new_loan->duration = $duration;
        if($new_loan->save()){
            $data = [
                'status'    => 'success',
                'message'   => 'New loan account created successfully!',
            ];
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'Error creating loan account!'
            ];
        }

        // return
        return $data;
    }

    /*
    |---------------------------------------------
    | GET ALL PENDING LOANS
    |---------------------------------------------
    */
    public function loadPendingLoans(){
        $pending_loans = Loan::where("status", "pending")->orderBy("id", "DESC")->get();
        if(count($pending_loans) >  0){
            $loan_box = [];
            foreach ($pending_loans as $pl) {

                // get bioData
                $bio_data   = new BioData();
                $bio_info   = $bio_data->getUserById($pl->user_id);
                $details    = User::where("id", $pl->user_id)->first();

                $data = [
                    'id'        => $pl->id,
                    'user_id'   => $pl->user_id,
                    'status'    => $pl->status,
                    'balance'   => $pl->balance,
                    'total'     => $pl->total,
                    'interest'  => $pl->interest,
                    'duration'  => $pl->duration,
                    'details'   => $details,
                    'bio_info'  => $bio_info,
                    'description' => "Loan Account",
                    'date'      => $pl->created_at->diffForHumans()
                ];
                // push
                array_push($loan_box, $data);
            }
        }else{
            $loan_box = [];
        }

        // return
        return $loan_box;
    }

    /*
    |---------------------------------------------
    | GET ALL USER LOANS
    |---------------------------------------------
    */
    public function getAllUserLoans($user_id){
        $all_loans = Loan::where("user_id", $user_id)->get();

        // return
        return $all_loans;
    }

    /*
    |---------------------------------------------
    | GET ALL APPROVED LOANS
    |---------------------------------------------
    */
    public function loadApprovedLoans(){
        $approve_loans = Loan::where("status", "approved")->orderBy("id", "DESC")->get();
        if(count($approve_loans) >  0){
            $loan_box = [];
            foreach ($approve_loans as $pl) {

                // get bioData
                $bio_data   = new BioData();
                $bio_info   = $bio_data->getUserById($pl->user_id);
                $details    = User::where("id", $pl->user_id)->first();

                $data = [
                    'id'        => $pl->id,
                    'user_id'   => $pl->user_id,
                    'status'    => $pl->status,
                    'balance'   => $pl->balance,
                    'total'     => $pl->total,
                    'interest'  => $pl->interest,
                    'duration'  => $pl->duration,
                    'details'   => $details,
                    'bio_info'  => $bio_info,
                    'description' => "Loan Account",
                    'date'      => $pl->created_at->diffForHumans()
                ];
                // push
                array_push($loan_box, $data);
            }
        }else{
            $loan_box = [];
        }

        // return
        return $loan_box;
    }

    /*
    |---------------------------------------------
    | GET ALL ACTIVE & RUNNING LOANS
    |---------------------------------------------
    */
    public function loadAllLoans(){
        $active_loans = Loan::orderBy("id", "DESC")->get();
        if(count($active_loans) >  0){
            $loan_box = [];
            foreach ($active_loans as $pl) {

                // get bioData
                $bio_data   = new BioData();
                $bio_info   = $bio_data->getUserById($pl->user_id);
                $details    = User::where("id", $pl->user_id)->first();

                $data = [
                    'id'        => $pl->id,
                    'user_id'   => $pl->user_id,
                    'status'    => $pl->status,
                    'balance'   => $pl->balance,
                    'total'     => $pl->total,
                    'interest'  => $pl->interest,
                    'duration'  => $pl->duration,
                    'details'   => $details,
                    'bio_info'  => $bio_info,
                    'description' => "Loan Account",
                    'date'      => $pl->created_at->diffForHumans()
                ];
                // push
                array_push($loan_box, $data);
            }
        }else{
            $loan_box = [];
        }

        // return
        return $loan_box;
    }


    /*
    |---------------------------------------------
    | LOAD ALL SETTLED LOANS
    |---------------------------------------------
    */
    public function loadSettledLoans(){
        $active_loans = Loan::where('status', 'settled')->orderBy("id", "DESC")->get();
        if(count($active_loans) >  0){
            $loan_box = [];
            foreach ($active_loans as $pl) {

                // get bioData
                $bio_data   = new BioData();
                $bio_info   = $bio_data->getUserById($pl->user_id);
                $details    = User::where("id", $pl->user_id)->first();

                $data = [
                    'id'        => $pl->id,
                    'user_id'   => $pl->user_id,
                    'status'    => $pl->status,
                    'balance'   => $pl->balance,
                    'total'     => $pl->total,
                    'interest'  => $pl->interest,
                    'duration'  => $pl->duration,
                    'details'   => $details,
                    'bio_info'  => $bio_info,
                    'description' => "Loan Account",
                    'date'      => $pl->created_at->diffForHumans()
                ];
                // push
                array_push($loan_box, $data);
            }
        }else{
            $loan_box = [];
        }

        // return
        return $loan_box;
    }

    /*
    |---------------------------------------------
    | CREATE ACCOUNT FROM EXCEL
    |---------------------------------------------
    */
    public function createLoanFromExcel($payload, $user_id){
        $duration   = $payload['duration'];
        $interest   = $payload['interest'];
        $balance    = $payload['balance'];
        $amount     = 0;

        if($interest == null){
            $interest = 0;
        }else{
            $interest = $payload['interest'];
        }

        if($balance == null){
            $balance = 0;
        }else{
            $balance = $payload['balance'];
        }

        if($duration == null){
            $duration = 0;
        }else{
            $duration = $payload['duration'];
        }

        $mutable  = Carbon::now();
        $duration = $mutable->addDays($duration);

        // get formular
        $percent    = $this->getPercent($interest);
        $interest   = $percent * $amount;
        $total      = $amount + $interest;

        // ehck if user already have account
        $account_no = "MC-".rand(000, 999).rand(000, 999).rand(000, 999);
        
        $new_account                = new Loan();
        $new_account->user_id       = $user_id;
        $new_account->status        = "approved";
        $new_account->balance       = $payload['balance'];
        $new_account->total         = $payload['balance'];
        $new_account->interest      = $interest;
        $new_account->duration      = $duration;
        $new_account->save();
    }
}
