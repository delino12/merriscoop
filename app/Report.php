<?php

namespace Merriscoop;

use Illuminate\Database\Eloquent\Model;
use Merriscoop\Loan;
use Merriscoop\Account;
use Merriscoop\BioData;
use Merriscoop\User;

class Report extends Model
{
    /*
    |---------------------------------------------
    | GENERATE REPORTS
    |---------------------------------------------
    */
    public function generateReports($payload){
    	// generate reports
    	if($payload->report_type == "loans"){
    		$data = $this->getLoanReports($payload);
    	}elseif($payload->report_type == "savings"){
    		$data = $this->getSavingsReports($payload);
    	}else{
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> 'No report type found !'
    		];
    	}

    	// return 
    	return $data;
    }

    /*
    |---------------------------------------------
    | GENERATE LOANS REPORTS
    |---------------------------------------------
    */
    public function getLoanReports($payload){
    	$from 	= $payload->start_date;
    	$to 	= $payload->end_date;

    	$loans 	= Loan::whereBetween("created_at", [$from, $to])->get();
    	if(count($loans) > 0){
    		$loan_box = [];
    		foreach ($loans as $pl) {
                $bio_data   = new BioData();
                $bio_info   = $bio_data->getUserById($pl->user_id);
                $details    = User::where("id", $pl->user_id)->first();

                $data = [
                    'id'        => $pl->id,
                    'user_id'   => $pl->user_id,
                    'status'    => $pl->status,
                    'balance'   => $pl->balance,
                    'total'     => $pl->total,
                    'interest'  => $pl->interest,
                    'duration'  => $pl->duration,
                    'details'   => $details,
                    'bio_info'  => $bio_info,
                    'description' => "Loan Account",
                    'date'      => $pl->created_at->toDateTimeString()
                ];
                // push
                array_push($loan_box, $data);
    		}
    	}else{
    		$loan_box = [];
    	}

    	// return 
    	return $loan_box;
    }

    /*
    |---------------------------------------------
    | GENERATE SAVINGS REPORTS
    |---------------------------------------------
    */
    public function getSavingsReports($payload){
    	$from 	= $payload->start_date;
    	$to 	= $payload->end_date;

    	$account = Account::whereBetween("created_at", [$from, $to])->get();
    	if(count($account) > 0){
    		$account_box = [];
    		foreach ($account as $al) {
    			# code...
    			// get bioData
                $bio_data   = new BioData();
                $bio_info   = $bio_data->getUserById($al->user_id);
                $details    = User::where("id", $al->user_id)->first();

                $data = [
                    'id'        => $al->id,
                    'user_id'   => $al->user_id,
                    'status'    => $al->status,
                    'balance'   => $al->balance,
                    'total'     => $al->total,
                    'interest'  => $al->interest,
                    'duration'  => $al->duration,
                    'details'   => $details,
                    'bio_info'  => $bio_info,
                    'description' => "Savings Account",
                    'date'      => $al->created_at->toDateTimeString()
                ];

                // push
                array_push($account_box, $data);
    		}
    	}else{
    		$account_box = [];
    	}

    	// return 
    	return $account_box;
    }
}
