<?php

namespace Merriscoop;

use Illuminate\Database\Eloquent\Model;
use Hash;
use Auth;

class Admin extends Model
{
    /*
    |---------------------------------------------
    | CHECK IF DEFAULT ADMIN ALREADY EXIST
    |---------------------------------------------
    */
    public function checkAlreadyExist(){
    	$check_admin = Admin::where("email", "admin@merriscoop.com")->first();
    	if($check_admin !== null){
    		return true;
    	}else{
    		return false;
    	}
    }

    /*
    |---------------------------------------------
    | ADD DEFAULT ADMIN
    |---------------------------------------------
    */
    public function addDefaultAdmin(){
    	$default_admin 				= new Admin();
    	$default_admin->name 		= "Merris Coop";
    	$default_admin->email 		= "admin@merriscoop.com";
    	$default_admin->password 	= bcrypt("welcome2merriscoop2018");
    	$default_admin->level 		= "alpha";
    	$default_admin->save();
    }

    /*
    |---------------------------------------------
    | CREATE ADMIN
    |---------------------------------------------
    */
    public function addAdmin($request){
        $names      = $request->names;
        $level      = $request->level;
        $email      = $request->email;
        $password   = $request->password;

        // check if user already added 
        $already_exist = Admin::where('email', $email)->first();
        if($already_exist !== null){
            $data = [
                'status'   => 'error',
                'message'  => 'This user already exist !'
            ];
        }else{
            $new_admin              = new Admin();
            $new_admin->name        = $names;
            $new_admin->email       = $email;
            $new_admin->level       = $level;
            $new_admin->password    = bcrypt($password);
            $new_admin->save();

            $data = [
                'status'   => 'success',
                'message'  => $names.' has been added to admin!'
            ];
        }

        // return 
        return $data;
    }

    /*
    |---------------------------------------------
    | LIST ADMIN
    |---------------------------------------------
    */
    public function listAdmin(){
        $all_admin = Admin::orderBy('id', 'DESC')->get();
        if(count($all_admin) > 0){
            $admin_box = [];
            foreach ($all_admin as $admin) {
                # code...
                $data = [
                    'id'    => $admin->id,
                    'names' => $admin->name,
                    'email' => $admin->email,
                    'level' => $admin->level,
                    'date'  => $admin->created_at->diffForHumans()
                ];

                array_push($admin_box, $data);
            }
        }else{
            $admin_box = [];
        }

        // return 
        return $admin_box;
    }

    /*
    |---------------------------------------------
    | CHANGE PASSWORD
    |---------------------------------------------
    */
    public function changeAdminPassword($payload){

        $old_password = $payload->old_password;
        $new_password = $payload->new_password;

        $admin = Admin::where("id", Auth::user()->id)->first();

        if($admin === null){
            $data = [
                'status'    => 'error',
                'message'   => 'Could not find admin user',
            ];
        }else{
            if(Hash::check($old_password, $admin->password)){
                $update_password            = Admin::find($admin->id);
                $update_password->password  = bcrypt($new_password);
                if($update_password->update()){
                    $data = [
                        'status'    => 'success',
                        'message'   => 'Password changed successfully!',
                    ];
                    // logout admin
                    Auth::logout();
                }else{
                    $data = [
                        'status'    => 'error',
                        'message'   => 'Could not update password!',
                    ];
                }
            }else{
                $data = [
                    'status'    => 'error',
                    'message'   => 'Invalid password, Attempt to change password has been recorded!',
                ];
            }
        }

        // return 
        return $data;
    }
}
