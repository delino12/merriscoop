<?php

namespace Merriscoop;

use Illuminate\Database\Eloquent\Model;
use Merriscoop\Mail\NotifyWaitingVisitors;

class WaitList extends Model
{
    /*
    |---------------------------------------------
    | Add to waiting
    |---------------------------------------------
    */
    public function addToWaiting($email){
    	
    	// check if user is already on a waiting list
    	$already_waiting = WaitList::where('email', $email)->first();
    	if($already_waiting !== null){
    		
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> $email.' has already been added to our waiting list',
    		];
    	}else{
    		$waiting_user 			= new WaitList();
	    	$waiting_user->email 	= $email;

	    	if($waiting_user->save()){
	    		$data = [
	    			'status' 	=> 'success',
	    			'message' 	=> $email.' has been added to our waiting list.',
	    		];

	    		\Mail::to($email)->send(new NotifyWaitingVisitors());
	    	}else{
	    		$data = [
	    			'status' 	=> 'error',
	    			'message' 	=> 'Failed to save '.$email.' to our waiting list',
	    		];
	    	}
    	}

    	// return 
    	return $data;
    }	
}
