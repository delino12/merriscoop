<?php

namespace Merriscoop;

use Illuminate\Database\Eloquent\Model;
use Merriscoop\SmsAlert;

class SmsQueue extends Model
{
    /*
    |---------------------------------------------
    | ADD TO SMS QUEUE
    |---------------------------------------------
    */
    public function addToSmsQueue($mobile, $message){
    	// body
    	$new_sms 			= new SmsQueue();
    	$new_sms->status 	= "waiting";
    	$new_sms->phone 	= $mobile;
    	$new_sms->message 	= $message;
    	$new_sms->save();
    }

    /*
    |-----------------------------------------
    | CLEAR WAITING SMS QUEUE
    |-----------------------------------------
    */
    public function processQueueMessage(){
    	// body
    	$all_waiting = SmsQueue::where("status", "waiting")->orWhere("status", "failed")->get();
    	if(count($all_waiting) > 0){
    		foreach ($all_waiting as $wl) {
                if(strlen($wl->phone) > 10 && strlen($wl->phone) < 12){
                    $sms_alert = new SmsAlert();
                    if($sms_alert->sendMessages($wl->phone, $wl->message, "MerrisCoop")){
                        // update status
                        $update_status          = SmsQueue::find($wl->id);
                        $update_status->status  = 'sent';
                        $update_status->update();
                    }else{
                        // update status
                        $update_status          = SmsQueue::find($wl->id);
                        $update_status->status  = 'failed';
                        $update_status->update();
                    }
                }
    		}
    	}
    }
}
