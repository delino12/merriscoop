<?php

namespace Merriscoop;

use Illuminate\Database\Eloquent\Model;

class Formular extends Model
{
    /*
    |---------------------------------------------
    | UPDATE FORMULAR
    |---------------------------------------------
    */
    public function updateLoanFormular($payload){

    	// check if already exist
    	$already_exist = Formular::where('formular_type', 'loans')->first();
    	if($already_exist !== null){
    		$update_formular 				= Formular::find($already_exist->id);
	    	$update_formular->min_amount 	= $payload->min_amount;
	    	$update_formular->max_amount 	= $payload->max_amount;
	    	$update_formular->amount_1 		= $payload->amount_1;
	    	$update_formular->amount_2 		= $payload->amount_2;
	    	$update_formular->amount_3 		= $payload->amount_3;
	    	$update_formular->amount_4 		= $payload->amount_4;
	    	$update_formular->formular_type = $payload->f_type;
	    	if($update_formular->update()){
	    		$data = [
	    			'status' 	=> 'success',
	    			'message' 	=> 'Configuration successful updated!',
	    		];
	    	}else{
	    		$data = [
	    			'status' 	=> 'error',
	    			'message' 	=> 'Failed to update configuration',
	    		];
	    	}
    	}else{
    		$update_formular 				= new Formular();
	    	$update_formular->min_amount 	= $payload->min_amount;
	    	$update_formular->max_amount 	= $payload->max_amount;
	    	$update_formular->amount_1 		= $payload->amount_1;
	    	$update_formular->amount_2 		= $payload->amount_2;
	    	$update_formular->amount_3 		= $payload->amount_3;
	    	$update_formular->amount_4 		= $payload->amount_4;
	    	$update_formular->formular_type = $payload->f_type;
	    	if($update_formular->save()){
	    		$data = [
	    			'status' 	=> 'success',
	    			'message' 	=> 'Configuration successful updated!',
	    		];
	    	}else{
	    		$data = [
	    			'status' 	=> 'error',
	    			'message' 	=> 'Failed to update configuration',
	    		];
	    	}
    	}

    	// return
    	return $data;
    }

    /*
    |---------------------------------------------
    | UPDATE FORMULAR
    |---------------------------------------------
    */
    public function updateSavingsFormular($payload){

    	// check if already exist
    	$already_exist = Formular::where('formular_type', 'savings')->first();
    	if($already_exist !== null){
    		$update_formular 				= Formular::find($already_exist->id);
	    	$update_formular->min_amount 	= $payload->min_amount;
	    	$update_formular->max_amount 	= $payload->max_amount;
	    	$update_formular->amount_1 		= $payload->amount_1;
	    	$update_formular->amount_2 		= $payload->amount_2;
	    	$update_formular->amount_3 		= $payload->amount_3;
	    	$update_formular->amount_4 		= $payload->amount_4;
	    	$update_formular->formular_type = $payload->f_type;
	    	if($update_formular->update()){
	    		$data = [
	    			'status' 	=> 'success',
	    			'message' 	=> 'Configuration successful updated!',
	    		];
	    	}else{
	    		$data = [
	    			'status' 	=> 'error',
	    			'message' 	=> 'Failed to update configuration',
	    		];
	    	}
    	}else{
    		$update_formular 				= new Formular();
	    	$update_formular->min_amount 	= $payload->min_amount;
	    	$update_formular->max_amount 	= $payload->max_amount;
	    	$update_formular->amount_1 		= $payload->amount_1;
	    	$update_formular->amount_2 		= $payload->amount_2;
	    	$update_formular->amount_3 		= $payload->amount_3;
	    	$update_formular->amount_4 		= $payload->amount_4;
	    	$update_formular->formular_type = $payload->f_type;
	    	if($update_formular->save()){
	    		$data = [
	    			'status' 	=> 'success',
	    			'message' 	=> 'Configuration successful updated!',
	    		];
	    	}else{
	    		$data = [
	    			'status' 	=> 'error',
	    			'message' 	=> 'Failed to update configuration',
	    		];
	    	}
    	}

    	// return
    	return $data;
    }

    /*
    |---------------------------------------------
    | GET FORMULAR
    |---------------------------------------------
    */
    public function getFormular(){
    	$formular = Formular::all();

    	// return 
    	return $formular;
    }
}
