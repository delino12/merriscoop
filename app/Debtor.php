<?php

namespace Merriscoop;

use Illuminate\Database\Eloquent\Model;
use Merriscoop\Loan;

class Debtor extends Model
{
    /*
    |---------------------------------------------
    | ADD A NEW DEBTOR
    |---------------------------------------------
    */
    public function createNewDebtor($payload){
    	$new_debtor 			= new Debtor();
    	$new_debtor->user_id 	= $payload->user_id;
    	$new_debtor->amount 	= $payload->amount;
    	$new_debtor->status 	= $payload->status;
    	$new_debtor->save()
    }
}
