<?php

namespace Merriscoop;

use Illuminate\Database\Eloquent\Model;
use Merriscoop\Loan;
use Merriscoop\BioData;
use Merriscoop\User;
use Merriscoop\SmsAlert;
use Merriscoop\SmsQueue;
use Auth;

class Transaction extends Model
{
    /*
    |---------------------------------------------
    | LOG NEW TRANSACTIONS
    |---------------------------------------------
    */
    public function recordNewTransaction($payload, $user_id, $relate_id, $signature){
    	$new_transaction 				= new Transaction();
		$new_transaction->user_id 		= $user_id;
		$new_transaction->related_id 	= $related_id;
		$new_transaction->ref_no 		= "MCX-".time();
		$new_transaction->signature 	= $signature;
		$new_transaction->name 			= $payload->name;
		$new_transaction->type 			= $payload->type;
		$new_transaction->note 			= $payload->note;
		$new_transaction->amount 		= $payload->amount;
		$new_transaction->status 		= $payload->status;
		if($new_transaction->save()){

            $sms_amount = number_format($amount, 2);
            // send quick alert
            $msg = 'You have received a Credit Alert of NGN'.$sms_amount.', visit www.merriscoop.com Today to start growing your savings';

            if(env("APP_DEVELOPMENT") == false){
                $this->notifySmsAlert($user_id, $msg);
            }

			return true;
		}else{
			return false;
		}
    }

    /*
    |---------------------------------------------
    | FETCH TRANSACTION DATA
    |---------------------------------------------
    */
    public function getUserById($user_id){
        $trans_data = Transaction::where("user_id", $user_id)->first();

        // return
        return $trans_data;
    }

    /*
    |---------------------------------------------
    | LOAN TRANSACTION
    |---------------------------------------------
    */
    public function saveLoanTransaction($amount, $user_id, $loan_id){
        $new_transaction                = new Transaction();
        $new_transaction->user_id       = $user_id;
        $new_transaction->related_id    = $loan_id;
        $new_transaction->ref_no        = "MCX-".time();
        $new_transaction->signature     = "MerrisCoop";
        $new_transaction->name          = "Loan repayment";
        $new_transaction->type          = "loan";
        $new_transaction->note          = "Loan Deposit";
        $new_transaction->amount        = $amount;
        $new_transaction->status        = "success";
        if($new_transaction->save()){

            $sms_amount = number_format($amount, 2);
            // send quick alert
            $msg = 'Credit Alert NGN'.$sms_amount.' of loan re-payment was successful, visit www.merriscoop.com Today to access to quick Loans';

            if(env("APP_DEVELOPMENT") == false){
                $this->notifySmsAlert($user_id, $msg);
            }

            return true;
        }else{
            return false;
        }
    }


    /*
    |---------------------------------------------
    | LOAN TRANSACTION
    |---------------------------------------------
    */
    public function saveAccountTransaction($amount, $user_id, $account_id){
        $new_transaction                = new Transaction();
        $new_transaction->user_id       = $user_id;
        $new_transaction->related_id    = $account_id;
        $new_transaction->ref_no        = "MCX-".time();
        $new_transaction->signature     = "MerrisCoop";
        $new_transaction->name          = "Saving Deposit";
        $new_transaction->type          = "savings";
        $new_transaction->note          = "Quick Topup Deposit";
        $new_transaction->amount        = $amount;
        $new_transaction->status        = "success";
        if($new_transaction->save()){

            $sms_amount = number_format($amount, 2);
            
            // send quick alert
            $msg = 'Credit Alert NGN'.$sms_amount.' of quick deposit was successful, visit www.merriscoop.com Today to access to quick Loans';
            // $this->notifySmsAlert($user_id, $msg);
            if(env("APP_DEVELOPMENT") == false){
                $this->notifySmsAlert($user_id, $msg);
            }

            return true;
        }else{
            return false;
        }
    }

    /*
    |---------------------------------------------
    | LOAN TRANSACTION
    |---------------------------------------------
    */
    public function saveWithdrawFromAccountTransaction($amount, $user_id, $account_id){
        $new_transaction                = new Transaction();
        $new_transaction->user_id       = $user_id;
        $new_transaction->related_id    = $account_id;
        $new_transaction->ref_no        = "MCX-".time();
        $new_transaction->signature     = "MerrisCoop";
        $new_transaction->name          = "Instant Withdraw";
        $new_transaction->type          = "savings";
        $new_transaction->note          = "Instant Withdraw";
        $new_transaction->amount        = $amount;
        $new_transaction->status        = "success";
        if($new_transaction->save()){
            $account_info   = Account::where("user_id", $user_id)->first();
            $balance        = $account_info->balance;

            $sms_balance    = number_format($balance, 2);
            $sms_amount     = number_format($amount, 2);

            // send quick alert
            $msg = 'Debit Alert NGN'.$sms_amount.' instant withdraw was successful, Account balance is '.$sms_balance;
            if(env("APP_DEVELOPMENT") == false){
                $this->notifySmsAlert($user_id, $msg);
            }
            return true;
        }else{
            return false;
        }
    }

    /*
    |---------------------------------------------
    | GET ONE TRANSACTION
    |---------------------------------------------
    */
    public function getOneTransaction($id){
        $transaction    = Transaction::where("id", $id)->first();
        if($transaction !== null){
            $user       = User::where("id", $transaction->user_id)->first();
            $biodata    = BioData::where("user_id", $transaction->user_id)->first();
            $loan       = Loan::where("id", $transaction->related_id)->first();
            $account    = Account::where("id", $transaction->related_id)->first();
            
            $data = [
                'user'          => $user,
                'loan'          => $loan,
                'account'       => $account,
                'transaction'   => $transaction,
                'details'       => $biodata,
            ];
        }else{
            $data = [];
        }

        // return 
        return $data;
    }

    /*
    |---------------------------------------------
    | GET All TRANSACTION
    |---------------------------------------------
    */
    public function getAllTransaction(){
        $all_transactions = Transaction::orderBy("id", "DESC")->get();
        if(count($all_transactions) > 0){
            $trans_box = [];
            foreach ($all_transactions as $transaction) {
                # code...
                $user       = User::where("id", $transaction->user_id)->first();
                $biodata    = BioData::where("user_id", $transaction->user_id)->first();
                $loan       = Loan::where("id", $transaction->related_id)->first();
                $account    = Account::where("id", $transaction->related_id)->first();
                
                $data = [
                    'account_type'  => $user->account_type,
                    'loan'          => $loan,
                    'account'       => $account,
                    'transaction'   => $transaction,
                    'details'       => $biodata,
                ];

                array_push($trans_box, $data);
            }
        }else{
            $trans_box = [];
        }

        // return 
        return $trans_box;
    }


    /*
    |---------------------------------------------
    | NOTIFY SMS ALERT
    |---------------------------------------------
    */
    public function notifySmsAlert($user_id, $msg){
        $bio_data = new BioData();
        $bio_info = $bio_data->getUserById($user_id);

        $sender = "MerrisCoop";
        $phone  = $bio_info->phone;

        // $sms_alert = new SmsAlert();
        // $sms_alert->sendMessages($phone, $msg, $sender);

        $sms_queue = new SmsQueue();
        $sms_queue->addToSmsQueue($phone, $msg);
    }
}
