<?php

namespace Merriscoop;

use Illuminate\Database\Eloquent\Model;

class BioData extends Model
{
    /*
    |---------------------------------------------
    | CREATE USER BIO INFORMATION
    |---------------------------------------------
    */
    public function createUserBioData($payload, $user_id){
    	$bio_data 					= new BioData();
    	$bio_data->user_id 			= $user_id;
		$bio_data->firstname 		= $payload->firstname;
		$bio_data->lastname 		= $payload->lastname;
		$bio_data->phone 			= $payload->phone;
		$bio_data->address 			= $payload->address;
		$bio_data->country 			= $payload->country;
		$bio_data->state 			= $payload->state;
		$bio_data->bvn 				= $payload->bvn;
		$bio_data->passport_id 		= $payload->idcard;
		$bio_data->fingerprint 		= $payload->fingerprint;
		$bio_data->nok_firstname 	= $payload->nok_firstname;
		$bio_data->nok_lastname 	= $payload->nok_lastname;
		$bio_data->nok_phone 		= $payload->nok_phone;
		$bio_data->nok_address 		= $payload->nok_address;
		$bio_data->gad_firstname 	= $payload->gad_firstname;
		$bio_data->gad_lastname 	= $payload->gad_lastname;
		$bio_data->gad_address 		= $payload->gad_address;
		$bio_data->occupation 		= $payload->occupation;
		$bio_data->avatar 			= $payload->avatar;
		$bio_data->status 			= 'active';
		if($bio_data->save()){
			return true;
		}else{
			return false;
		}
    }

    /*
    |---------------------------------------------
    | FETCH USER DATA
    |---------------------------------------------
    */
    public function getUserById($user_id){
    	$bio_data = BioData::where("user_id", $user_id)->first();

    	// return
    	return $bio_data;
    }

    /*
    |---------------------------------------------
    | CREATE USER BIO DATA FROM EXCEL
    |---------------------------------------------
    */
    public function createBiodataFromExcel($payload, $user_id){
    	if($payload['state'] == null){
    		$state = 'None';
    	}else{
    		$state = $payload['state'];
    	}

    	if($payload['country'] == null){
    		$country = 'None';
    	}else{
    		$country = $payload['country'];
    	}

    	if($payload['mobile'] == null){
    		$mobile = 'None';
    	}else{
    		$mobile = $payload['mobile'];
    	}

    	if($payload['occupation'] == null){
    		$occupation = 'None';
    	}else{
    		$occupation = $payload['occupation'];
    	}

    	if($payload['address_1'] == null){
    		$address_1 = 'None';
    	}else{
    		$address_1 = $payload['address_1'];
    	}

    	if($payload['address_2'] == null){
    		$address_2 = 'None';
    	}else{
    		$address_2 = $payload['address_2'];
    	}

    	$bio_data 					= new BioData();
    	$bio_data->user_id 			= $user_id;
		$bio_data->firstname 		= $payload['first_name'];
		$bio_data->lastname 		= $payload['last_name'];
		$bio_data->phone 			= '0'.$mobile;
		$bio_data->address 			= $address_1.' Address 2: '.$address_2;
		$bio_data->country 			= $country;
		$bio_data->state 			= $state;
		$bio_data->bvn 				= "None";
		$bio_data->passport_id 		= "None";
		$bio_data->fingerprint 		= "None";
		$bio_data->nok_firstname 	= "None";
		$bio_data->nok_lastname 	= "None";
		$bio_data->nok_phone 		= "None";
		$bio_data->nok_address 		= "None";
		$bio_data->gad_firstname 	= "None";
		$bio_data->gad_lastname 	= "None";
		$bio_data->gad_address 		= "None";
		$bio_data->occupation 		= $occupation;
		$bio_data->avatar 			= "None";
		$bio_data->status 			= 'active';
		$bio_data->save();
    }
}
