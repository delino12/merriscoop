<?php

namespace Merriscoop;

use Illuminate\Database\Eloquent\Model;
use Merriscoop\Ajo;

class AjoBook extends Model
{
    /*
    |---------------------------------------------
    | ADD NEW AJO BOOK
    |---------------------------------------------
    */
    public function createNew($payload){
    	if($this->alreadyExist($payload)){
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> $payload->book_name. ' Already created !',
    		];
    	}else{
    		$new_book			= new AjoBook();
    		$new_book->name 	= $payload->book_name;
    		$new_book->ajo_type = $payload->book_type;
    		$new_book->amount 	= $payload->amount;
    		$new_book->quota 	= $payload->quota;
    		$new_book->status 	= "active";
    		if($new_book->save()){
    			$data = [
    				'status' 	=> 'success',
    				'message' 	=> $payload->book_name. ' Created !'
    			];
			}else{
				$data = [
					'status' 	=> 'error',
					'message' 	=> 'Failed to create new ajo book ',
				];
			}
    	}

    	// return
    	return $data;
    }


    /*
    |---------------------------------------------
    | GET ALL AJO BOOK
    |---------------------------------------------
    */
    public function fetchAllAjoBook(){
    	$all_book = AjoBook::orderBy("id", "DESC")->get();
    	if(count($all_book) > 0){
    		$book_box = [];
    		foreach ($all_book as $bl) {
    			# code...
    			$ajo 		= new Ajo();
    			$members 	= count($ajo->getMembers($bl->id));

    			$data = [
    				'id' 		=> $bl->id,
    				'name' 		=> $bl->name,
    				'ajo_type' 	=> ucwords($bl->ajo_type),
    				'members' 	=> number_format($members),
    				'amount' 	=> number_format($bl->amount, 2),
    				'turnover'  => number_format($members * $bl->amount),
    				'last_seen' => $bl->created_at->diffForHumans(),
    				'date'      => $bl->created_at,
    			];

    			array_push($book_box, $data);
    		}
    	}else{
    		$book_box = [];
    	}

    	return $book_box;
    }


    /*
    |---------------------------------------------
    | CHECK IF SET ALREADY EXIST
    |---------------------------------------------
    */
    public function alreadyExist($payload){
    	$ajo_book = AjoBook::where('name', $payload->book_name)->first();
    	if($ajo_book === null){
    		return false;
    	}else{
    		return true;
    	}
    }
}
