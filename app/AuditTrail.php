<?php

namespace Merriscoop;

use Illuminate\Database\Eloquent\Model;
use Merriscoop\Admin;

class AuditTrail extends Model
{
    /*
    |---------------------------------------------
    | All Trails
    |---------------------------------------------
    */
    public function getTrails(){
    	$audit_trails = AuditTrail::orderBy('id', 'DESC')->get();
    	if(count($audit_trails) > 0){
    		$trail_box = [];
    		foreach ($audit_trails as $al) {

    			$admin = Admin::where("id", $al->admin_id)->first();

    			$data = [
    				'admin' 		=> $admin,
    				'process' 		=> $al->process,
    				'description' 	=> $al->description,
    				'last_updated' 	=> $al->created_at->diffForHumans() 
    			];

    			array_push($trail_box, $data);
    		}
    	}else{
    		$trail_box = [];
    	}

    	// return
    	return $trail_box;
    }

    /*
    |---------------------------------------------
    | Log Trails
    |---------------------------------------------
    */
    public function logTrail($admin_id, $process, $description){
    	$new_trail 				= new AuditTrail();
    	$new_trail->admin_id 	= $admin_id;
    	$new_trail->process 	= $process;
    	$new_trail->description = $description;
    	$new_trail->status 		= "active";
    	$new_trail->save();
    }		
}
