<?php

namespace Merriscoop;

use Illuminate\Database\Eloquent\Model;
use Merriscoop\BioData;
use Merriscoop\User;

class Ajo extends Model
{
    /*
    |---------------------------------------------
    | GET AJO USING BOOK ID
    |---------------------------------------------
    */
    public function getMembers($book_id){
    	$ajo = Ajo::where("ajobook_id", $book_id)->get();

    	// return 
    	return $ajo;
    }

    /*
    |---------------------------------------------
    | ADD MEMBER
    |---------------------------------------------
    */
    public function addMember($payload){

        if($this->alreadyExist($payload)){
            $data = [
                'status'    => 'error',
                'message'   => 'Member already exist',
            ];
        }else{
            $new_ajo                    = new Ajo();
            $new_ajo->user_id           = $payload->user_id;
            $new_ajo->ajobook_id        = $payload->ajobook_id;
            $new_ajo->status            = $payload->status;
            $new_ajo->amount            = $payload->amount;
            $new_ajo->charge            = $payload->charge;
            $new_ajo->next_payout_date  = $payload->next_payout_date;
            $new_ajo->next_payout_user  = $payload->next_payout_user;
            $new_ajo->duration          = $payload->duration;
            $new_ajo->turn_collected    = 0;
            if($new_ajo->save()){
                $data = [
                    'status'    => 'success',
                    'message'   => 'Member added successfully!',
                ];
            }else{
                $data = [
                    'status'    => 'error',
                    'message'   => 'Failed to add member!',
                ];
            }
        }
    
        // return
        return $data;
    }

    /*
    |---------------------------------------------
    | GET MEMBERS
    |---------------------------------------------
    */
    public function getAjoMembers($book_id){
        $ajo = Ajo::where("ajobook_id", $book_id)->get();
        if(count($ajo) > 0){
            $members_box = [];
            foreach ($ajo as $aj) {
                // get bioData
                $bio_data   = new BioData();
                $bio_info   = $bio_data->getUserById($aj->user_id);
                $details    = User::where("id", $aj->user_id)->first();

                $data = [
                    'id'                => $aj->id,
                    'user_id'           => $aj->user_id,
                    'ajobook_id'        => $aj->ajobook_id,
                    'status'            => $aj->status,
                    'amount'            => number_format($aj->amount, 2),
                    'charge'            => number_format($aj->charge, 2),
                    'next_payout_date'  => $aj->next_payout_date,
                    'next_payout_user'  => $aj->next_payout_user,
                    'duration'          => $aj->duration,
                    'turn_collected'    => number_format($aj->turn_collected),
                    'details'           => $details
                ];
                // push
                array_push($members_box, $data);
            }
        }else{
            $members_box = [];
        }

        // return 
        return $members_box;
    }


    /*
    |---------------------------------------------
    | CHECK ALREADY EXITS
    |---------------------------------------------
    */
    public function alreadyExist($payload){
        $ajo_book = Ajo::where('user_id', $payload->user_id)->first();
        if($ajo_book === null){
            return false;
        }else{
            return true;
        }   
    }
}
