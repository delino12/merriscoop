<?php

namespace Merriscoop;

use Illuminate\Database\Eloquent\Model;
use Merriscoop\Account;
use Merriscoop\Loan;
use Merriscoop\BioData;
use Merriscoop\Transaction;
use Merriscoop\Investment;
use Merriscoop\SmsAlert;
use Merriscoop\AuditTrail;
use Auth;

class User extends Model
{
    // procted user password from results set
    protected $hidden = ['password'];

    /*
    |---------------------------------------------
    | CREATE NEW LOAN USER
    |---------------------------------------------
    */
    public function addNewDebtor($payload){
        $name = $payload->firstname.' '.$payload->lastname;
        if($this->verifyAlreadyCreated($payload)){
            $data = [
                'status' => 'error',
                'message' => $name.' already exist please verify & validate duplicate records! ',
            ];
        }else{
            $new_user               = new User();
            $new_user->name         = $name;
            $new_user->email        = $payload->email;
            $new_user->password     = bcrypt('password');
            $new_user->account_type = $payload->account_type;
            if($new_user->save()){

                $user_id = $new_user->id;

                // create user account
                $new_account = new Loan();
                if($new_account->createLoanAccount($payload, $user_id)){

                    // create bio data
                    $add_biodata = new BioData();
                    if($add_biodata->createUserBioData($payload, $user_id)){
                        $data = [
                            'status'    => 'success',
                            'message'   => 'Account has been created successfully!',
                        ];

                        $this->notifyAccountCreation($user_id, "Welcome to MerrisCoop, Your account has been successfully created. Thanks for choosing MerrisCoop.");
                    }else{
                        $data = [
                            'status' => 'error',
                            'message' => 'Fail to create user bio data information',
                        ];
                    }
                }else{
                    $data = [
                        'status'    => 'error',
                        'message'   => 'Information was created but failed to create loan account',
                    ];
                }

            }else{
                $data = [
                    'status'    => 'error',
                    'message'   => 'Could not create user account',
                ];
            }
        }
        
        // return 
        return $data;
    }

    /*
    |---------------------------------------------
    | CREATE NEW INVESTMENT USER
    |---------------------------------------------
    */
    public function addNewCreditor($payload){
        $name = $payload->firstname.' '.$payload->lastname;
        if($this->verifyAlreadyCreated($payload)){
            $data = [
                'status' => 'error',
                'message' => $name.' already exist please verify & validate duplicate records! ',
            ];
        }else{
            $new_user               = new User();
            $new_user->name         = $payload->firstname.' '.$payload->lastname;
            $new_user->email        = $payload->email;
            $new_user->password     = bcrypt('password');
            $new_user->account_type = $payload->account_type;
            if($new_user->save()){

                // get latest using password encrypted
                $user_id = $new_user->id;

                // create user account
                $new_account = new Account();
                if($new_account->createUserAccount($payload, $user_id)){
                    // create bio data
                    $add_biodata = new BioData();
                    if($add_biodata->createUserBioData($payload, $user_id)){
                        $data = [
                            'status'    => 'success',
                            'message'   => 'Account has been created successfully!',
                        ];

                        $this->notifyAccountCreation($user_id, "Welcome to MerrisCoop, Your account has been successfully created. Thanks for choosing MerrisCoop.");
                    }else{
                        $data = [
                            'status'  => 'error',
                            'message' => 'Fail to create user bio data information',
                        ];
                    }
                }else{
                    $data = [
                        'status'    => 'error',
                        'message'   => 'Information was created but failed to create savings account',
                    ];
                }

            }else{
                $data = [
                    'status'    => 'error',
                    'message'   => 'Could not create user account',
                ];
            }
        }

        // return 
        return $data;
    }

    /*
    |---------------------------------------------
    | ADD NEW SAVINGS
    |---------------------------------------------
    */
    public function addSavings($request){

        $user_id    = $request->user_id;
        $amount     = $request->amount;
        $interest   = $request->interest;
        $duration   = $request->duration;
        $naration   = $request->naration;

        // create user account
        $new_account = new Account();
        if($new_account->createUserAccount($payload, $user_id)){
            $data = [
                'status'    => 'success',
                'message'   => $amount.' deposit was  successfully',
            ];

            $this->logTrail(Auth::guard("admin")->user()->id, 'Initiate Deposit', 'Savings Deposit was successful!');
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'failed to deposit new savings',
            ];
        }

        // return
        return $data;
    }

    /*
    |---------------------------------------------
    | VERIFY ALREADY CREATED
    |---------------------------------------------
    */
    public function verifyAlreadyCreated($payload){
        $already_exist = BioData::where([
            ['firstname', $payload->firstname],
            ['lastname', $payload->lastname],
            ['phone', $payload->phone],
        ])->first();

        if($already_exist !== null){
            return true;
        }else{
            return false;
        }
    }

    /*
    |---------------------------------------------
    | GET ALL APP USERS
    |---------------------------------------------
    */
    public function getAllUsers(){
        $users = User::orderBy('id', "DESC")->get();
        $users_box = [];
        if(count($users) >  0){
            foreach ($users as $user) {
                // get bioData
                $bio_data   = new BioData();
                $bio_info   = $bio_data->getUserById($user->id);

                // get loan details
                $loan_data  = new Loan();
                $loan_info  = $loan_data->getUserById($user->id);
                
                // get account details
                $account_data = new Account();
                $account_info = $account_data->getUserById($user->id);

                if($user->account_type == "savings"){
                    $account = $account_info;
                    $desc    = "Saving Account";
                }elseif($user->account_type == "loans"){
                    $account = $loan_info;
                    $desc    = "Loan Account";
                }

                // user transactions
                $data = [
                    'id'            => $user->id,
                    'name'          => ucfirst($user->name),
                    'type'          => $user->account_type,
                    'details'       => $bio_info,
                    'account'       => $account,
                    'description'   => $desc,
                    'transactions'  => "",
                    'account_info'  => $account_info, 
                    'date'          => $user->created_at->diffForHumans()
                ];

                array_push($users_box, $data);
            }
        }

        // return
        return $users_box;
    }

    /*
    |---------------------------------------------
    | GET ALL APP USERS
    |---------------------------------------------
    */
    public function searchUser($keyword){
        $keyword = '%'.$keyword.'%';
        $users = User::where('name', 'LIKE', $keyword)->get();
        $users_box = [];
        if(count($users) >  0){
            foreach ($users as $user) {
                // get bioData
                $bio_data   = new BioData();
                $bio_info   = $bio_data->getUserById($user->id);

                // get loan details
                $loans = Loan::where([['status', 'approved'], ['user_id', $user->id]])->first();
                
                // user transactions
                $data = [
                    'id'            => $user->id,
                    'name'          => ucfirst($user->name),
                    'type'          => $user->account_type,
                    'details'       => $bio_info,
                    'account'       => $loans,
                    'date'          => $user->created_at->diffForHumans()
                ];

                if($user->account_type == 'loans'){
                    array_push($users_box, $data);
                }
            }
        }

        // return
        return $users_box;
    }

    /*
    |---------------------------------------------
    | GET ALL APP USERS SAVERS
    |---------------------------------------------
    */
    public function searchSavingsUser($keyword){
        $keyword = '%'.$keyword.'%';
        $users = User::where('name', 'LIKE', $keyword)->get();
        $users_box = [];
        if(count($users) >  0){
            foreach ($users as $user) {
                // get bioData
                $bio_data   = new BioData();
                $bio_info   = $bio_data->getUserById($user->id);

                // get loan details
                $loans = Account::where('user_id', $user->id)->first();
                
                // user transactions
                $data = [
                    'id'            => $user->id,
                    'name'          => ucfirst($user->name),
                    'type'          => $user->account_type,
                    'details'       => $bio_info,
                    'account'       => $loans,
                    'date'          => $user->created_at->diffForHumans()
                ];

                if($user->account_type == 'savings'){
                    array_push($users_box, $data);
                }
            }
        }

        // return
        return $users_box;
    }

    /*
    |---------------------------------------------
    | GET ALL SAVINGS USERS
    |---------------------------------------------
    */
    public function loadAllSavingsUsers(){
        $users = User::where("account_type", "savings")->orderBy('id', "DESC")->get();
        $users_box = [];
        if(count($users) >  0){
            foreach ($users as $user) {
                // get bioData
                $bio_data   = new BioData();
                $bio_info   = $bio_data->getUserById($user->id);

                // get loan details
                $loan_data  = new Loan();
                $loan_info  = $loan_data->getUserById($user->id);
                
                // get account details
                $account_data = new Account();
                $account_info = $account_data->getUserById($user->id);

                if($user->account_type == "savings"){
                    $account = $account_info;
                    $desc    = "Saving Account";
                }elseif($user->account_type == "loans"){
                    $account = $loan_info;
                    $desc    = "Loan Account";
                }

                // user transactions
                $data = [
                    'id'            => $user->id,
                    'name'          => ucfirst($user->name),
                    'type'          => $user->account_type,
                    'details'       => $bio_info,
                    'account'       => $account,
                    'description'   => $desc,
                    'transactions'  => "",
                    'account_info'  => $account_info, 
                    'date'          => $user->created_at->diffForHumans()
                ];

                array_push($users_box, $data);
            }
        }

        // return
        return $users_box;

    }

    /*
    |---------------------------------------------
    | GET SINGLE USER INFORMATION
    |---------------------------------------------
    */
    public function getSingleUser($id){
        $user = User::where('id', $id)->first();
        if($user !== null){
            // get bioData
            $bio_data   = new BioData();
            $bio_info   = $bio_data->getUserById($user->id);

            // get loan details
            $loan_data  = new Loan();
            $loan_info  = $loan_data->getUserById($user->id);
            
            // get account details
            $account_data = new Account();
            $account_info = $account_data->getUserById($user->id);

            if($user->account_type == "savings"){
                $account = $account_info;
                $desc    = "Saving Account";
            }elseif($user->account_type == "loans"){
                $account = $loan_info;
                $desc    = "Loan Account";
            }

            // user transactions
            $data = [
                'id'            => $user->id,
                'email'         => $user->email,
                'name'          => ucfirst($user->name),
                'type'          => $user->account_type,
                'details'       => $bio_info,
                'account'       => $account,
                'description'   => $desc,
                'transactions'  => "",
                'date'          => $user->created_at->diffForHumans()
            ];

        }else{

            $data = [];
        }

        // return
        return $data;
    }

    /*
    |---------------------------------------------
    | APPROVE USER LOANS
    |---------------------------------------------
    */
    public function approveUserLoan($payload){
        $user_id = $payload->user_id;
        $loan_id = $payload->loan_id;


        // check if user has a pending loans
        $already_applied = Loan::where([["user_id", $user_id], ["status", "approved"]])->first();
        if($already_applied !== null ){
            $data = [
                'status'    => 'error',
                'message'   => 'Customer already have a pending loan...',
            ];
        }else{
            // update and approve account
            $update_loan         = Loan::find($loan_id);
            $update_loan->status = "approved";
            if($update_loan->update()){
                $data = [
                    'status'    => 'success',
                    'message'   => 'Loan Account has been approved',
                ];

                $this->logTrail(Auth::guard("admin")->user()->id, 'Loan Approval', 'Approval was successful!');
            }else{
                $data = [
                    'status'    => 'error',
                    'message'   => 'Failed to approve loan account',
                ];
            }
        }

        // return 
        return $data;
    }

    /*
    |---------------------------------------------
    | MAKE PAYMENT FOR LOANS
    |---------------------------------------------
    */
    public function makePayment($payload){
        $amount     = $payload->amount;
        $user_id    = $payload->user_id;
        $loan_id    = $payload->loan_id;

        $check_loan = Loan::where([['user_id', $user_id], ['id', $loan_id]])->first();
        if($check_loan !== null){
            // check loan amount
            if($check_loan->status == "approved"){
                // check exceeded amount payment
                if($amount > $check_loan->total){
                    $data = [
                        'status'    => 'error',
                        'message'   => 'Amount exceed loan re-payment balance',
                    ];
                }else{
                    // check if payment is final payment
                    $new_balance = $check_loan->total - $amount;
                    if($new_balance == 0){
                        $status = "settled";
                    }else{
                        $status = "approved";
                    }

                    // update loan
                    $update_loan            = Loan::find($loan_id);
                    $update_loan->total     = $new_balance;
                    $update_loan->status    = $status;
                    if($update_loan->update()){
                        $data = [
                            'status'    => 'success',
                            'message'   => 'Loan repayment successful!',
                        ];

                        // record transaction
                        $this->logTransaction($amount, $user_id, $loan_id);
                        $this->logTrail(Auth::guard("admin")->user()->id, 'Loan Re-payment', 'Deposit was successful!');
                    }else{
                        $data = [
                            'status'  => 'error',
                            'message' => 'Failed to completed re-payment, try again!',
                        ];
                    }
                }
            }else{
                $data = [
                    'status'    => 'error',
                    'message'   => 'Loan can not be alter until approved or settled!'
                ];
            }
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'There is no active loan currently!'
            ];
        }

        // return 
        return $data;
    }

    /*
    |---------------------------------------------
    | MAKE PAYMENT FOR SAVINGS
    |---------------------------------------------
    */
    public function makePaymentToSavings($payload){
        $amount     = $payload->amount;
        $user_id    = $payload->user_id;
        $account_id = $payload->account_id;

        $check_account = Account::where([['user_id', $user_id], ['id', $account_id]])->first();
        if($check_account !== null){
            // update loan
            $update_account            = Account::find($account_id);
            $update_account->balance   = $update_account->balance + $amount;
            $update_account->total     = $update_account->total + $amount;
            if($update_account->update()){
                $data = [
                    'status'    => 'success',
                    'message'   => 'Deposit was successful!',
                ];

                // record transaction
                $this->logSavingsTransaction($amount, $user_id, $account_id);
                $this->logTrail(Auth::guard("admin")->user()->id, 'Initiate Deposit', 'Deposit was successful!');
            }else{
                $data = [
                    'status' => 'error',
                    'message' => 'Failed to completed re-payment, try again!',
                ];
            }
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'There is no active loan currently!'
            ];
        }

        // return 
        return $data;
    }

    /*
    |---------------------------------------------
    | MAKE PAYMENT FOR SAVINGS
    |---------------------------------------------
    */
    public function makeWithdrawFromSavings($payload){
        $amount     = $payload->amount;
        $user_id    = $payload->user_id;
        $account_id = $payload->account_id;

        $check_account = Account::where([['user_id', $user_id], ['id', $account_id]])->first();
        if($check_account !== null){

            //check if amount exceed balance
            if($check_account->balance < $amount && $check_account->total < $amount){
                $data = [
                    'status'    => 'error',
                    'message'   => 'Insufficient balance, deposit amount or topup account and try again!',
                ];
            }else{
                // update loan
                $update_account            = Account::find($account_id);
                $update_account->balance   = $update_account->balance - $amount;
                $update_account->total     = $update_account->total - $amount;
                if($update_account->update()){
                    $data = [
                        'status'    => 'success',
                        'message'   => 'Withdraw was successful!',
                    ];

                    // record transaction
                    $this->logWithdrawSavingsTransaction($amount, $user_id, $account_id);
                    $this->logTrail(Auth::guard("admin")->user()->id, 'Initiate Withdrawal', 'Withdraw was successful!');
                }else{
                    $data = [
                        'status'    => 'error',
                        'message'   => 'Failed to completed re-payment, try again!',
                    ];
                }
            }
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'There is no active loan currently!'
            ];
        }

        // return 
        return $data;
    }

    /*
    |---------------------------------------------
    | LOG TRANSACTION FROM LOANS
    |---------------------------------------------
    */
    public function logTransaction($amount, $user_id, $loan_id){
        $transaction = new Transaction();
        $transaction->saveLoanTransaction($amount, $user_id, $loan_id);
    }

    /*
    |---------------------------------------------
    | LOG TRANSACTION FROM SAVINGS
    |---------------------------------------------
    */
    public function logSavingsTransaction($amount, $user_id, $account_id){
        $transaction = new Transaction();
        $transaction->saveAccountTransaction($amount, $user_id, $account_id);
    }

    /*
    |---------------------------------------------
    | LOG WITHDRAW TRANSACTION FROM SAVINGS
    |---------------------------------------------
    */
    public function logWithdrawSavingsTransaction($amount, $user_id, $account_id){
        $transaction = new Transaction();
        $transaction->saveWithdrawFromAccountTransaction($amount, $user_id, $account_id);
    }


    /*
    |---------------------------------------------
    | NOTIFY ACCOUNT CREATION
    |---------------------------------------------
    */
    public function notifyAccountCreation($user_id, $msg){
        $bio_data = new BioData();
        $bio_info = $bio_data->getUserById($user_id);

        $sender = "MerrisCoop";
        $phone  = $bio_info->phone;

        $sms_alert = new SmsAlert();
        $sms_alert->sendMessages($phone, $msg, $sender);
    }

    /*
    |---------------------------------------------
    | UPDATE USER PROFILE
    |---------------------------------------------
    */
    public function updateUserProfile($payload){
        $bio_data = BioData::where("user_id", $payload->user_id)->first();
        if($bio_data !== null){
            $update_data            = BioData::find($bio_data->id);
            $update_data->firstname = $payload->firstname;
            $update_data->lastname  = $payload->lastname;
            $update_data->address   = $payload->address;
            $update_data->phone     = $payload->phone;
            $update_data->update();

            $update_user        = User::find($payload->user_id);
            $update_user->name  = $payload->firstname.' '.$payload->lastname;
            $update_user->update();

            $data = [
                'status'    => 'success',
                'message'   => 'Profile updated successfully!',
            ];

            $this->logTrail(Auth::guard("admin")->user()->id, 'Bio Data Update', 'Update was successful!');
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'Could not find user account updated successfully!',
            ];
        }

        // return
        return $data;
    }

    /*
    |---------------------------------------------
    | LOG TRAIL
    |---------------------------------------------
    */
    public function logTrail($admin_id, $process, $description){
        $new_trail = new AuditTrail();
        $new_trail->logTrail($admin_id, $process, $description);
    }

    /*
    |---------------------------------------------
    | CREATE NEW USER FROM EXCEL REQUEST
    |---------------------------------------------
    */
    public function createAccountFromExcel($payload){
        // verify user names
        if($payload['first_name'] == null && $payload['last_name'] == null){
            return false;
        }else{
            // check already exist
            $names = $payload['first_name'].' '.$payload['last_name'];
            $already_exist = User::where("name", $names)->first();
            if($already_exist !== null){
                return false;
            }else{
                $account_type = $payload['account_type'];
                if($account_type !== null){
                    $account_type   = explode(' ', $account_type);
                    if(count($account_type) > 0){
                        $account_type   = str_replace(" ", "", $account_type[0]);
                    }else{
                        $account_type = "savings";
                    }

                    $new_user               = new User();
                    $new_user->name         = $names;
                    $new_user->email        = "none@domain.com";
                    $new_user->password     = bcrypt('password');
                    $new_user->account_type = strtolower($account_type);
                    $new_user->category     = strtolower($payload['account_type']);
                    if($new_user->save()){
                        
                        // create bio_data
                        $user_id = $new_user->id;

                        // create account
                        $new_account = new Account();
                        $new_account->createAccountFromExcel($payload, $user_id);

                        // create bio data
                        $bio_data = new BioData();
                        $bio_data->createBiodataFromExcel($payload, $user_id);

                        return true;
                    }else{
                        return false;
                    }
                }else{
                    return false;
                }
            }
        }
    }

    /*
    |---------------------------------------------
    | CREATE NEW USER FROM EXCEL REQUEST
    |---------------------------------------------
    */
    public function createLoansFromExcel($payload){
        // verify user names
        if($payload['first_name'] == null && $payload['last_name'] == null){
            return false;
        }else{
            // check already exist
            $names = $payload['first_name'].' '.$payload['last_name'];
            $already_exist = User::where("name", $names)->first();
            if($already_exist !== null){
                return false;
            }else{
                $account_type = $payload['account_type'];
                if($account_type !== null){
                    
                    $new_user               = new User();
                    $new_user->name         = $names;
                    $new_user->email        = "none@domain.com";
                    $new_user->password     = bcrypt('password');
                    $new_user->account_type = strtolower($account_type);
                    $new_user->category     = strtolower($payload['account_type']);
                    if($new_user->save()){
                        
                        // create bio_data
                        $user_id = $new_user->id;

                        // create account
                        $new_account = new Loan();
                        $new_account->createLoanFromExcel($payload, $user_id);

                        // create bio data
                        $bio_data = new BioData();
                        $bio_data->createBiodataFromExcel($payload, $user_id);

                        return true;
                    }else{
                        return false;
                    }
                }else{
                    return false;
                }
            }
        }
    }
}



