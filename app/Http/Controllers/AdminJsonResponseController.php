<?php

namespace Merriscoop\Http\Controllers;

use Illuminate\Http\Request;
use Merriscoop\Formular;
use Merriscoop\Account;
use Merriscoop\User;
use Merriscoop\BioData;
use Merriscoop\Loan;
use Merriscoop\Investment;
use Merriscoop\Transaction;
use Merriscoop\NewsHeadline;
use Merriscoop\Report;
use Merriscoop\AjoBook;
use Merriscoop\Ajo;
use Merriscoop\Admin;
use Merriscoop\AuditTrail;
use Excel;
use Auth;


class AdminJsonResponseController extends Controller
{
    /*
    |---------------------------------------------
    | AUTHENTICATE ADMIN PAGES
    |---------------------------------------------
    */
    public function __construct(){
        $this->middleware("auth:admin")->except('loadAllTransactions', 'getWorkCategory', 'createNewUser', 'createNewDebtor', 'createNewSavings');
    }

    /*
    |---------------------------------------------
    | CREATE NEW DEBTOR ACCOUNT
    |---------------------------------------------
    */
    public function createNewDebtor($request){
        // check for loan formular
        $loan = new Loan();
        if(!empty($request->interest) && !empty($request->duration)){
            $add_user   = new User();
            $data       = $add_user->addNewDebtor($request);
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'Select a interest & duration!'
            ];
        }

        // return
        return $data;
    }

    /*
    |---------------------------------------------
    | CREATE NEW DEBTOR ACCOUNT
    |---------------------------------------------
    */
    public function createNewSavings($request){
        $add_user   = new User();
        $data       = $add_user->addNewCreditor($request);

        // return
        return $data;
    }

    /*
    |---------------------------------------------
    | RE-INVESTMENT
    |---------------------------------------------
    */
    public function addNewSavings(Request $request){
        $add_user   = new User();
        $data       = $add_user->addSavings($request);

        // return
        return $data;
    }


    /*
    |---------------------------------------------
    | LOAD ALL SAVINGS USERS
    |---------------------------------------------
    */
    public function loadSavingsUsers(){
        $load_user  = new User();
        $data       = $load_user->loadAllSavingsUsers();

        // return
        return $data;
    }

    /*
    |---------------------------------------------
    | CREATE NEW USER
    |---------------------------------------------
    */
    public function createNewUser(Request $request){

        if(empty($request->idcard)){
            $data = [
                'status'    => 'error',
                'message'   => 'Client must provide a valid ID card or Drivers licience',
            ];
        }else{
            if($request->account_type == "savings"){
                // create savings account
                $data = $this->createNewSavings($request);

            }elseif($request->account_type == "loans"){
                // create debtors account
                $data = $this->createNewDebtor($request);

            }else{
                // return 
                $data = [
                    'status'    => 'error',
                    'message'   => 'Please select account type',
                ];
            }
        }

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | LOAD ALL USERS
    |---------------------------------------------
    */
    public function loadUsersInformation(){
        $user = new User();
        $data = $user->getAllUsers();

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | LOAD ALL SINGLE USER
    |---------------------------------------------
    */
    public function loadSingleUserInformation($id){
        $user = new User();
        $data = $user->getSingleUser($id);

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | APPROVE CLIENT LOANS
    |---------------------------------------------
    */
    public function approveClientLoan(Request $request){
        
        $user = new User();
        $data = $user->approveUserLoan($request);

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | ACCEPT CLIENT PAYMENT
    |---------------------------------------------
    */
    public function initPayment(Request $request){
        
        $user = new User();
        $data = $user->makePayment($request);

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | GET LOAN HISTORY
    |---------------------------------------------
    */
    public function loadLoanHistory($loan_id){
        $loan = new Loan();
        $data = $loan->getLoanById($loan_id);

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | GET LOAN HISTORY
    |---------------------------------------------
    */
    public function loadAccountHistory($account_id){
        $account    = new Account();
        $data       = $account->getAccountById($account_id);

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | LOAN SINGLE TRANSACTIONS
    |---------------------------------------------
    */
    public function loanSingleTransaction($id){
        $transaction    = new Transaction();
        $data           = $transaction->getOneTransaction($id);

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | UPDATE FORMULAR CONFIG
    |---------------------------------------------
    */
    public function updateConfig(Request $request){
        $formular   = new Formular();

        if($request->f_type == 'savings'){
            $data = $formular->updateSavingsFormular($request);
        }elseif($request->f_type == 'loans'){
            $data = $formular->updateLoanFormular($request);
        }

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | GET FORMULAR CONFIG
    |---------------------------------------------
    */
    public function getConfig(){
        $formular   = new Formular();
        $data       = $formular->getFormular();

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | Loan Dashboard statistic
    |---------------------------------------------
    */
    public function dashboardStatisic(){
        $pending_loans   = Loan::where("status", "pending")->sum("balance");
        $approved_loans  = Loan::where("status", "approved")->sum("balance");
        $settled_loans   = Loan::where("status", "settled")->sum("balance");

        $total_interest     = Loan::sum("interest");
        $total_loan_request = Loan::sum("balance");

        $count_pending  = count(Loan::where("status", "pending")->get());
        $count_settled  = count(Loan::where("status", "settled")->get());
        $count_approved = count(Loan::where("status", "approved")->get());

        $loan_data = [
            'total_pending'     => number_format($pending_loans, 2),
            'total_approved'    => number_format($approved_loans, 2),
            'total_settled'     => number_format($settled_loans, 2),
            'total_interest'    => number_format($total_interest, 2),
            'count_pending'     => number_format($count_pending),
            'count_settled'     => number_format($count_settled),
            'count_approved'    => number_format($count_approved),
            'count_active'      => number_format($count_settled + $count_approved),
            'total_request'     => number_format($total_loan_request, 2)
        ];

        $count_savings = Account::count();
        $total_savings = Account::sum('balance');
        $total_savings_interest = Account::sum('interest');

        $account_data = [
            'count_active'      => number_format($count_savings),
            'total_savings'     => number_format($total_savings, 2),
            'total_interest'    => number_format($total_savings_interest, 2)
        ];


        $stat = [
            'loan' => $loan_data,
            'account' => $account_data
        ];

        // return response
        return response()->json($stat);
    }

    /*
    |---------------------------------------------
    | LOAD ALL TRANSACTIONS
    |---------------------------------------------
    */
    public function loadAllTransactions(){
        $transactions = new Transaction();
        $data         = $transactions->getAllTransaction();

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | LOAD BUSINESS NEWS FROM API
    |---------------------------------------------
    */
    public function loadBusinessNews(){
        $news_update    = new NewsHeadline();
        $data           = $news_update->displayNewsHeadlines();

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | APPLY NEW LOAN
    |---------------------------------------------
    */
    public function applyNewLoan(Request $request){
        $new_loan   = new Loan(); 
        $data       = $new_loan->applyExistingLoan($request);

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | LOAN PENDING LOANS
    |---------------------------------------------
    */
    public function loadPendingLoans(){
        $loans   = new Loan(); 
        $data    = $loans->loadPendingLoans();

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | LOAN APPROVED LOANS
    |---------------------------------------------
    */
    public function loandApprovedLoans(){
        $loans   = new Loan(); 
        $data    = $loans->loadApprovedLoans();

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | LOAN ACTIVE LOANS
    |---------------------------------------------
    */
    public function loadActiveLoans(){
        $loans   = new Loan(); 
        $data    = $loans->loadAllLoans();

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | LOAD ALL SETTLED LOANS
    |---------------------------------------------
    */
    public function loadSettledLoans(){
        $loans   = new Loan(); 
        $data    = $loans->loadSettledLoans();

        // return response
        return response()->json($data);
    }


    /*
    |---------------------------------------------
    | LOAD ALL SETTLED LOANS
    |---------------------------------------------
    */
    public function generateReports(Request $request){

        $reports = new Report();
        $data    = $reports->generateReports($request);

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | CREATE AJO BOOK
    |---------------------------------------------
    */
    public function createAjoBook(Request $request){
        $ajo_book   = new AjoBook();
        $data       = $ajo_book->createNew($request);

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | GET ALL AJO BOOK
    |---------------------------------------------
    */
    public function getAjoBook(){
        $ajo_book   = new AjoBook();
        $data       = $ajo_book->fetchAllAjoBook();

        // return response
        return response()->json($data);
    }


    /*
    |---------------------------------------------
    | GET WORK CATEGORY
    |---------------------------------------------
    */
    public function getWorkCategory(){
        
        $data = [
            'Trader',
            'Salary Work',
            'Student',
            'Doctor',
            'Enterprenuer',
            'School Teacher',
            'Schools',
            'Church',
            'Co-operative',
            'Market Associations',
            'Company'
        ];

        // return response
        return response()->json($data);
    }


    /*
    |---------------------------------------------
    | GET NAMES HINTS
    |---------------------------------------------
    */
    public function getNameHints(){
        $users = User::all();
        if(count($users) > 0){
            $users_box = [];
            foreach ($users as $ul) {
                $data = [
                    'id'    => $ul->id,
                    'name'  => $ul->name,
                ];
                array_push($users_box, $data);
            }
        }else{
            $users_box = [];
        }

        // return response
        return response()->json($users_box);
    }

    /*
    |---------------------------------------------
    | ADD AJO MEMBER
    |---------------------------------------------
    */
    public function addAjoMember(Request $request){
        $ajo_member = new Ajo();
        $data       = $ajo_member->addMember($request);

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | GET AJO MEMBERS
    |---------------------------------------------
    */
    public function getAjoMember(Request $request){
        $book_id = $request->book_id;

        $ajo_members    = new Ajo();
        $data           = $ajo_members->getAjoMembers($book_id);

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | SEARCH CUSTOMER
    |---------------------------------------------
    */
    public function searchCustomer(Request $request){
        $user = new User();
        $data = $user->searchSavingsUser($request->keyword);

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | ACCEPT DEPOSIT
    |---------------------------------------------
    */
    public function acceptDeposit(Request $request){
        $user = new User();
        $data = $user->makePaymentToSavings($request);

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | ACCEPT DEPOSIT
    |---------------------------------------------
    */
    public function acceptWithdraw(Request $request){
        $user = new User();
        $data = $user->makeWithdrawFromSavings($request);

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | ADMIN UPLOAD CUSTOMER INFORMATION
    |---------------------------------------------
    */
    public function uploadCustomerInformation(Request $request){
        $customer_file  = $request->file('customers_information');
        $file_path      = $customer_file->getRealPath();
        // run excel import
        $excel_data = Excel::toCollection([], $customer_file);

        $failed_count   = 0;
        $success_count  = 0;
        foreach ($excel_data as $sub_list) {
            // sub list
            foreach ($sub_list as $cl) {
                if($cl[0] !== "FIRST NAME"){
                    $first_name     = $cl[0];
                    $last_name      = $cl[1];
                    $account_type   = $cl[2];
                    $email          = $cl[3];
                    $mobile         = $cl[4];
                    $address_1      = $cl[5];
                    $address_2      = $cl[6];
                    $state          = $cl[7];
                    $country        = $cl[8];
                    $occupation     = $cl[9];
                    $balance        = $cl[10];
                    $total          = $cl[11];
                    $interest       = $cl[12];
                    $duration       = $cl[13];

                    // merge data to array
                    $payload = [
                        'first_name'    => $first_name,
                        'last_name'     => $last_name,
                        'account_type'  => $account_type,
                        'email'         => $email,
                        'mobile'        => $mobile,
                        'address_1'     => $address_1,
                        'address_2'     => $address_2,
                        'state'         => $state,
                        'country'       => $country,
                        'occupation'    => $occupation,
                        'balance'       => $balance,
                        'total'         => $total,
                        'interest'      => $interest,
                        'duration'      => $duration,
                    ];


                    if($first_name !== null && $last_name !== null){
                        // create user from excel file
                        $user = new User();
                        if($user->createAccountFromExcel($payload)){
                            $success_count++;
                        }else{
                            $failed_count++;
                        }
                    }
                }
            }
        }

        // return redirect back
        $msg = 'Filed upload was successful: '.$failed_count.' failed!; '.$success_count.' was uploaded successful!';
        return redirect()->back()->with('message', $msg);
    }

    /*
    |---------------------------------------------
    | ADMIN UPLOAD CUSTOMER LOANS
    |---------------------------------------------
    */
    public function uploadCustomerLoans(Request $request){
        $customer_file  = $request->file('loans_file');
        $file_path      = $customer_file->getRealPath();
        // run excel import
        $excel_data = Excel::toCollection([], $customer_file);

        $failed_count   = 0;
        $success_count  = 0;
        foreach ($excel_data as $sub_list) {
            // sub list
            foreach ($sub_list as $cl) {
                // return $cl;
                if($cl[1] !== "GROUP NAME"){
                    $sn      = $cl[0];
                    $names   = $cl[1];
                    $balance = $cl[2];
                    $address = $cl[3];
                    $mobile  = $cl[4];

                    $account_type   = "Loans";

                    // merge data to array
                    $payload = [
                        'first_name'    => $names,
                        'last_name'     => "GROUP",
                        'account_type'  => $account_type,
                        'email'         => "example@domain.com",
                        'mobile'        => $mobile,
                        'address_1'     => $address,
                        'address_2'     => $address,
                        'state'         => "Lagos",
                        'country'       => "Nigeria",
                        'occupation'    => "Corporative Group",
                        'balance'       => floatval($balance),
                        'total'         => floatval($balance),
                        'interest'      => 0,
                        'duration'      => 0,
                    ];


                    if($payload['first_name'] !== null && $payload['last_name'] !== null){
                        // create user from excel file
                        $user = new User();
                        if($user->createLoansFromExcel($payload)){
                            $success_count++;
                        }else{
                            $failed_count++;
                        }
                    }
                }
            }
        }

        // return redirect back
        $msg = 'Filed upload was successful: '.$failed_count.' failed!; '.$success_count.' was uploaded successful!';
        return redirect()->back()->with('message', $msg);
    }

    /*
    |---------------------------------------------
    | ADMIN CREATE ADMIN
    |---------------------------------------------
    |
    */
    public function adminAddUsers(Request $request){
        $admin  = new Admin();
        $data   = $admin->addAdmin($request);

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | LIST ALL ADMIN
    |---------------------------------------------
    */
    public function loadAdmin(){
        $admin  = new Admin();
        $data   = $admin->listAdmin();

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | CHANGE PASSWORD
    |---------------------------------------------
    */
    public function changePassword(Request $request){
        $admin  = new Admin();
        $data   = $admin->changeAdminPassword($request);

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | CHANGE PASSWORD
    |---------------------------------------------
    */
    public function updateProfile(Request $request){
        $user   = new User();
        $data   = $user->updateUserProfile($request);

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | CHANGE PASSWORD
    |---------------------------------------------
    */
    public function getAllTrails(Request $request){
        $audit_trails   = new AuditTrail();
        $data           = $audit_trails->getTrails();

        // return response
        return response()->json($data);
    }
}
