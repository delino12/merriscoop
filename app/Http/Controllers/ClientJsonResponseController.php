<?php

namespace Merriscoop\Http\Controllers;

use Illuminate\Http\Request;
use Merriscoop\NewsHeadline;
use Merriscoop\Mail\NewApplicationMail;
use Mail;

class ClientJsonResponseController extends Controller
{
    /*
    |---------------------------------------------
    | SHOW LATEST NEWS HEADLINES
    |---------------------------------------------
    */
    public function loadNews(){
    	$news = new NewsHeadline();
    	$data = $news->displayNewsHeadlines();

    	// return response
    	return response()->json($data);
    }

    /*
    |---------------------------------------------
    | SEND APPLICATION LETTER
    |---------------------------------------------
    */
    public function applicationLetter(Request $request){
        $firstname      = $request->firstname;
        $lastname       = $request->lastname;
        $email          = $request->email;
        $mobile         = $request->mobile;
        $description    = $request->description;
        $address        = $request->address;
        $resume         = $request->file('resume');

        $resume_ext    = $resume->getClientOriginalExtension();
        // if($resume_ext !== 'doc' || $resume_ext !== 'docx'){
        //     $msg = 'Invalid file format, all file format must be docs';
        //     return redirect()->back()->with('error', $msg);
        // }else{
            $new_resume_name = ucwords($firstname).' '.ucwords($lastname).' Resume.'.$resume_ext;
            // return $path;

            //Move Uploaded File
            $destinationPath = 'uploads';
            $resume->move($destinationPath, $new_resume_name);
            $path_to_resume = public_path('uploads').'/'.$new_resume_name;

            $mail_data = [
                'firstname'   => $firstname,
                'lastname'    => $lastname,
                'email'       => $email,
                'mobile'      => $mobile,
                'description' => $description,
                'address'     => $address,
                'resume'      => $path_to_resume,
            ];

            Mail::to('merriscoop@gmail.com')->send(new NewApplicationMail($mail_data));
            $msg = 'We have received your mail, we will get back to you as soon as possible.';
            return redirect()->back()->with('success', $msg);
        // }
    }
}
