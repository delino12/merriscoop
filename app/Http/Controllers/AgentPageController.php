<?php

namespace Merriscoop\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class AgentPageController extends Controller
{
	/*
    |---------------------------------------------
    | AUTHENTICATE ADMIN PAGES
    |---------------------------------------------
    */
    protected $redirectUrl = "/agent/login";
    
    public function __construct(){
        $this->middleware("auth:agent");
    }

    /*
    |---------------------------------------------
    | SHOW AGENT DASHBOARD
    |---------------------------------------------
    */
    public function dashboard(){
    	return view('__agent.dashboard');
    }

    /*
    |---------------------------------------------
    | SHOW AGENT DEPOSIT
    |---------------------------------------------
    */
    public function deposit(){
        return view('__agent.deposit');
    }

    /*
    |---------------------------------------------
    | SHOW AGENT DEPOSIT
    |---------------------------------------------
    */
    public function loanDeposit(){
        return view('__agent.loan-deposit');
    }    

    /*
    |---------------------------------------------
    | SHOW AGENT NEW CUSTOMER
    |---------------------------------------------
    */
    public function customers(){
        return view('__agent.all-customer');
    }

    /*
    |---------------------------------------------
    | SHOW AGENT NEW CUSTOMER
    |---------------------------------------------
    */
    public function balance(){
        return view('__agent.balance');
    }

    /*
    |---------------------------------------------
    | SHOW AGENT NEW CUSTOMER
    |---------------------------------------------
    */
    public function addCustomer(){
        return view('__agent.add-customer');
    }

    /*
    |---------------------------------------------
    | SHOW AGENT LOANS
    |---------------------------------------------
    */
    public function loans(){
        return view('__agent.loans');
    }

    /*
    |---------------------------------------------
    | SHOW AGENT TRANSACTIONS
    |---------------------------------------------
    */
    public function transactions(){
        return view('__agent.transactions');
    }

    /*
    |---------------------------------------------
    | SHOW AGENT LOGOUT
    |---------------------------------------------
    */
    public function logout(){
        Auth::logout();
        return redirect("agent/login");
    }
}
