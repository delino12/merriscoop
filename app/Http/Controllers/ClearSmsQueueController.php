<?php

namespace Merriscoop\Http\Controllers;

use Illuminate\Http\Request;
use Merriscoop\SmsQueue;

class ClearSmsQueueController extends Controller
{
    /*
    |---------------------------------------------
    | CLEAR ALL SMS QUEUE
    |---------------------------------------------
    */
    public function clearQueue(){
    	$clear = new SmsQueue;
    	$clear->processQueueMessage();

    	// message
    	$data = [
    		'status' 	=> 'success',
    		'message' 	=> 'Sms alert queue process sent!',
    	];

    	// return response
    	return response()->json($data);
    }
}
