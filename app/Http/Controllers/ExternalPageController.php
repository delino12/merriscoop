<?php

namespace Merriscoop\Http\Controllers;

use Illuminate\Http\Request;
use Merriscoop\Mail\SendDiscussMail;
use Merriscoop\Mail\SendSubscribeAlert;
use Merriscoop\Mail\SendContactUs;

use Auth;

class ExternalPageController extends Controller
{
    /*
    |---------------------------------------------
    | ENTRY POINT
    |---------------------------------------------
    */
    public function index(){
	   return view('index');
    }

    /*
    |---------------------------------------------
    | ABOUT
    |---------------------------------------------
    */
    public function about(){
        return view('external-pages.about');
    }

    /*
    |---------------------------------------------
    | INVESTMENT
    |---------------------------------------------
    */
    public function investments(){
        return view('external-pages.investments');
    }

    /*
    |---------------------------------------------
    | INVESTMENT
    |---------------------------------------------
    */
    public function careers(){
        return view('external-pages.career');
    }

    /*
    |---------------------------------------------
    | NEWS PAGES
    |---------------------------------------------
    */
    public function news(){
        return view('external-pages.news');
    }

    /*
    |---------------------------------------------
    | SERVICES PAGES
    |---------------------------------------------
    */
    public function services(){
        return view('external-pages.services');
    }

    /*
    |---------------------------------------------
    | LENDING PAGES
    |---------------------------------------------
    */
    public function lending(){
        return view('external-pages.lending');
    }

    /*
    |---------------------------------------------
    | SAVING PAGE
    |---------------------------------------------
    */
    public function savings(){
        return view('external-pages.savings');
    }

    /*
    |---------------------------------------------
    | TERMS AND CONDITION PAGE
    |---------------------------------------------
    */
    public function terms(){
        return view('external-pages.terms');
    }

    /*
    |---------------------------------------------
    | CONTACT US PAGE
    |---------------------------------------------
    */
    public function contact(){
        return view('external-pages.contact');
    }

    /*
    |---------------------------------------------
    | COMPLAINTS US PAGE
    |---------------------------------------------
    */
    public function complaint(){
        return view('external-pages.complaint');
    }


    /*
    |---------------------------------------------
    | COOKIES POLICY
    |---------------------------------------------
    */
    public function cookiePolicy(){
        return view('external-pages.cookies-policy');
    }

    /*
    |---------------------------------------------
    | COOKIES POLICY
    |---------------------------------------------
    */
    public function policy(){
        return view('external-pages.cookies-policy');
    }


    /*
    |---------------------------------------------
    | PRIVATE POLICY
    |---------------------------------------------
    */
    public function privatePolicy(){
        return view('external-pages.private-policy');
    }

    /*
    |---------------------------------------------
    | FAQ
    |---------------------------------------------
    */
    public function faq(){
        return view('external-pages.faq');
    }

    /*
    |---------------------------------------------
    | CONTACT US DESK JSON
    |---------------------------------------------
    */
    public function contactDesk(Request $request){
        $name       = $request->name;
        $subject    = $request->subject;
        $email      = $request->email;
        $phone      = $request->phone;

        $data = [
            'status'    => 'success',
            'message'   => 'Thanks for contacting MerrisCoop Desk, We will get back to you within 24hrs.',
        ];

        $mail_data = [
            'name'      => $name,
            'email'     => $email,
            'subject'   => $subject,
            'phone'     => $phone,
        ];

        // send to admin
        \Mail::to(env("MERRIS"))->send(new SendDiscussMail($mail_data));

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | CONTACT US DESK JSON
    |---------------------------------------------
    */
    public function contactUs(Request $request){
        $name       = $request->name;
        $message    = $request->message;
        $email      = $request->email;

        $mail_data = [
            'name'      => $name,
            'message'   => $message,
            'email'     => $email
        ];

        // send to admin
        \Mail::to(env("MERRIS"))->send(new SendContactUs($mail_data));

        $data = [
            'status'    => 'success',
            'message'   => 'Thanks for contacting MerrisCoop Office, We will get back to you within 24hrs.',
        ];

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | CONTACT US DESK JSON
    |---------------------------------------------
    */
    public function subscribeToNewsletter(Request $request){
        $email      = $request->email;

        if(empty($email)){
            $data = [
                'status'    => 'error',
                'message'   => 'Provide a valid email address',
            ];
        }else{
            $data = [
                'status'    => 'success',
                'message'   => 'Subscription successful!, Now you can get updates from Merriscoop Weekly Newsletters',
            ];

            $mail_data = [
                'email'     => $email,
            ];

            // send to admin
            \Mail::to(env("MERRIS"))->send(new SendSubscribeAlert($mail_data));
        }

        // return response
        return response()->json($data);
    }

}
