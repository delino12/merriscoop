<?php

namespace Merriscoop\Http\Controllers;

use Illuminate\Http\Request;
use Merriscoop\Agent;
use Auth;

class AgentAuthenticationController extends Controller
{
	/*
    |---------------------------------------------
    | AUTHENTICATION LOGIN ROUTE
    |---------------------------------------------
    */
    public function __construct(){
    	$this->middleware("guest");
    }

    /*
    |---------------------------------------------
    | SHOW AGENT LOGIN
    |---------------------------------------------
    */
    public function showAgentLogin(){
    	$this->initDefaultAgent();
    	return view("__auth.agent-login");
    }

    /*
    |---------------------------------------------
    | PROCESS LOGIN
    |---------------------------------------------
    */
    public function processLogin(Request $request){
    	// body
        $code  = $request->code;
        $pin   = $request->pin;

        // verify login
        $check_exist = Agent::where('email', $code)->first();
        if($check_exist == null){
            $data = [
                'status'    => 'error',
                'message'   => 'Invalid agent code try again!'
            ];
        }else{
            // body
            if(Auth::guard("agent")->attempt(['email' => $code, 'password' => $pin])){
                $data = [
                    'status'    => 'success',
                    'message'   => 'Login successful!'
                ];
            }else{
                $data = [
                    'status'    => 'error',
                    'message'   => 'Invalid password... try again!'
                ];
            }
        }

        // return response.
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | CREATE DEFAULT AGENT
    |---------------------------------------------
    */
    public function initDefaultAgent(){
    	return Agent::createDefaultAgent();
    }

}
