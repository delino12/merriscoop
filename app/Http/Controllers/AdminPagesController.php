<?php

namespace Merriscoop\Http\Controllers;

use Illuminate\Http\Request;
use Merriscoop\Admin;
use Merriscoop\AjoBook;
use Merriscoop\AuditTrail;
use Auth;

class AdminPagesController extends Controller
{
    // redirect if not auth
    protected $redirectUrl = 'admin/login';

    /*
    |---------------------------------------------
    | AUTHENTICATE ADMIN PAGES
    |---------------------------------------------
    */
    public function __construct(){
        $this->middleware("auth:admin");
    }

    /*
    |---------------------------------------------
    | SHOW ADMIN PAGES CONTROLLER
    |---------------------------------------------
    */
    public function dashboard(){
        $this->logTrail(Auth::guard("admin")->user()->id, 'Navigation', 'Active on Dashboard page!');

        $admin = Admin::where("email", Auth::user()->email)->first();
    	return view("__admin.dashboard", compact('admin'));
    }
    
    /*
    |---------------------------------------------
    | TRANSFER
    |---------------------------------------------
    */
    public function transfer(){
        $admin = Admin::where("email", Auth::user()->email)->first();
        return view("__admin.transfer", compact('admin'));
    }


    /*
    |---------------------------------------------
    | AJO REGULAR
    |---------------------------------------------
    */
    public function ajoRegular(){
        $admin = Admin::where("email", Auth::user()->email)->first();
        return view("__admin.ajo-regular", compact('admin'));
    }

    /*
    |---------------------------------------------
    | AJO REGULAR
    |---------------------------------------------
    */
    public function ajoPro(){
        $admin = Admin::where("email", Auth::user()->email)->first();
        return view("__admin.ajo-pro", compact('admin'));
    }


    /*
    |---------------------------------------------
    | AJO REGULAR
    |---------------------------------------------
    */
    public function viewAjo($id){
        $admin = Admin::where("email", Auth::user()->email)->first();
        $book_id = $id;
        $ajobook = AjoBook::where("id", $id)->first();
        return view("__admin.view-ajo", compact('admin', 'book_id', 'ajobook'));
    }
    
    /*
    |---------------------------------------------
    | TRANSACTIONS
    |---------------------------------------------
    */
    public function transactions(){
        $this->logTrail(Auth::guard("admin")->user()->id, 'Navigation', 'Active on Customer Transactions page!');

        $admin = Admin::where("email", Auth::user()->email)->first();
        return view("__admin.transactions", compact('admin'));
    }
    
    /*
    |---------------------------------------------
    | NEW USER APPLICATION
    |---------------------------------------------
    */
    public function newCustomer(){
        $this->logTrail(Auth::guard("admin")->user()->id, 'Navigation', 'Active on New Customer page!');

        $admin = Admin::where("email", Auth::user()->email)->first();
        return view("__admin.new-customer", compact('admin'));
    }

    /*
    |---------------------------------------------
    | VIEW CLIENT ACCOUNT
    |---------------------------------------------
    */
    public function viewCustomer($id){
        $this->logTrail(Auth::guard("admin")->user()->id, 'Navigation', 'Active on Viewing Customer Profile page!');

        $admin = Admin::where("email", Auth::user()->email)->first();
        return view("__admin.view-customer", compact('admin', 'id'));
    }

    /*
    |---------------------------------------------
    | VIEW CLIENT ACCOUNT
    |---------------------------------------------
    */
    public function viewLoanHistory($id){
        $this->logTrail(Auth::guard("admin")->user()->id, 'Navigation', 'Active on User Loans History page!');

        $admin = Admin::where("email", Auth::user()->email)->first();
        return view("__admin.loan-history", compact('admin', 'id'));
    }

    /*
    |---------------------------------------------
    | VIEW CLIENT ACCOUNT
    |---------------------------------------------
    */
    public function viewSavingsHistory($id){
        $this->logTrail(Auth::guard("admin")->user()->id, 'Navigation', 'Active on User Savings History page!');

        $admin = Admin::where("email", Auth::user()->email)->first();
        return view("__admin.savings-history", compact('admin', 'id'));
    }

    /*
    |---------------------------------------------
    | VIEW PAYMENT
    |---------------------------------------------
    */
    public function viewPaymentHistory($id){
        $this->logTrail(Auth::guard("admin")->user()->id, 'Navigation', 'Active on View Loans Reciept page!');

        $admin = Admin::where("email", Auth::user()->email)->first();
        return view("__admin.view-payment", compact('admin', 'id'));
    }

    /*
    |---------------------------------------------
    | VIEW PAYMENT
    |---------------------------------------------
    */
    public function viewSavingsPaymentHistory($id){
        $this->logTrail(Auth::guard("admin")->user()->id, 'Navigation', 'Active on View Savings Reciept page!');

        $admin = Admin::where("email", Auth::user()->email)->first();
        return view("__admin.view-payment-savings", compact('admin', 'id'));
    }

    /*
    |---------------------------------------------
    | ACTIVE LOANS
    |---------------------------------------------
    */
    public function activeLoans(){
        $this->logTrail(Auth::guard("admin")->user()->id, 'Navigation', 'Active on View Loans Customers page!');

        $admin = Admin::where("email", Auth::user()->email)->first();
        return view("__admin.active-loans", compact('admin'));
    } 

    /*
    |---------------------------------------------
    | NEW LOAN APPLICATION
    |---------------------------------------------
    */
    public function applyNewLoan(){
        $this->logTrail(Auth::guard("admin")->user()->id, 'Navigation', 'Active on Apply New Loans page!');

        $admin = Admin::where("email", Auth::user()->email)->first();
        return view("__admin.new-loans", compact('admin'));
    }  

    /*
    |---------------------------------------------
    | NEW SAVINGS APPLICATION
    |---------------------------------------------
    */
    public function applyNewSavings(){
        $this->logTrail(Auth::guard("admin")->user()->id, 'Navigation', 'Active on New Savings page!');

        $admin = Admin::where("email", Auth::user()->email)->first();
        return view("__admin.new-savings", compact('admin'));
    }

    /*
    |---------------------------------------------
    | ACTIVE SAVINGS
    |---------------------------------------------
    */
    public function activeSavings(){
        $this->logTrail(Auth::guard("admin")->user()->id, 'Navigation', 'Active on Savings Account page!');

        $admin = Admin::where("email", Auth::user()->email)->first();
        return view("__admin.active-savings", compact('admin'));
    }
    
    /*
    |---------------------------------------------
    | APPROVED LOANS
    |---------------------------------------------
    */
    public function approvedLoans(){
        $this->logTrail(Auth::guard("admin")->user()->id, 'Navigation', 'Active on Approved Loans page!');

        $admin = Admin::where("email", Auth::user()->email)->first();
        return view("__admin.approved-loans", compact('admin'));
    }

    /*
    |---------------------------------------------
    | PENDING LOANS
    |---------------------------------------------
    */
    public function pendingLoans(){
        $this->logTrail(Auth::guard("admin")->user()->id, 'Navigation', 'Active on Pending Loans page!');

        $admin = Admin::where("email", Auth::user()->email)->first();
        return view("__admin.pending-loans", compact('admin'));
    }

    /*
    |---------------------------------------------
    | SETTLED LOANS
    |---------------------------------------------
    */
    public function settledLoans(){
        $this->logTrail(Auth::guard("admin")->user()->id, 'Navigation', 'Active on Settled Loans page!');

        $admin = Admin::where("email", Auth::user()->email)->first();
        return view("__admin.settled-loans", compact('admin'));
    }

    /*
    |---------------------------------------------
    | SETTLED LOANS
    |---------------------------------------------
    */
    public function reports(){
        $this->logTrail(Auth::guard("admin")->user()->id, 'Navigation', 'Active on Reports page!');

        $admin = Admin::where("email", Auth::user()->email)->first();
        return view("__admin.reports", compact('admin'));
    }

    /*
    |---------------------------------------------
    | AUDIT TRAILS
    |---------------------------------------------
    */
    public function auditTrails(){
        $this->logTrail(Auth::guard("admin")->user()->id, 'Navigation', 'Active on Audit Trail page!');

        $admin = Admin::where("email", Auth::user()->email)->first();
        return view("__admin.audit-trails", compact('admin'));
    }

    /*
    |---------------------------------------------
    | ADMIN SETTINGS
    |---------------------------------------------
    */
    public function settings(){
        $this->logTrail(Auth::guard("admin")->user()->id, 'Navigation', 'Active on Settings page!');

        $admin = Admin::where("email", Auth::user()->email)->first();
        return view("__admin.settings", compact('admin'));
    }

    /*
    |---------------------------------------------
    | SHOW CUSTOMERS
    |---------------------------------------------
    */
    public function showCustomers(){
        $this->logTrail(Auth::guard("admin")->user()->id, 'Navigation', 'Active on All Customer page!');

        $admin = Admin::where("email", Auth::user()->email)->first();
        return view("__admin.all-customers", compact('admin'));
    }

    /*
    |---------------------------------------------
    | SHOW CUSTOMERS
    |---------------------------------------------
    */
    public function quickDeposit(){
        $this->logTrail(Auth::guard("admin")->user()->id, 'Navigation', 'Active on Quick Deposit page!');

        $admin = Admin::where("email", Auth::user()->email)->first();
        return view("__admin.quick-deposit", compact('admin'));
    }

    /*
    |---------------------------------------------
    | SHOW CUSTOMERS
    |---------------------------------------------
    */
    public function quickWithdraw(){
        $this->logTrail(Auth::guard("admin")->user()->id, 'Navigation', 'Active on Quick Withdraw page!');

        $admin = Admin::where("email", Auth::user()->email)->first();
        return view("__admin.quick-withdraw", compact('admin'));
    }

    /*
    |---------------------------------------------
    | SHOW CUSTOMERS
    |---------------------------------------------
    */
    public function configuration(){
        $this->logTrail(Auth::guard("admin")->user()->id, 'Navigation', 'Active on configuration page!');

        $admin = Admin::where("email", Auth::user()->email)->first();
        return view("__admin.configuration", compact('admin'));
    }

    /*
    |---------------------------------------------
    | LOGOUT ADMIN SECTION
    |---------------------------------------------
    */
    public function logout(){
        $this->logTrail(Auth::guard("admin")->user()->id, 'Admin Logout', 'Logout was successful!');
        Auth::logout();
        return redirect('/admin/login');
    }

    /*
    |---------------------------------------------
    | LOG TRAIL
    |---------------------------------------------
    */
    public function logTrail($admin_id, $process, $description){
        $new_trail = new AuditTrail();
        $new_trail->logTrail($admin_id, $process, $description);
    }
}
