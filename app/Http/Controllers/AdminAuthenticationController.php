<?php

namespace Merriscoop\Http\Controllers;

use Illuminate\Http\Request;
use Merriscoop\Admin;
use Merriscoop\AuditTrail;
use Auth;

class AdminAuthenticationController extends Controller
{
    /*
    |---------------------------------------------
    | AUTHENTICATION LOGIN ROUTE
    |---------------------------------------------
    */
    public function __construct(){
    	$this->middleware("guest");
    }

    /*
    |---------------------------------------------
    | SHOW LOGIN VIEW
    |---------------------------------------------
    */
    public function showAuthLogin(){
    	// init default login
    	$this->createDefaultAdminLogin();
    	return view('__auth.login');
    }

    /*
    |---------------------------------------------
    | PROCESS LOGIN
    |---------------------------------------------
    */
    public function processLogin(Request $request){
    	// body
        $email      = $request->email;
        $password   = $request->password;

        // verify login
        $check_exist = Admin::where('email', $email)->first();
        if($check_exist == null){

            $data = [
                'status'    => 'error',
                'message'   => 'Invalid Email try again !'
            ];

        }else{

            // body
            if(Auth::guard("admin")->attempt(['email' => $email, 'password' => $password])){
                $data = [
                    'status'    => 'success',
                    'message'   => 'Login successful !'
                ];

                $this->logTrail(Auth::guard("admin")->user()->id, 'Admin Login', 'Login process was successful!');

            }else{

                $data = [
                    'status'    => 'error',
                    'message'   => 'Invalid password... try again !'
                ];
            }
        }

        // return response.
        return response()->json($data);
    }


    /*
    |---------------------------------------------
    | CREATE DEFAULT ADMIN LOGIN
    |---------------------------------------------
    */
    public function createDefaultAdminLogin(){
    	
    	// create new admin
    	$new_admin = new Admin();
    	if(!$new_admin->checkAlreadyExist()){
    		$new_admin->addDefaultAdmin();
    	}
    }

    /*
    |---------------------------------------------
    | LOG TRAIL
    |---------------------------------------------
    */
    public function logTrail($admin_id, $process, $description){
        $new_trail = new AuditTrail();
        $new_trail->logTrail($admin_id, $process, $description);
    }
}
