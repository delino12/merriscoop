<?php

namespace Merriscoop\Http\Controllers;

use Illuminate\Http\Request;
use Merriscoop\WaitList;

class DevelopmentModeController extends Controller
{
    /*
    |---------------------------------------------
    | MAINTENACE MODE devMode
    |---------------------------------------------
    */
    public function devMode(){
    	return view("__devmode.development");
    }


    /*
    |---------------------------------------------
    | MAINTENACE MODE updateMode
    |---------------------------------------------
    */
    public function updateMode(){
    	return view("__devmode.upgrade");
    }


    /*
    |---------------------------------------------
    | ADD TO WAITING LIST
    |---------------------------------------------
    */
    public function addToWaiting(Request $request){
        $email = $request->email;

        $wait_list  = new WaitList();
        $data       = $wait_list->addToWaiting($email);

        
        // return response
        return response()->json($data);
    }
}
