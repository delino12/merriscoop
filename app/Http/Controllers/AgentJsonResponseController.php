<?php

namespace Merriscoop\Http\Controllers;

use Illuminate\Http\Request;
use Merriscoop\Formular;
use Merriscoop\Account;
use Merriscoop\User;
use Merriscoop\BioData;
use Merriscoop\Loan;
use Merriscoop\Investment;
use Merriscoop\Transaction;
use Merriscoop\NewsHeadline;
use Auth;

class AgentJsonResponseController extends Controller
{

    /*
    |---------------------------------------------
    | LOAN ACTIVE LOANS
    |---------------------------------------------
    */
    public function loadActiveLoans(){
        $loans   = new Loan(); 
        $data    = $loans->loadAllLoans();

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | ACCEPT CLIENT PAYMENT
    |---------------------------------------------
    */
    public function initPayment(Request $request){
        
        $user = new User();
        $data = $user->makePayment($request);

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | GET LOAN HISTORY
    |---------------------------------------------
    */
    public function loadLoanHistory($loan_id){
        $loan = new Loan();
        $data = $loan->getLoanById($loan_id);

        // return response
        return response()->json($data);
    }


    /*
    |---------------------------------------------
    | LOAD ALL USERS
    |---------------------------------------------
    */
    public function loadUsersInformation(){
        $user = new User();
        $data = $user->getAllUsers();

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | LOAD ALL TRANSACTIONS
    |---------------------------------------------
    */
    public function loadAllTransactions(){
        $transactions = new Transaction();
        $data         = $transactions->getAllTransaction();

        // return response
        return response()->json($data);
    }


    /*
    |---------------------------------------------
    | ACCEPT DEPOSIT
    |---------------------------------------------
    */
    public function rePayment(Request $request){
        $user = new User();
        $data = $user->makePayment($request);

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | SEARCH CUSTOMER
    |---------------------------------------------
    */
    public function searchSavingsCustomer(Request $request){
        $user = new User();
        $data = $user->searchSavingsUser($request->keyword);

        // return response
        return response()->json($data);
    }

    /*
    |---------------------------------------------
    | SEARCH CUSTOMER
    |---------------------------------------------
    */
    public function searchLoansCustomer(Request $request){
        $user = new User();
        $data = $user->searchUser($request->keyword);

        // return response
        return response()->json($data);
    }
    
    /*
    |---------------------------------------------
    | ACCEPT DEPOSIT
    |---------------------------------------------
    */
    public function acceptDeposit(Request $request){
        $user = new User();
        $data = $user->makePaymentToSavings($request);

        // return response
        return response()->json($data);
    }
}
