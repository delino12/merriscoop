<?php

namespace Merriscoop;

use Illuminate\Database\Eloquent\Model;
use Merriscoop\Formular;
use Merriscoop\BioData;
use Merriscoop\Transaction;
use Merriscoop\User;
use Carbon\Carbon;

class Account extends Model
{
    /*
    |---------------------------------------------
    | CREATE NEW USER ACCOUNT
    |---------------------------------------------
    */
    public function createUserAccount($payload, $user_id){
        $mutable  = Carbon::now();
        $duration = $mutable->addDays($payload->duration);

        // get formular
        $percent    = $this->getPercent($payload->interest);

        $interest   = $percent * $payload->amount;
        $total      = $payload->amount + $interest;

        // ehck if user already have account
        $already_account = Account::where("user_id", $user_id)->first();
        if($already_account !== null){
            $account_no = $already_account->account_id;
        }else{
            $account_no = "MC-03".rand(000, 999).rand(000, 999).rand(000, 999);
        }

    	$new_account 				= new Account();
    	$new_account->user_id 		= $user_id;
    	$new_account->account_id 	= $account_no;
    	$new_account->status 		= "active";
    	$new_account->balance       = $payload->amount;
        $new_account->total         = $total;
        $new_account->interest      = $interest;
        $new_account->duration      = $duration;
        if($new_account->save()){
            return true;
        }else{
            return false;
        }
    }

    /*
    |---------------------------------------------
    | FETCH ACCOUNT DATA
    |---------------------------------------------
    */
    public function getUserById($user_id){
        $account_data = Account::where("user_id", $user_id)->first();

        // return
        return $account_data;
    }

    /*
    |---------------------------------------------
    | comment
    |---------------------------------------------
    */
    public function getAccountById($account_id){
        $account_data = Account::where("id", $account_id)->first();
        if($account_data !== null){
            // get transaction history
            $transactions = Transaction::where("related_id", $account_id)->get();
            $trans_box = [];
            if(count($transactions) > 0){
                foreach ($transactions as $el) {
                    # code...
                    $data = [
                        'id'            => $el->id,
                        'user_id'       => $el->user_id,
                        'related_id'    => $el->related_id,
                        'ref_no'        => $el->ref_no,
                        'signature'     => $el->signature,
                        'name'          => $el->name,
                        'type'          => $el->type,
                        'note'          => $el->note,
                        'amount'        => $el->amount,
                        'status'        => $el->status,
                        'created_at'    => $el->created_at->diffForHumans(),
                        'updated_at'    => $el->updated_at->diffForHumans()
                    ];

                    array_push($trans_box, $data);
                }
            }

            $history = [
                'account'       => $account_data,
                'transactions'  => $trans_box
            ];
        }

        // return
        return $history;
    }

    /*
    |---------------------------------------------
    | FETCH INVESTMENT PERCENT
    |---------------------------------------------
    */
    public function getPercent($interest){
        $data = $interest / 100;

        // return
        return $data;
    }

    /*
    |---------------------------------------------
    | CREATE ACCOUNT FROM EXCEL
    |---------------------------------------------
    */
    public function createAccountFromExcel($payload, $user_id){
        $duration   = $payload['duration'];
        $interest   = $payload['interest'];
        $balance    = $payload['balance'];
        $amount     = 0;

        if($interest == null){
            $interest = 0;
        }else{
            $interest = $payload['interest'];
        }

        if($balance == null){
            $balance = 0;
        }else{
            $balance = $payload['balance'];
        }

        if($duration == null){
            $duration = 0;
        }else{
            $duration = $payload['duration'];
        }

        $mutable  = Carbon::now();
        $duration = $mutable->addDays($duration);

        // get formular
        $percent    = $this->getPercent($interest);
        $interest   = $percent * $amount;
        $total      = $amount + $interest;

        // ehck if user already have account
        $account_no = "MC-".rand(000, 999).rand(000, 999).rand(000, 999);
        
        $new_account                = new Account();
        $new_account->user_id       = $user_id;
        $new_account->account_id    = $account_no;
        $new_account->status        = "active";
        $new_account->balance       = $amount;
        $new_account->total         = $total;
        $new_account->interest      = $interest;
        $new_account->duration      = $duration;
        $new_account->save();
    }
}
