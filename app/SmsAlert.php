<?php

namespace Merriscoop;

use Illuminate\Database\Eloquent\Model;

class SmsAlert extends Model
{
    protected $user;
    protected $pass;
    protected $url;
    protected $route;
    protected $vtype;

    // init SMS Factory
    public function __construct()
    {
        # code...
        $this->user     = env("SMS_API_USER");
        $this->pass     = env("SMS_API_PASS");
        $this->url 		= env("SMS_API_URL");
        $this->route 	= 1;
        $this->vtype    = 1;
    }

    // send message 
    public function sendMessages($receivers, $message, $sender)
    {

        $query = [
        	'username' => $this->user,
        	'password' => $this->pass,
        	'mobile'   => $receivers,
        	'sender'   => "MerrisCoop",
        	'message'  => $message,
        	'route'    => 1,
        	'vtype'    => 1
        ];

        // send message using SMS Api
        $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, "https://www.mobileairtimeng.com/smsapi/bulksms.php");
	    curl_setopt($ch, CURLOPT_POST, 1);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 200);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 200);
	    $res = curl_exec($ch);

        // filtered response
        $filtered = explode("|", $res);
        if($filtered[0] == '2001'){
            // filtered
            $this->logSMS($receivers, $message);

            // return true
            return true;
        }else{
            return false;
        }

        // close curl
	    curl_close($ch);
    }

    // send to all users
    public function readErrorCode($sms_response)
    {
    	if($sms_response == '-100'){
            // empty username
            $msg = "<span class='text-danger'>empty username... </span>";

        }elseif($sms_response == '-200'){
            // empty password
            $msg = "<span class='text-danger'>empty username... </span>";
        }
        elseif($sms_response == '-300'){
            // empty sender id
            $msg = "<span class='text-danger'>empty sender ID </span>";

        }elseif($sms_response == '-400'){
            // sender id greater than 14 Characters
            $msg = "<span class='text-danger'>sender id is too Long... </span>";

        }elseif($sms_response == '-500'){
            // Invalid receivers phone number
            $msg = "<span class='text-danger'>Invalid receivers phone no... </span>";

        }elseif($sms_response == '-600'){
            // invalid username/password
            $msg = "<span class='text-danger'>invalid username and password... </span>";

        }elseif($sms_response == '-700'){
            // empty message
            $msg = "<span class='text-danger'>empty message </span>";

        }elseif($sms_response == '-800'){
            // insufficient credit
            $msg = "<span class='text-danger'>Insufficient credit.. </span>";

        }elseif($sms_response == '-900'){
            // Account deactivated
            $msg = "<span class='text-danger'>Account deactivated... </span>";

        }else{
            // message has been sent !
            $msg = "<span class='text-success'>Message sent successfully! </span>";
        }

        // return response
        return $msg;
    }

    // log sms process
    public function logSMS($phone, $message)
    {
        # code...
        $this->alert_phone    	= $phone;
        $this->alert_message    = $message;
        $this->status   		= 'sent';
        $this->save();
    }
}
