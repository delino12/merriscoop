<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormularsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formulars', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('min_amount', 18, 4);
            $table->decimal('max_amount', 18, 4);
            $table->decimal('amount_1', 18, 4);
            $table->decimal('amount_2', 18, 4);
            $table->decimal('amount_3', 18, 4);
            $table->decimal('amount_4', 18, 4);
            $table->string('formular_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formulars');
    }
}
