<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAjosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ajos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('ajobook_id');
            $table->string('status');
            $table->decimal('amount', 18, 4);
            $table->decimal('charge', 18, 4);
            $table->string('next_payout_date');
            $table->string('next_payout_user');
            $table->string('duration');
            $table->integer('turn_collected');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ajos');
    }
}
