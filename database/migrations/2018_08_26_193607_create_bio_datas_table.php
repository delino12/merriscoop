<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBioDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bio_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('phone');
            $table->string('address');
            $table->string('country');
            $table->string('state');
            $table->string('bvn')->nullable();
            $table->string('passport_id');
            $table->string('fingerprint')->nullable();
            $table->string('nok_firstname')->nullable();
            $table->string('nok_lastname')->nullable();
            $table->string('nok_phone')->nullable();
            $table->string('nok_address')->nullable();
            $table->string('gad_firstname')->nullable();
            $table->string('gad_lastname')->nullable();
            $table->string('gad_address')->nullable();
            $table->string('occupation')->nullable();
            $table->string('avatar')->nullable();
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bio_datas');
    }
}
