<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAjoProsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ajo_pros', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ajobook_id');
            $table->integer('group_id');
            $table->string('group_name');
            $table->string('status');
            $table->decimal('amount', 18, 4);
            $table->decimal('charge', 18, 4);
            $table->string('next_payout_date');
            $table->string('next_payout_user');
            $table->string('duration');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ajo_pros');
    }
}
