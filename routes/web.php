<?php

/*
|---------------------------------------------------------------------------------------------------------
| ENTRY POINT URI
|---------------------------------------------------------------------------------------------------------
|
*/
Route::get('/', 					'ExternalPageController@index')->name('index');
Route::get('/about', 				'ExternalPageController@about')->name('about');
Route::get('/news', 				'ExternalPageController@news')->name('news');
Route::get('/services', 			'ExternalPageController@services')->name('services');
Route::get('/lending', 				'ExternalPageController@lending')->name('lending');
Route::get('/savings', 				'ExternalPageController@savings')->name('savings');
Route::get('/contact', 				'ExternalPageController@contact')->name('contact');
Route::get('/investments', 			'ExternalPageController@investments')->name('investments');
Route::get('/careers', 				'ExternalPageController@careers')->name('careers');
Route::get('/terms',				'ExternalPageController@terms')->name('terms');
Route::get('/policy',				'ExternalPageController@policy')->name('policy');
Route::get('/private-policy',		'ExternalPageController@privatePolicy')->name('private_policy');
Route::get('/faq',					'ExternalPageController@faq')->name('faq');
Route::get('/cookies-policy',		'ExternalPageController@cookiePolicy')->name('cookie_policy');
Route::get('/complaint',			'ExternalPageController@complaint')->name('complaint');

Route::post('/client/contact-desk',	'ExternalPageController@contactDesk');
Route::post('/client/contact-us',	'ExternalPageController@contactUs');
Route::post('/client/subscribe',	'ExternalPageController@subscribeToNewsletter');


/*
|---------------------------------------------------------------------------------------------------------
| WEB SITE VISITOR
|---------------------------------------------------------------------------------------------------------
|
*/
Route::post('/add/to/wait-list', 	'DevelopmentModeController@addToWaiting')->name('index');


/*
|---------------------------------------------------------------------------------------------------------
| LOAD GLOBAL NEWS
|---------------------------------------------------------------------------------------------------------
|
*/
Route::get('/load/global/news', 	'ClientJsonResponseController@loadNews')->name('global_news');
Route::post('/send/application/form','ClientJsonResponseController@applicationLetter')->name('application_letter');


/*
|---------------------------------------------------------------------------------------------------------
| AUTHENTICATION SECTION URI
|---------------------------------------------------------------------------------------------------------
|
*/
Route::get('/admin/login', 			'AdminAuthenticationController@showAuthLogin')->name('login');
Route::post('/admin/login', 		'AdminAuthenticationController@processLogin')->name('process_login');


/*
|---------------------------------------------------------------------------------------------------------
| AUTHENTICATION FOR AGENT ACCOUNT
|---------------------------------------------------------------------------------------------------------
|
*/
Route::get('/agent/login', 			'AgentAuthenticationController@showAgentLogin');
Route::post('/agent/login', 		'AgentAuthenticationController@processLogin');

/*
|---------------------------------------------------------------------------------------------------------
| AUTHENTICATION FOR AGENT ACCOUNT
|---------------------------------------------------------------------------------------------------------
|
*/
Route::get('/agent/dashboard',		'AgentPageController@dashboard');
Route::get('/agent/deposit',		'AgentPageController@deposit');
Route::get('/agent/loan-deposit',	'AgentPageController@loanDeposit');
Route::get('/agent/customers',		'AgentPageController@customers');
Route::get('/agent/check/balance',	'AgentPageController@balance');
Route::get('/agent/new/customer',	'AgentPageController@addCustomer');
Route::get('/agent/loans',			'AgentPageController@loans');
Route::get('/agent/transactions',	'AgentPageController@transactions');
Route::get('/agent/logout',			'AgentPageController@logout');

/*
|---------------------------------------------------------------------------------------------------------
| AGENT JSON RESPONSE CONTROLLER
|---------------------------------------------------------------------------------------------------------
|
*/
Route::get('/agent/all/customers',		'AgentJsonResponseController@loadUsersInformation');
Route::get('/agent/all/transactions', 	'AgentJsonResponseController@loadAllTransactions');
Route::get('/agent/all/loans',			'AgentJsonResponseController@loadActiveLoans');
Route::post('/agent/search/customer/2', 'AgentJsonResponseController@searchSavingsCustomer');
Route::post('/agent/search/customer/1', 'AgentJsonResponseController@searchLoansCustomer');
Route::post('/agent/accept/deposit',	'AgentJsonResponseController@rePayment');
Route::post('/agent/savings/deposit', 	'AgentJsonResponseController@acceptDeposit');


/*
|---------------------------------------------------------------------------------------------------------
| ADMIN PAGES SECTION URI
|---------------------------------------------------------------------------------------------------------
|
*/
Route::get('/admin/dashboard',			'AdminPagesController@dashboard')->name('admin_dashboard');
Route::get('/admin/transfer',			'AdminPagesController@transfer')->name('admin_transfer');
Route::get('/admin/apply/new/loan',		'AdminPagesController@applyNewLoan')->name('admin_new_loan');
Route::get('/admin/apply/new/savings',	'AdminPagesController@applyNewSavings')->name('admin_new_loan');
Route::get('/admin/ajo/group',			'AdminPagesController@ajoPro')->name('admin_ajo_pro');
Route::get('/admin/ajo/general',		'AdminPagesController@ajoRegular')->name('admin_ajo_regular');
Route::get('/admin/reports',			'AdminPagesController@reports')->name('admin_reports');
Route::get('/admin/audit/trails',		'AdminPagesController@auditTrails')->name('admin_audit_trails');
Route::get('/admin/transactions',		'AdminPagesController@transactions')->name('admin_transaction');
Route::get('/admin/new-client',			'AdminPagesController@newCustomer')->name('admin_application');
Route::get('/admin/view-client/{id}',	'AdminPagesController@viewCustomer')->name('admin_view_client');
Route::get('/admin/view-loan/{id}',		'AdminPagesController@viewLoanHistory')->name('admin_view_loan');
Route::get('/admin/view-savings/{id}',			'AdminPagesController@viewSavingsHistory')->name('admin_view_savings');
Route::get('/admin/view-payment/{id}',			'AdminPagesController@viewPaymentHistory')->name('admin_payment');
Route::get('/admin/view-payment-savings/{id}',	'AdminPagesController@viewSavingsPaymentHistory')->name('admin_savings');
Route::get('/admin/active-loans',				'AdminPagesController@activeLoans')->name('admin_active');
Route::get('/admin/active-savings',		'AdminPagesController@activeSavings')->name('admin_active_2');
Route::get('/admin/approved-loans',		'AdminPagesController@approvedLoans')->name('admin_approved');
Route::get('/admin/pending-loans',		'AdminPagesController@pendingLoans')->name('admin_pending');
Route::get('/admin/settled-loans',		'AdminPagesController@settledLoans')->name('admin_settled');
Route::get('/admin/settings',			'AdminPagesController@settings')->name('admin_settings');
Route::get('/admin/view/ajo/{id}',		'AdminPagesController@viewAjo')->name('admin_view_ajo');
Route::get('/admin/all-customers',		'AdminPagesController@showCustomers')->name('admin_customers');
Route::get('/admin/quick-deposit',		'AdminPagesController@quickDeposit')->name('admin_deposit');
Route::get('/admin/quick-withdraw',		'AdminPagesController@quickWithdraw')->name('admin_withdraw');
Route::get('/admin/config-admin',		'AdminPagesController@configuration')->name('admin_configuration');
Route::get('/admin/logout',				'AdminPagesController@logout')->name('logout');



/*
|---------------------------------------------------------------------------------------------------------
| ADMIN JSON SECTION URI
|---------------------------------------------------------------------------------------------------------
|
*/
Route::post('/admin/create/new/user',		'AdminJsonResponseController@createNewUser');
Route::get('/admin/load/all/customers',		'AdminJsonResponseController@loadUsersInformation');
Route::get('/admin/load/one/customer/{id}', 'AdminJsonResponseController@loadSingleUserInformation');
Route::post('/admin/approve-client/loan',	'AdminJsonResponseController@approveClientLoan');
Route::post('/admin/update/formular',		'AdminJsonResponseController@updateConfig');
Route::get('/admin/get/formular',			'AdminJsonResponseController@getConfig');
Route::post('/admin/init/payment',			'AdminJsonResponseController@initPayment');
Route::get('/admin/loan/history/{id}',		'AdminJsonResponseController@loadLoanHistory');
Route::get('/admin/account/history/{id}',	'AdminJsonResponseController@loadAccountHistory');
Route::get('/admin/one/transaction/{id}', 	'AdminJsonResponseController@loanSingleTransaction');
Route::get('/admin/load/statistic',			'AdminJsonResponseController@dashboardStatisic');
Route::get('/admin/load/all/transactions',	'AdminJsonResponseController@loadAllTransactions');
Route::get('/admin/load/business/news',		'AdminJsonResponseController@loadBusinessNews');
Route::post('/admin/new/loan/account',		'AdminJsonResponseController@applyNewLoan');
Route::get('/admin/load/pending/loans',		'AdminJsonResponseController@loadPendingLoans');
Route::get('/admin/load/approved/loans',	'AdminJsonResponseController@loandApprovedLoans');
Route::get('/admin/load/active/loans',		'AdminJsonResponseController@loadActiveLoans');
Route::get('/admin/load/settled/loans',		'AdminJsonResponseController@loadSettledLoans');
Route::get('/admin/new/savings/account',	'AdminJsonResponseController@addNewSavings');
Route::get('/admin/load/savings/users',		'AdminJsonResponseController@loadSavingsUsers');
Route::post('/admin/generate/reports',		'AdminJsonResponseController@generateReports');
Route::post('/admin/create/book',			'AdminJsonResponseController@createAjoBook');
Route::get('/admin/get-all/book',			'AdminJsonResponseController@getAjoBook');
Route::get('/admin/occupation/category',	'AdminJsonResponseController@getWorkCategory');
Route::get('/admin/fetch/user',				'AdminJsonResponseController@getNameHints');
Route::post('/admin/add/members',			'AdminJsonResponseController@addAjoMember');
Route::get('/admin/get-ajo/members',		'AdminJsonResponseController@getAjoMember');
Route::post('/admin/search/customer',		'AdminJsonResponseController@searchCustomer');
Route::post('/admin/accept/deposit',		'AdminJsonResponseController@acceptDeposit');
Route::post('/admin/accept/withdraw',		'AdminJsonResponseController@acceptWithdraw');
Route::post('/admin/upload/customers/info',	'AdminJsonResponseController@uploadCustomerInformation');
Route::post('/admin/upload/customers/loans','AdminJsonResponseController@uploadCustomerLoans');
Route::post('/admin/create/admin/user',		'AdminJsonResponseController@adminAddUsers');
Route::get('/admin/load/admin',				'AdminJsonResponseController@loadAdmin');
Route::post('/admin/change/password',		'AdminJsonResponseController@changePassword');
Route::post('/admin/update/profile',		'AdminJsonResponseController@updateProfile');
Route::get('/admin/load/all/trails',		'AdminJsonResponseController@getAllTrails');

/*
|---------------------------------------------------------------------------------------------------------
| DEVELOPMENT MODE CONTROLLER 
|---------------------------------------------------------------------------------------------------------
|
*/
Route::get('/development-mode',		'DevelopmentModeController@devMode')->name('dev_mode');
Route::get('/updates-mode',			'DevelopmentModeController@updateMode')->name('update_mode');

/*
|---------------------------------------------------------------------------------------------------
| DEVELOPMENT SECTION CALL TRIGGER FOR APP MAINTAINANCE
|---------------------------------------------------------------------------------------------------
|
*/
// clear config & caches files
Route::get('/dev-clear', function (){
	Artisan::call('config:clear');
	Artisan::call('view:clear');	
	Artisan::call('cache:clear');
	Artisan::call('route:clear');
	return redirect()->back();
});

// start maintainance
Route::get('/dev-down', function (){
	Artisan::call('down');
	return redirect()->back();
});

// stop maintainance
Route::get('/dev-up', function (){
	Artisan::call('up');
	return redirect()->back();
});

// stop maintainance
Route::get('/dev-setup-database', function (){
	Artisan::call('migrate');
	return redirect()->back();
});

// stop maintainance
Route::get('/dev-reset-database', function (){
	Artisan::call('migrate:refresh');
	return redirect()->back();
});

// application upgrading mode
Route::get('/dev-mode', function(){
	return view('dev-mode.dev-mode');
});